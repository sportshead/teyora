"use strict";

/* eslint-disable @typescript-eslint/no-var-requires */
const { exec } = require("child_process");
const path = require("path");
const fs = require("fs");

module.exports = (async () => {
    const begin = Date.now();

    const rootDir = (() => {
        const root = path.resolve("/");
        let currentPath = path.resolve(__dirname);

        while (!fs.existsSync(path.join(currentPath, ".Teyora-anchor"))) {
            if (currentPath === root)
                return null;
            else
                currentPath = path.resolve(currentPath, "..");
        }

        return currentPath;
    })();
    const srcDir = path.resolve(rootDir, "frontend");
    const commonsDir = path.resolve(rootDir, "common");


    const runProcess = async (command, options, runOptions = {
        silent: false
    }) => {
        if (!runOptions.silent) console.log("-".repeat(60));
        const proc = exec(command, {
            cwd: srcDir,
            windowsHide: true,
            ...options
        });

        let stdout = Buffer.alloc(0);
        let stderr = Buffer.alloc(0);

        // noinspection JSUnresolvedFunction
        proc.stdout.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stdout.write(d);

            stdout = Buffer.concat([stdout, data]);
        });

        // noinspection JSUnresolvedFunction
        proc.stderr.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stderr.write(d);

            stderr = Buffer.concat([stderr, data]);
        });

        const code = await new Promise((res, rej) => {
            proc.on("exit", function (code) {
                if (code === 0) res(code);
                else rej(code);
            });
        }).catch(r => r);

        if (!runOptions.silent) console.log("-".repeat(60));
        return {
            code: code,
            stdout: stdout,
            stderr: stderr
        };
    };

    // Introduction
    if (require.main === module) {
        console.log();
        console.log(" ---- Teyora Build Script ---- ");
        console.log("         (for frontend)         ");
        console.log(" ------------------------------ ");
        console.log();
    }

    // Determine Teyora version
    const rwVersion = (/TY_FRONTEND_VERSION\s?=\s?"(.*)";/g
        .exec(fs.readFileSync(path.resolve(commonsDir, "src", "Constants.ts")).toString("utf-8")))[1];

    // Determine git commit
    const gitHash = (await runProcess("git rev-parse HEAD", {}, {
        silent: true
    })).stdout.toString("utf-8").trim();

    const rwIdentifier = `${rwVersion}${
        process.env.NODE_ENV === "production" ? "p" : "d"
    }-${gitHash.substr(0, 6)}`;

    console.log(`Building for commit: ${gitHash.trim()}...`);
    console.log(`Teyora (frontend) v${rwIdentifier}`);
    console.log();

    if (require.main === module) {
        console.log("Updating constants...");
        console.log();

        fs.writeFileSync(
            path.resolve(commonsDir, "src", "DynamicConstants.ts"),
            `export const TY_GIT_HASH = "${gitHash}";`
        );
    }

    // Change package.json version
    const packageJsonPath = path.resolve(srcDir, "package.json");
    fs.writeFileSync(packageJsonPath,
        fs.readFileSync(packageJsonPath)
            .toString("utf-8")
            .replace(/("version":\s?").+?(")/, `$1${rwVersion}$2`)
    );

    // npm dependency check
    console.log("Checking dependencies...");

    const npmDepCheck = (await runProcess("npm i --no-progress")).code;

    if (npmDepCheck !== 0) {
        console.error(`npm finished with code: \`${npmDepCheck}\`.`);
        console.error("Something bad must have happened. Please fix the issue before rebuilding.");

        process.exit();
    } else {
        console.log("Dependencies all OK.");
    }

    // Webpack
    console.log("(Web)packing things up...");

    const wpPackCode = (await runProcess("npm run webpack")).code;

    if (wpPackCode !== 0) {
        console.error(`Webpack finished with code: \`${wpPackCode}\`.`);
        console.error("Something bad must have happened. Please fix the issue before rebuilding.");

        process.exit();
    } else {
        console.log("Webpack finished successfully.");
    }

    console.log();
    console.log("Artifacts created. Swapping out old Teyora on static...");
    console.log();

    // Create new Teyora file.
    const rwJsHeader = Buffer.from(
        [
            "/**",
            " * ",
            " * Teyora - the modern counter-vandalism tool for Wikipedians.",
            " * ",
            ` * TY-version = ${rwIdentifier}`,
            " * ",
            " * (c) 2020 The Teyora contributors.",
            " * Licensed under the Apache License 2.0 - read more at https://gitlab.com/Teyora",
            " * ",
            "**/",
            ""
        ].join("\n")
    );
    const rwStaticScripts = path.resolve(rootDir, "static", "scripts");
    if (!fs.existsSync(rwStaticScripts))
        fs.mkdirSync(rwStaticScripts);

    const rwStaticJsPath = path.resolve(rwStaticScripts, `Teyora-v${rwIdentifier}.js`);
    fs.writeFileSync(rwStaticJsPath,
        Buffer.concat([rwJsHeader, fs.readFileSync(
            path.resolve(srcDir, "build", "Teyora.js")
        )]),
        { encoding: "utf-8" }
    );

    // Swap out Teyora file in static.
    const rwScriptLoader = path.resolve(rwStaticScripts, "Teyora.js");
    fs.writeFileSync(rwScriptLoader,
        [
            "/**",
            " *",
            " * Teyora - the modern counter-vandalism tool for Wikipedians.",
            " *",
            " * Loading Script. This script will automatically load the latest Teyora version.",
            " *",
            " * (c) 2020 The Teyora contributors.",
            " * Licensed under the Apache License 2.0 - read more at https://gitlab.com/Teyora",
            " *",
            " **/",
            "",
            "(() => {",
            "    const rw_script = document.getElementById(\"Teyora-script\");",
            "",
            "    rw_script.id = undefined;",
            "    rw_script.parentElement.removeChild(rw_script);",
            "",
            "    const Teyora = document.createElement(\"script\");",
            `    Teyora.src = "scripts/Teyora-v${rwIdentifier}.js";`,
            `    Teyora.setAttribute("data-ty-commit", "${gitHash}");`,
            "    document.getElementsByTagName(\"head\")[0].appendChild(Teyora);",
            "})();"
        ].join("\n")
    );

    // Delete other Teyora JS files.
    //
    // This is performed after creating the file so if anyone reloads so we can
    // avoid the infinitesimal chance that someone would load Teyora while the
    // old Teyora had just been deleted.
    fs.readdirSync(rwStaticScripts).forEach(v => {
        if (v !== `Teyora-v${rwIdentifier}.js`
            && /Teyora-v[0-9.dp]+?-[a-f0-9]+?.js/.test(v))
            fs.unlinkSync(path.resolve(rwStaticScripts, v));
    });

    if (require.main === module) {
        for (let i = 0; i < 10; i++) console.log();

        console.log(`Build successfully completed after ${(Date.now() - begin) / 1000}s.`);
        console.log("Happy patrolling! ~~~~");
    }
})();