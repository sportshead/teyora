export interface TeyoraSettings {

    activeWikis: string;

}

export const TY_DEFAULT_SETTINGS : TeyoraSettings = {

    activeWikis: "enwiki"

};

export default class SettingsManager {

    public static settings : TeyoraSettings;

    static async loadSettings() : Promise<void> {
        // Load settings from WP or the database.

        // For now, we'll use the defaults.
        SettingsManager.settings = TY_DEFAULT_SETTINGS;
    }

}