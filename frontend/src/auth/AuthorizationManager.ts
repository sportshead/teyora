import { UserEntry } from "../../../backend/src/db/dbcollections/user";
import { ProfileEntry, UserProfile } from "../../../backend/src/db/dbcollections/profile";
import Teyora from "../App";

export default class AuthorizationManager {

    /**
     * Gets the current user (also checks if logged in)
     **/
    public async getUser() : Promise<UserEntry | null> {
        // Todo: Check if token present, if so try to get profile and if this fails reauth is needed.
        // TODO: Restructure needed
        return new Promise((resolve, reject) => {
            // Send a request to the server to get the user
            fetch("/api/user/info").then(response => { 
                // If not authed don't send user info
                if (response.status === 403) {
                    // Account is probably locked, no point continuing as they'd just be an error
                    window.location.replace("/api/logout");
                }
                if (response.status !== 200) resolve(null); else response.json().then(data => {
                    resolve(data);
                });
            }).catch(err => {
                // Request error that should be handled
                console.error(err);
                reject(null);
            });
        });
    }

    /**
     * Asks the server to unlock or autoapprove the account
    **/
    public async unlockAccount(wikiID: string) : Promise<UserEntry | null> {
        return new Promise((resolve, reject) => {
            // Send a request to the server to get the user
            fetch("/api/user/unlock?wikiID="+ wikiID).then(response => {
                if (response.status !== 200) reject(response.json());
                else response.json().then((data: UserEntry) => {
                    Teyora.TY.CurrentUser = data;
                    resolve(data);
                });
            }).catch(err => {
                // Request error that should be handled
                console.error(err);
                reject(null);
            });
        });
    }

    /**
     * Gets the profile of the user
     */
    public async getProfile() : Promise<ProfileEntry> {
        // Todo: Check if token present, if so try to get profile and if this fails reauth is needed.
        // TODO: Restructure needed
        return new Promise((resolve, reject) => {
            // Send a request to the server to get the user
            fetch("/api/user/profile").then(response => { 
                // If not authed don't send user info
                if (response.status === 403) {
                    // Return empty, if the user is suspended they can take a crash
                    resolve({} as ProfileEntry);
                }
                if (response.status !== 200) resolve({} as ProfileEntry); else response.json().then(data => {
                    resolve(data);
                });
            }).catch(err => {
                // Request error that should be handled
                console.error(err);
                reject(null);
            });
        });
    }

    /**
     * Update the profile of the user
     */

    public async updateProfile(profile: UserProfile) : Promise<UserProfile> {
        return new Promise((resolve, reject) => {
            // Send a request to the server to get the user
            fetch("/api/user/profile", {
                method: "PUT", // Get = get this profile, PUT = put (update) thi profile
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(profile)
            }).then(response => { 
                // If not authed don't send user info
                if (response.status === 403) {
                    // Return empty, if the user is suspended they can take a crash
                    reject({});
                }
                if (response.status !== 200) reject({}); else response.json().then(data => {
                    resolve(data);
                });
            }).catch(err => {
                // Request error that should be handled
                console.error(err);
                reject(null);
            });
        });
    }


}