import {Component} from "react";
import { createTheme } from "@mui/material/styles";
import {getTheme} from "./Themes";
import Teyora from "../App";
import {SnackbarHandler} from "./components/SnackbarHandler";
import {OptionsObject} from "notistack";
import { MessageBoxProps } from "./dialogs/MessageBox";
import { LocalisedButtons } from "./LocalisedButtons";
import i18n from "../i18n";

/**
 * The Teyora UI controller. Just a container class for a lot of UI features.
 **/
export default class TeyoraUI {

    static snackbarHandler : Component;

    static showSnackbar(text : string, options: OptionsObject) : void {
        (TeyoraUI.snackbarHandler as SnackbarHandler)
            .showSnackbar(text, options);
    }

    // Get how the current user is referred to in the UI
    // Nickname (if set) or username
    static getUserRef() : string {
        if (Teyora.TY.CurrentUser === null) return "";
        return (Teyora.TY.CurrentProfile.profile && Teyora.TY.CurrentProfile.profile.nickname) || Teyora.TY.CurrentUser.username.split(":")[1];
    }

    // Show a very basic alert box with a message and specifed buttons (just OK if not specified)
    static showAlertBox(message : string, title?: string, buttonText?: string[]) : Promise<string> {
        const localButtons = LocalisedButtons();
        return new Promise(resolve => {
            const props : MessageBoxProps = {
                title: title || "Alert",
                message: message,
                buttons: (buttonText || [localButtons.OK]).map(label => ({ label, onClick: close => {close();resolve(label);} })),

                open: true,
                onClose: () => {
                    // Do nothing
                }
            };
            Teyora.TY.MsgBoxQueue.enqueueMessageBox(props);
        });
    }

    // Show an alert box that asks the user if they want to log out.
    static async confirmLogout() : Promise<void> {
        const lB = LocalisedButtons();
        const result = await TeyoraUI.showAlertBox(
            i18n.t("login:confirmLogout.text"),
            i18n.t("login:confirmLogout.title", {user: Teyora.TY.CurrentUser.username}),
            [lB.CANCEL, lB.LOGOUT]
        );
        if (result === lB.LOGOUT) window.location.href = "/api/logout";
    }

    /**
     * Grabs all Material UI theme values, and drops them into the CSS as usable
     * CSS variables.
     **/
    static injectTheme() : void {
        const muiTheme = createTheme(getTheme(Teyora.TY.state.theme));
        const getValues = (prefix : string, object : Record<string, any>, options : {
            numberUnits: "raw" | "ms" | "s" | "px" | "vh" | "vw" | "vmin" | "vmax" | "em" | "rem"
        } = {
            numberUnits: "raw"
        }) : Record<string, any> => {
            let foundValues : Record<string, any> = {};
            for (const [key, value] of Object.entries(object)) {
                if (typeof value === "object") {
                    foundValues = Object.assign(
                        foundValues,
                        getValues(prefix + "-" + key, value, options)
                    );
                } else if (typeof value === "number") {
                    foundValues[prefix + "-" + key] =
                        (options.numberUnits === "raw" ? value : `${value}${options.numberUnits}`);
                } else if (typeof value === "string") {
                    foundValues[prefix + "-" + key] = value;
                }
            }
            return foundValues;
        };
        let styleVariables = "";
        for (const [key, value] of Object.entries(getValues("--TY", muiTheme.palette)))
            styleVariables = styleVariables + `${key}: ${value};`;
        for (const [key, value] of
            Object.entries(getValues("--TY-transition", muiTheme.transitions, {numberUnits: "ms"})))
            styleVariables = styleVariables + `${key}: ${value};`;
        document.getElementById("Teyora-style").innerHTML = `:root{${styleVariables}}`;
    }

}