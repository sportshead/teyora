// Shown when the app crashes
import * as React from "react";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import {ITYWindowProps} from "../../App";
import Button from "@mui/material/Button";
import { DialogActions } from "@mui/material";
import FullPageImage from "../components/FullPageImage";
import i18n from "../../i18n";

/**
 * The AppCrash will show if the app has an unexpected error and can't continue.
 **/
export default class AppCrash extends React.Component<ITYWindowProps> {

    /**
     * Renders the window.
     **/
    render() : JSX.Element {
        if (!i18n.isInitialized) window.location.reload(); // A crash occured before i18n was initialized. Reload the page.

        return (
            <div>
                <FullPageImage image="/images/backdrops/ColdTower.jpg" />
                <Dialog open={true}>
                    <DialogTitle>{i18n.t("ui:AppCrash.title")}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {i18n.t("ui:AppCrash.preErrorMessage")}
                        </DialogContentText><br />
                        <DialogContentText>
                            {this.props.message || i18n.t("ui:AppCrash.noErrorMessage")} 
                        </DialogContentText><br/>
                        <DialogContentText>
                            {i18n.t("ui:AppCrash.postErrorMessage")}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>window.location.reload()}>{i18n.t("ui:AppCrash.reloadButton")}</Button>
                    </DialogActions>
                </Dialog>
            </div>);
    }

}