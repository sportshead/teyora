// The dialog used for messages, prompts etc.
import * as React from "react";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Button from "@mui/material/Button";
import { DialogActions } from "@mui/material";
import MessageBoxQueue from "./MessageBoxQueue";
import { TY_FRONTEND_DIALOG_DELAY } from "../../../../common/src/Constants";

export interface MessageBoxButton {
    label: string;
    // eslint-disable-next-line no-unused-vars
    onClick: (close: ()=>void) => void;
}

// Used for this component only
export interface MessageBoxQueueProps {
    queue: MessageBoxQueue;
}

export interface MessageBoxProps {
    title: string;
    message: string;
    open: boolean;
    buttons: MessageBoxButton[];
    onClose: () => void;
}


/**
 * A generic message box dialog.
 **/
export default class MessageBox extends React.Component<MessageBoxQueueProps> {

    state:MessageBoxProps = {
        open: false,
        buttons: [],
        onClose: () => {console.log("Default onClose");},
        title: "Teyora",
        message: "This is the default message. If you see it, let us know.",
    };

    constructor(props: MessageBoxQueueProps) {
        super(props);
    }

    // lets the dialog be reused
    public setNewProps(props : MessageBoxProps) : void {
        if (props == null) return; // This will cause an error if the props are null.
        this.setState({
            open: props.open,
            buttons: props.buttons,
            onClose: props.onClose,
            title: props.title,
            message: props.message,
        });
    }

    public setOpen() : void {
        this.setState({
            open: true,
        });
    }

    public setClosed() : void {
        this.setState({
            open: false,
        });
    }

    /**
     * Renders the window.
     **/
    render() : JSX.Element {
        // Handles when a new dialog is added to the queue.
        this.props.queue.queueUpdate = ()=>{
            // If a dialog is already open, we don't want to open a new one just yet.
            if (this.state.open) return;

            // Now set the props to next messagebox in the queue.
            this.setNewProps(this.props.queue.getNextMessageBox());    
        };

        return <Dialog
            open={this.state.open}
            fullWidth={true}
            maxWidth="sm"
        >
            <DialogTitle>{this.state.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {this.state.message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                {this.state.buttons.map((button, index) => {
                    return <Button
                        key={index}
                        onClick={()=>button.onClick(()=>{
                            // Set state to closed when closed() called
                            this.setState({open: false});

                            // Open the next dialog after the specificed delay.
                            setTimeout(()=>{
                                this.props.queue.queueUpdate();
                            }, TY_FRONTEND_DIALOG_DELAY);

                            // Call onClose()
                            this.state.onClose();
                        })}>
                        {button.label}
                    </Button>;
                })}
            </DialogActions>
        </Dialog>;
    }

}