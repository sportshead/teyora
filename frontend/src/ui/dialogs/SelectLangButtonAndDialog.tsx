import * as React from "react";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import {ITYWindowProps} from "../../App";
import Button from "@mui/material/Button";
import TranslateIcon from "@mui/icons-material/Translate";
import FavIcon from "@mui/icons-material/Favorite";
import { useTranslation, withTranslation } from "react-i18next";
import { languageList } from "../../i18n";
import { Avatar, List, ListItem, ListItemAvatar, ListItemText } from "@mui/material";

/**
 * The SelectLangButtonAndDialog is an important component that is used to switch languages.
 * You should use it sparingly to prevent confusion and duplication.
 **/
interface LangSelectProps {
    open: boolean;
    onClose: () => void;
}

function LanguageSelectDialog(props: LangSelectProps): JSX.Element {
    const { t, i18n } = useTranslation();
    const { open, onClose } = props;
    
    const handleListItemClick = async (name: string, code: string) => {
        if (t("name") === name) return; // Don't change language if it's already the current one.
        
        // No need to confirm, just go ahead and change the language.
        await i18n.changeLanguage(code);
    };
  
    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>{t("selector")}</DialogTitle>
            <List>
                {languageList.map((lang) => (
                    <ListItem button onClick={() => handleListItemClick(lang.name, lang.code)} key={lang.code}>
                        <ListItemAvatar>
                            <Avatar>
                                <TranslateIcon />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={lang.name} />
                    </ListItem>
                ))}

                {/* Help with translations list item */}
                <ListItem autoFocus button onClick={() => window.open(t("selectorHelpLink"))}>
                    <ListItemAvatar>
                        <Avatar>
                            <FavIcon />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={t("selectorHelpNote")} />
                </ListItem>
            </List>
        </Dialog>
    );
}

export interface SelectLangButtonAndDialogProps extends ITYWindowProps {
    buttonColor?: "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning";
}

class SelectLangButttonAndDialog extends React.Component<SelectLangButtonAndDialogProps> {

    state: LangSelectProps = {
        open: false,
        onClose: () => this.setState({open: false}),
    };

    /**
     * Renders the button that opens the window.
     **/
    render() : JSX.Element {
        const { t } = this.props; // These are filled in by the withTranslation() decorator.
        
        // Translate icon then language name.
        return (<div>
            <Button
                startIcon={<TranslateIcon />}
                onClick={()=>this.setState({open: true})}
                color={(this.props.buttonColor || "secondary")}
            >
                {t("name")}
            </Button>
            <LanguageSelectDialog open={this.state.open} onClose={this.state.onClose} />
        </div>);
    }
}

export default withTranslation()(SelectLangButttonAndDialog);