/*
The stepper, and steps of the setup process.
*/

import * as React from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Teyora from "../../../App";
import TeyoraUI from "../../TeyoraUI";
import { LocalisedButtons } from "../../LocalisedButtons";
import { useTranslation } from "react-i18next";
import { Alert, Avatar, List, ListItem, ListItemAvatar, ListItemText } from "@mui/material";
import { AccountCircle, AddTask, Brush, FactCheck, Merge, MergeType, School } from "@mui/icons-material";
import WikiSelector from "../../components/WikiSelector";
import { defaultWikiSupportMap } from "../../../../../common/src/objects/WikiSupportMap";
import { TY_FRONTEND_VERIFICATION_LOADER_DELAY } from "../../../../../common/src/Constants";
import ProfileEditor from "../../components/profile/ProfileEditor";
import { ProfileEntry } from "../../../../../backend/src/db/dbcollections/profile";
import Celebration from "../../components/Celebration";


let steps:string[] = [];
let handleNext:() => void; // So it can be accessed throughout this file

export default function FTSStepper() {
    const { t } = useTranslation();
    steps = t("login:firstTimeSetup.stepSummaries").split("/");
    
    const stepContent: JSX.Element[] = [
        WelcomeStep(),
        EligibilityStep(),
        ProfileStep(),
        FinishStep()
    ];

    const lB = LocalisedButtons();
    const [activeStep, setActiveStep] = React.useState(0);

    handleNext = async () => {
        if (activeStep === steps.length - 1) {
            // When complete, reload the page
            window.location.reload();
            return;
        }

        // When first step, check superoverlord and show warning
        if (activeStep === 0 && Teyora.TY.CurrentUser.userID === "SuperOverlord") {
            await TeyoraUI.showAlertBox(
                t("login:firstTimeSetup.SuperOverlordWarning.text"),
                t("login:firstTimeSetup.SuperOverlordWarning.title")
            );
        }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    return (
        <Box sx={{ width: "100%" }}>
            <Stepper activeStep={activeStep}>
                {steps.map((label) => {
                    const stepProps: { completed?: boolean } = {};
                    const labelProps: {
                        optional?: React.ReactNode;
                    } = {};

                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>

            
            <React.Fragment>
                
                {/* Add the active step to the page */}
                {stepContent[activeStep] || <h1>Developer error: You need more steps, or this language has too many steps</h1>}

                {/* Navigation Buttons */}
                <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                    <Button
                        color="inherit"
                        disabled={activeStep === 0}
                        onClick={handleBack}
                        sx={{ mr: 1 }}
                    >
                        {lB.BACK}
                    </Button>
                    <Box sx={{ flex: "1 1 auto" }} />
                    
                    {/* Next button, disabled if on verification step and account locked */}
                    <Button onClick={handleNext} disabled={
                        (activeStep == 1 && Teyora.TY.CurrentUser.isLocked) ||
                        (activeStep == 2 && Object.keys(Teyora.TY.CurrentProfile).length === 0)
                    }>
                        {[lB.NEXT, lB.SKIP, lB.SKIP, t("login:firstTimeSetup.complete")][activeStep]}
                    </Button>
                </Box>
            </React.Fragment>
            
        </Box>
    );
}

// First Step

function WelcomeStep(): JSX.Element {
    const { t } = useTranslation();
    return <Box sx={{ width: "100%", paddingTop: "3vh", paddingLeft: "1vh" }}>
        <Typography variant="h6" gutterBottom>
            {t("login:firstTimeSetup.welcome", { user: Teyora.TY.CurrentUser.username.split(":")[1]})}
        </Typography>
        <Typography variant="body1" gutterBottom>
            {t("login:firstTimeSetup.welcomeDesc")}
        </Typography>

        <List sx={{ width: "100%" }}>
            {/* Verify your account */}
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <FactCheck />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={steps[1]} secondary={t("login:firstTimeSetup.eligibilityDescription")} />
            </ListItem>

            {/* Create a profile */}
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <AccountCircle />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={steps[2]} secondary={t("login:firstTimeSetup.profileDescription")} />
            </ListItem>

            {/* Finish */}
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <AddTask />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={steps[3]} secondary={t("login:firstTimeSetup.finishDescription")} />
            </ListItem>
        </List>
    </Box>;
}

// Second Step, eligibility
function EligibilityStep(): JSX.Element {
    const { t } = useTranslation();
    return <Box sx={{ width: "100%", paddingTop: "3vh", paddingLeft: "1vh" }}>
        <Typography variant="h6" gutterBottom>
            {t("login:firstTimeSetup.eligibility.title", { user: Teyora.TY.CurrentUser.username.split(":")[1]})}
        </Typography>
        <Typography variant="body1" gutterBottom>
            {t("login:firstTimeSetup.eligibility.desc")}
        </Typography>
        { // If already unlocked/approved
            !Teyora.TY.CurrentUser.isLocked && !Teyora.TY.CurrentUser.isApproved ?
                (<Alert severity="info">
                    {t("login:firstTimeSetup.eligibility.alreadyUnlocked")}
                </Alert>) :
                Teyora.TY.CurrentUser.isApproved && (
                    <Alert severity="success">
                        {t("login:firstTimeSetup.eligibility.alreadyApproved")}
                    </Alert>
                )
        }

        <br/>
        {/* Only show Wiki selector if it is actually useful, i.e. user isn't already approved */}
        {!Teyora.TY.CurrentUser.isApproved && (<WikiSelector onSelect={
            async (wikiKey: string) => {
                const wiki = Teyora.TY.WikiSupport[wikiKey];
                if (!wiki.supportMap) wiki.supportMap = defaultWikiSupportMap;
                
                if (!wiki.supportMap.canBeUsedToAutoApprove) {
                    // Wiki is not eligible for approval, i.e. testwiki, spam, etc.
                    TeyoraUI.showAlertBox(
                        t("login:firstTimeSetup.eligibility.wikiNotSupported.message"),
                        t("login:firstTimeSetup.eligibility.wikiNotSupported.title")
                    );
                    return;
                }

                // Wiki can be used to auto-approve, so we can move on
                Teyora.TY.SetFullPageLoaderOpen(true); // Big loading screen

                // Cosmetics, make it feel like some more work is going on whilst the load is open
                await new Promise(resolve => setTimeout(resolve, TY_FRONTEND_VERIFICATION_LOADER_DELAY));

                try {
                    // Try to verify
                    await Teyora.TY.authManager.unlockAccount(wikiKey);
                } catch (error) {
                    // We couldn't verify because of an error
                    Teyora.TY.SetFullPageLoaderOpen(false);
                    TeyoraUI.showAlertBox(
                        t("login:firstTimeSetup.eligibility.error.message"),
                        t("login:firstTimeSetup.eligibility.error.title")
                    );
                    return;
                }

                // Now we can check if the user is eligible
                Teyora.TY.SetFullPageLoaderOpen(false);
                if (Teyora.TY.CurrentUser.isLocked) {
                    // User is not eligible
                    TeyoraUI.showAlertBox(
                        t("login:firstTimeSetup.eligibility.notEligible.message"),
                        t("login:firstTimeSetup.eligibility.notEligible.title")
                    );
                    return;
                }

                // Else, we can now move on to the next step
                handleNext();

            }
        } />)}
    </Box>;
}

// Third Step, profile
function ProfileStep(): JSX.Element {
    const { t } = useTranslation();
    return (
        (Teyora.TY.CurrentUser.isLocked) ? (
            // An error if the last step was not completed (only possible if tampered with)
            <Alert severity="error">
                {t("login:firstTimeSetup.profile.bypassAttempted")}
            </Alert>
        ) : (
            <Box sx={{ width: "100%", paddingTop: "3vh", paddingLeft: "1vh" }}>
                {(Teyora.TY.CurrentUser.isApproved) ? (
                    <Alert severity="success">
                        {t("login:firstTimeSetup.profile.AccountApproved")}
                    </Alert>
                ) : (
                    <Alert severity="info">
                        {t("login:firstTimeSetup.profile.AccountNotApproved")}
                    </Alert>
                )}

                <Typography variant="h6" gutterBottom sx={{paddingTop: "1vh"}}>
                    {t("login:firstTimeSetup.profile.title")}
                </Typography>
                <Typography variant="body1" gutterBottom>
                    {t("login:firstTimeSetup.profile.desc")}
                </Typography>

                <ProfileEditor
                    user={Teyora.TY.CurrentUser}
                    profile={Teyora.TY.CurrentProfile}
                    onSave={async (profile: ProfileEntry) => {
                        Teyora.TY.SetFullPageLoaderOpen(true);
                        // Sleep for a bit to make it feel like the save is happening
                        await new Promise(resolve => setTimeout(resolve, TY_FRONTEND_VERIFICATION_LOADER_DELAY));
                        try {
                            await Teyora.TY.authManager.updateProfile(profile.profile);
                            Teyora.TY.SetFullPageLoaderOpen(false);
                            handleNext(); // Move to last step
                        } catch (error) {
                            Teyora.TY.SetFullPageLoaderOpen(false);
                            TeyoraUI.showAlertBox(
                                t("login:firstTimeSetup.profile.error.message"),
                                t("login:firstTimeSetup.profile.error.title")
                            );
                            return;
                        }
                    }}
                />
            </Box>
        )
    );
}

// Fourth Step, finish
function FinishStep(): JSX.Element {
    const { t } = useTranslation();
    return (
        <Box sx={{ width: "100%", paddingTop: "3vh", paddingLeft: "1vh" }}>
            <Celebration />
            <Typography variant="h4" gutterBottom sx={{paddingTop: "1vh"}}>
                {t("login:firstTimeSetup.finish.title", { user: TeyoraUI.getUserRef() })}
            </Typography>
            <Typography variant="body1">
                {t("login:firstTimeSetup.finish.desc")}
            </Typography>

            {/* Next steps */}
            <List sx={{ width: "100%" }}>
                {/* Verify your account */}
                <ListItem
                    secondaryAction={(
                        <Button variant="outlined">{t("login:firstTimeSetup.finish.learnHowButton")}</Button>
                    )}
                >
                    <ListItemAvatar>
                        <Avatar>
                            <Brush /> 
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={t("login:firstTimeSetup.finish.customise.title")}
                        secondary={t("login:firstTimeSetup.finish.customise.desc")}
                    />
                </ListItem>
                
                {/* Sandbox */}
                <ListItem
                    secondaryAction={(
                        <Button variant="outlined">{t("login:firstTimeSetup.finish.learnHowButton")}</Button>
                    )}
                >
                    <ListItemAvatar>
                        <Avatar>
                            <School />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={t("login:firstTimeSetup.finish.learn.title")}
                        secondary={t("login:firstTimeSetup.finish.learn.desc")}
                    />
                </ListItem>

                {/* Migration guide for Twinkle */}
                <ListItem
                    secondaryAction={(
                        <Button variant="outlined">{t("login:firstTimeSetup.finish.learnHowButton")}</Button>
                    )}
                >
                    <ListItemAvatar>
                        <Avatar>
                            <MergeType />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={t("login:firstTimeSetup.finish.migrate.title")}
                        secondary={t("login:firstTimeSetup.finish.migrate.desc")}
                    />
                </ListItem>
            </List>
        </Box>

    );
}