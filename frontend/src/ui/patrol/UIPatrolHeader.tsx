import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import {Avatar, IconButton} from "@mui/material";
import Menu from "@mui/icons-material/Menu";
import {TYWordmarkPrimaryContrasted} from "../components/TYLogo";
import Tooltip from "@mui/material/Tooltip";
import Lock from "@mui/icons-material/Lock";
import NotificationImportant from "@mui/icons-material/NotificationImportant";
import FindInPage from "@mui/icons-material/FindInPage";
import WatchLater from "@mui/icons-material/WatchLater";
import SupervisedUserCircle from "@mui/icons-material/SupervisedUserCircle";
import Cancel from "@mui/icons-material/Cancel";
import Typography from "@mui/material/Typography";
import {UIPatrolContext} from "./Contexts";
import Info from "@mui/icons-material/Info";
import Teyora from "../../App";
import TeyoraUI from "../TeyoraUI";


/**
 * The header for the {@link UIPatrolWindow}. This includes the TY logo, and the user account menu.
 **/
export class UIPatrolHeader extends React.Component<{hotdog? : boolean}> {

    render() : JSX.Element {
        return (
            <UIPatrolContext.Consumer>
                {
                    context => <AppBar position="static" className={"TY-patrol-header"}>
                        <Toolbar
                            disableGutters={true}
                            className={"TY-patrol-header-toolbar"}
                            color="primary">
                            <div className={"TY-patrol-header-left"}>
                                <IconButton
                                    onClick={() => {
                                        context.patrolWindow.setRecentChangesDrawerState();
                                    }}
                                    color="inherit"
                                    size="large">
                                    {
                                        this.props.hotdog == true ?
                                            <img src={"images/hotdog.svg"} alt={"Hot Dog"}/> : <Menu />
                                    }
                                </IconButton>
                                <TYWordmarkPrimaryContrasted />
                            </div>
                            <UIPatrolHeaderTools
                                className={"TY-patrol-header-center"}/>
                            <div className={"TY-patrol-header-right"}>
                                <UIPatrolHeaderPageInfo />
                                {
                                    context.patrolWindow.state.activeEntry != null ?
                                        <IconButton
                                            onClick={() => {
                                                context.patrolWindow.setExtraPageInfoState();
                                            }}
                                            color="inherit"
                                            size="large">
                                            <Info />
                                        </IconButton> : undefined
                                }
                                <IconButton color="inherit" size="large">
                                    {/* Todo: Replace with actual profile icon */}
                                    <Avatar alt={Teyora.TY.CurrentUser.username} src="/images/profilePictures/ACoolTower.jpg"></Avatar>
                                </IconButton>
                            </div>
                        </Toolbar>
                    </AppBar>
                }
            </UIPatrolContext.Consumer>
        );
    }

}

/**
 * The tools found at the center of the {@link UIPatrolHeader}. If an article is
 * not selected, this component needs to be hidden.
 **/
export class UIPatrolHeaderTools extends React.Component<{className? : string}> {

    render() : JSX.Element {
        return (
            <div className={`TY-patrol-tools ${this.props.className ?? ""}`}>
                <Tooltip
                    title={"Manage Page Protection"}>
                    <IconButton color="inherit" size="large">
                        <Lock />
                    </IconButton>
                </Tooltip>
                <Tooltip
                    title={"Alert on Change"}>
                    <IconButton color="inherit" size="large">
                        <NotificationImportant />
                    </IconButton>
                </Tooltip>
                <Tooltip
                    title={"Test Messagebox"}>
                    <IconButton color="inherit" onClick={()=>TeyoraUI.showAlertBox("Hello")} size="large">
                        <FindInPage />
                    </IconButton>
                </Tooltip>
                <Tooltip
                    title={"Test block screen"}>
                    <IconButton
                        color="inherit"
                        onClick={()=>Teyora.TY.setState({activeWindow: Teyora.TY.AccountSuspendedError()})}
                        size="large">
                        <WatchLater />
                    </IconButton>
                </Tooltip>
                <Tooltip
                    title={"Log out temp"}>
                    <IconButton
                        color="inherit"
                        onClick={async ()=>{
                            // Show an alert box asking if the user is sure they want to log out
                            const result = await TeyoraUI.showAlertBox("Are you sure you want to log out?", `Log out of ${Teyora.TY.CurrentUser.username}`, ["Yes", "No"]);
                            if (result == "Yes") window.location.replace("/api/logout");
                        }}
                        size="large">
                        <SupervisedUserCircle />
                    </IconButton>
                </Tooltip>
                <Tooltip
                    title={"Exit Revision"}>
                    <UIPatrolContext.Consumer>
                        {
                            context => <IconButton
                                color={"inherit"}
                                onClick={() => {
                                    context.patrolWindow.setState({
                                        activeEntry: null
                                    });
                                }}
                                size="large">
                                <Cancel />
                            </IconButton>
                        }
                    </UIPatrolContext.Consumer>
                </Tooltip>
            </div>
        );
    }

}

/**
 * The page info found at the right of the {@link UIPatrolHeader}. If an article is
 * not selected, this component needs to be hidden.
 **/
export class UIPatrolHeaderPageInfo extends React.Component<{className? : string}> {

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <div className={`TY-patrol-pageinfo ${this.props.className ?? ""}`}>
                    <Typography variant="h6">
                        {context.patrolWindow.state.activeEntry != null ?
                            context.patrolWindow.state.activeEntry.page : ""}
                    </Typography>
                    <Typography variant="caption">
                        {/* TODO: Add user cards and things */}
                        {context.patrolWindow.state.activeEntry != null ?
                            context.patrolWindow.state.activeEntry.user.user_text : ""}
                    </Typography>
                </div>
            }
        </UIPatrolContext.Consumer>;
    }

}