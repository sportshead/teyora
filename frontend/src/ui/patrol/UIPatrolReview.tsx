import * as React from "react";
import Paper from "@mui/material/Paper";
import {UIPatrolContext} from "./Contexts";
import {Typography, Chip} from "@mui/material";
import Avatar from "@mui/material/Avatar";
import { TeyoraEditInfo } from "../../../../common/src/objects/TeyoraEditInfo";
import {UIPatrolRCEntryBytes} from "./UIPatrolRecentChanges";
import TeyoraUI from "../TeyoraUI";

/**
 * The review and reversion panel for the {@link UIPatrolBody}.
 **/
export class UIPatrolReviewPanel extends React.Component {

    static contextType = UIPatrolContext;

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(props : any, state : any) {
        super(props, state);
    }

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <Paper
                    className={"TY-patrol-review"}
                    elevation={context.patrolWindow.state.activeEntry == null ? 0 : 8}
                    square={true}>
                    {
                        // TODO Please add a placeholder here soon.
                        context.patrolWindow.state.activeEntry && [
                            <UIPatrolReviewRevisionPanel key={"revPanel"}/>,
                            <UIPatrolReviewPageInfoPanel key={"infoPanel"} />
                        ]
                    }
                </Paper>
            }
        </UIPatrolContext.Consumer>;
    }
}

/**
 * The extra page information panel for the {@link UIPatrolReviewPanel}.
 **/
export class UIPatrolReviewRevisionPanel extends React.Component {

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(props : any, state : any) {
        super(props, state);
    }

    render() : JSX.Element {
        return <div
            className={"TY-patrol-review-rev"}>
            <UIPatrolReviewRevisionInfo />
            {/* TODO Reversion actions */}
            {/* TODO Diff viewer */}
            {/* TODO Diff preview panel */}
        </div>;
    }
}

export class UIPatrolReviewRevisionInfo extends React.Component {

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => {
                    const activeEntry : TeyoraEditInfo
                        = context.patrolWindow.state.activeEntry;

                    return <div className={"TY-patrol-review-rev-pageinfo"}>
                        {/* Page title */}
                        <Typography
                            noWrap={true}
                            className={"TY-patrol-review-rev-pagetitle"}
                            variant={"h4"}>
                            {activeEntry.page}
                        </Typography>

                        {/* Byte difference */}
                        <Typography variant={"caption"}>
                            <UIPatrolRCEntryBytes bytes={activeEntry.byteDifference} />
                        </Typography>

                        {/* Editing user */}
                        <div className={"TY-patrol-review-rev-editor"}>
                            <Chip
                                avatar={<Avatar>{activeEntry.user.user_text.substring(0,1)}</Avatar>}
                                label={activeEntry.user.user_text}
                                onClick={() => {
                                    // Show the editor card.
                                    TeyoraUI.showAlertBox("Epic editor card moment");
                                }} />
                        </div>

                        {/* Edit summary */}
                        <Typography
                            className={"TY-patrol-review-rev-editsummary"}
                            variant={"body1"}>
                            {activeEntry.editSummary}
                        </Typography>

                        {/* TODO Edit Tags */}
                    </div>;
                }
            }
        </UIPatrolContext.Consumer>;
    }

}

/**
 * The extra page information panel for the {@link UIPatrolReviewPanel}.
 **/
export class UIPatrolReviewPageInfoPanel extends React.Component {

    static contextType = UIPatrolContext;

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(props : any, state : any) {
        super(props, state);
    }

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <Paper
                    className={"TY-patrol-review-pageinfo" + (
                        context.patrolWindow.state.extraPageInfoDrawerOpen ? " open" : ""
                    )}
                    elevation={10}
                    square>
                    {/* TODO xtools Page info */}
                    {/* TODO User info */}
                    {/* page revision history */}
                </Paper>
            }
        </UIPatrolContext.Consumer>;
    }
}