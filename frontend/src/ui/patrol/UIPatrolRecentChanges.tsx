import * as React from "react";
import {Drawer} from "@mui/material";
import Paper from "@mui/material/Paper";
import DynamicFeed from "@mui/icons-material/DynamicFeed";
import Typography from "@mui/material/Typography";
import {UIPatrolContext} from "./Contexts";
import { TeyoraEditInfo } from "../../../../common/src/objects/TeyoraEditInfo";

/**
 * The recent changes panel for the {@link UIPatrolBody}.
 **/
export class UIPatrolRCPanel extends React.Component {

    static contextType = UIPatrolContext;

    constructor(props : unknown, state : unknown) {
        super(props, state);

        this.context.rcPatrolManager.registerCallback("add", () => {
            this.setState({});
        });
    }

    render() : JSX.Element {
        const entries = this.context.rcPatrolManager.enumerate().map(
            (entry : TeyoraEditInfo) =>
                <UIPatrolRCEntry key={entry.teyoraRevID} patrolEntry={entry}/>
        );

        return <UIPatrolContext.Consumer>
            {
                context => <Drawer
                    PaperProps={{
                        className: "TY-patrol-recentchanges scroll-left"
                    }}
                    className={"TY-patrol-recentchanges-container " + (
                        context.patrolWindow.state.recentChangesDrawerOpen ? "" : " inactive"
                    )}
                    variant="persistent"
                    anchor="left"
                    open={context.patrolWindow.state.recentChangesDrawerOpen}>
                    <div>
                        {entries}
                        <Paper
                            elevation={0}
                            square={true}
                            className={"TY-patrol-recentchanges-end"}
                            onClick={() => {
                                context.patrolWindow.setState({
                                    activeEntry: null
                                });
                            }}>

                            {/* TODO: Active edit card holding area */}

                            {/* TODO: Pinned Cards */}

                            {/* TODO: Top Feed play/pause button
                                LIVE (last edit at X time) > || cog
                            */}
                            <DynamicFeed/><br/>
                            <Typography variant={"caption"}>
                                {/* Tip Location (end of feed) */}
                                Tip: You can increase your feed limit in your preferences
                            </Typography>
                        </Paper>
                    </div>
                </Drawer>
            }
        </UIPatrolContext.Consumer>;
    }
}

/**
 * An entry for the recent changes panel. This takes in a {@link TeyoraEditInfo} for its
 * information.
 **/
export class UIPatrolRCEntry extends React.Component<{
    patrolEntry : TeyoraEditInfo
}> {

    render() : JSX.Element {
        const patrolEntry = this.props.patrolEntry;

        return <UIPatrolContext.Consumer>
            {
                context => {
                    const activeEntry = context.patrolWindow.state.activeEntry;

                    return <Paper
                        elevation={
                            activeEntry == null ? 0 :
                                (activeEntry.teyoraRevID === patrolEntry.teyoraRevID
                                    ? 8 : 0)
                        }
                        square={true}
                        className={"TY-patrol-recentchanges-entry"}
                        data-ty-rev-active={
                            activeEntry != null ?
                                (activeEntry.teyoraRevID === patrolEntry.teyoraRevID ?
                                    "true" : "false") : undefined
                        }
                        onClick={() => {
                            context.patrolWindow.setState({
                                activeEntry: patrolEntry
                            });
                        }}>

                        { /* To darken the entry if another entry is active, or it has been reviewed by another editor. */ }
                        <div className={"TY-patrol-recentchanges-overlay"} />

                        { /* First row - page title */ }
                        <Typography
                            variant="h6"
                            className={"TY-patrol-recentchanges-pagename"}>
                            <div>{patrolEntry.page}</div>
                        </Typography>

                        { /* Second row - bytes changed and responsible user */ }
                        <div className={"TY-patrol-recentchanges-metainfo"}>
                            <UIPatrolRCEntryBytes bytes={patrolEntry.byteDifference}/>
                            |
                            <span className={"TY-patrol-recentchanges-editor-username"}>
                                {patrolEntry.user.user_text}
                            </span>
                        </div>

                        { /* Third row - User comment - TODO: security and links! */ }
                        <Typography
                            variant="caption"
                            style={{
                                fontStyle: "italic"
                            }}>
                            <div dangerouslySetInnerHTML={{__html: patrolEntry.editSummaryFormatted}}/>
                        </Typography>
                    </Paper>;
                }
            }
        </UIPatrolContext.Consumer>;
    }

}

export class UIPatrolRCEntryBytes extends React.Component<{bytes : number, short? : boolean}, any> {

    render() : JSX.Element {
        const classes = ["TY-patrol-bytes"];

        // Highlight the bytes by color depending on sign.
        // If the number is 0, the element is colored gray.
        if (this.props.bytes < 0)
            classes.push("TY-patrol-bytes-negative");
        else if (this.props.bytes > 0)
            classes.push("TY-patrol-bytes-positive");

        // Makes the element bold if the edit is larger than 1 kB.
        if (Math.abs(this.props.bytes) > 1000)
            classes.push("TY-patrol-bytes-large");

        return <span className={classes.join(" ")}>
            {
                this.props.short === true ? [
                    Math.sign(this.props.bytes) === -1 ? "-" : "+",
                    Math.abs(this.props.bytes).toLocaleString(),
                ].join(" ") : [
                    Math.abs(this.props.bytes).toLocaleString(),
                    Math.abs(this.props.bytes) !== 1 ? "bytes" : "byte",
                    Math.sign(this.props.bytes) === -1 ? "removed" : "added"
                ].join(" ")
            }
        </span>;
    }

}