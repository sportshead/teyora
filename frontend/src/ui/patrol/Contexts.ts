import * as React from "react";
import RecentChangesPatrolManager from "../../patrol/RecentChangesPatrolManager";
import UIPatrolWindow from "./UIPatrolWindow";

/**
 * Teyora Patrol Window Context interface.
 */
export interface TeyoraPatrolContext {

    patrolWindow : UIPatrolWindow;
    rcPatrolManager : RecentChangesPatrolManager;

}

/**
 * Teyora Patrol Window Context.
 */
export const UIPatrolContext = React.createContext(({
    patrolWindow: null,
    rcPatrolManager: null
} as TeyoraPatrolContext));