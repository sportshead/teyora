// Exports localised text for localised buttons
import i18n from "../i18n";

export interface LocalisedButtonTypes {
    OK: string;
    CANCEL: string;
    YES: string;
    NO: string;
    ADD: string;
    EDIT: string;
    DELETE: string;
    SAVE: string;
    CLOSE: string;
    BACK: string;
    NEXT: string;
    PREVIOUS: string;
    SEARCH: string;
    CLEAR: string
    RESET: string;
    SUBMIT: string;
    UPDATE: string;
    CREATE: string;
    DOWNLOAD: string;
    UPLOAD: string;
    SEND: string;
    PRINT: string;
    EXPORT: string;
    IMPORT: string;
    RELOAD: string;
    REFRESH: string;
    OPEN: string;
    READ: string;
    WRITE: string;
    EXECUTE: string;
    COPY: string;
    MOVE: string;
    RENAME: string;
    LOGOUT: string;
    SELECT: string;
    SKIP: string;
}
// Gets the localised buttons from ui.commonButtons and returns them as a LocalisedButtonTypes
export function LocalisedButtons() : LocalisedButtonTypes {
    return {
        OK: i18n.t("ui:commonButtons.ok"),
        CANCEL: i18n.t("ui:commonButtons.cancel"),
        YES: i18n.t("ui:commonButtons.yes"),
        NO: i18n.t("ui:commonButtons.no"),
        ADD: i18n.t("ui:commonButtons.add"),
        EDIT: i18n.t("ui:commonButtons.edit"),
        DELETE: i18n.t("ui:commonButtons.delete"),
        SAVE: i18n.t("ui:commonButtons.save"),
        CLOSE: i18n.t("ui:commonButtons.close"),
        BACK: i18n.t("ui:commonButtons.back"),
        NEXT: i18n.t("ui:commonButtons.next"),
        PREVIOUS: i18n.t("ui:commonButtons.previous"),
        SEARCH: i18n.t("ui:commonButtons.search"),
        CLEAR: i18n.t("ui:commonButtons.clear"),
        RESET: i18n.t("ui:commonButtons.reset"),
        SUBMIT: i18n.t("ui:commonButtons.submit"),
        UPDATE: i18n.t("ui:commonButtons.update"),
        CREATE: i18n.t("ui:commonButtons.create"),
        DOWNLOAD: i18n.t("ui:commonButtons.download"),
        UPLOAD: i18n.t("ui:commonButtons.upload"),
        SEND: i18n.t("ui:commonButtons.send"),
        PRINT: i18n.t("ui:commonButtons.print"),
        EXPORT: i18n.t("ui:commonButtons.export"),
        IMPORT: i18n.t("ui:commonButtons.import"),
        RELOAD: i18n.t("ui:commonButtons.reload"),
        REFRESH: i18n.t("ui:commonButtons.refresh"),
        READ: i18n.t("ui:commonButtons.read"),
        WRITE: i18n.t("ui:commonButtons.write"),
        EXECUTE: i18n.t("ui:commonButtons.execute"),
        COPY: i18n.t("ui:commonButtons.copy"),
        MOVE: i18n.t("ui:commonButtons.move"),
        RENAME: i18n.t("ui:commonButtons.rename"),
        OPEN: i18n.t("ui:commonButtons.open"),
        LOGOUT: i18n.t("ui:commonButtons.logout"),
        SELECT: i18n.t("ui:commonButtons.select"),
        SKIP: i18n.t("ui:commonButtons.skip"),
    };
}