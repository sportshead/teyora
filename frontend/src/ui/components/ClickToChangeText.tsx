// Lets a UI element be clicked to change a property
import { Button, Box, ClickAwayListener, Paper, Stack, TextField, Tooltip, InputAdornment, Typography } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { LocalisedButtons } from "../LocalisedButtons";

interface ClickToChangeTextProps {
    title: string;
    text: string;
    onChange: (newText: string) => void;
    characterLimit?: number; // Used for verification
    regex?: RegExp; // Used for verification
    children: JSX.Element;
}

export default function ClickToChangeText(props: ClickToChangeTextProps): JSX.Element {
    const { t } = useTranslation();
    const lB = LocalisedButtons();
    const [inputOpen, setInputOpen] = React.useState(false);
    const [inputText, setInputText] = React.useState(props.text);
    return (
        <ClickAwayListener onClickAway={()=>setInputOpen(false)}>
            <Box>
                {!inputOpen ? ( // Closed
                    <Tooltip title={t("ui:ClickToChangeText.tooltip")} placement="bottom-start">
                        <span style={{cursor: "pointer"}} onClick={()=>setInputOpen(!inputOpen)}>
                            {props.children}
                        </span>
                    </Tooltip>
                ): 
                    ( // Open
                        <Paper elevation={3} sx={{padding: "1vh", maxWidth: "50%"}}>
                            <Stack direction={"row"} spacing={1}>
                                <TextField
                                    fullWidth={true}
                                    label={props.title}
                                    variant="outlined"
                                    value={inputText}
                                    onChange={(e)=>{
                                        setInputText(e.target.value);
                                    }}
                                    size="small"
                                    inputProps={{
                                        maxLength: props.characterLimit * 2, // So we don't interrupt the user typing
                                    }}
                                />
                                {props.characterLimit && (
                                    <Typography variant="body2" sx={{
                                        color: (inputText.length > props.characterLimit) ? "red" : "text.secondary",
                                    }}>
                                        {props.characterLimit - inputText.length}
                                    </Typography>
                                )}

                                {/* Set button */}
                                <Button
                                    variant="text"
                                    disabled={
                                        (props.characterLimit && inputText.length > props.characterLimit) ||
                                        (props.regex && !props.regex.test(inputText))
                                    }
                                    onClick={()=>{
                                        props.onChange(inputText);
                                        setInputOpen(false);
                                    }}
                                >{lB.OK}</Button>
                            </Stack>
                        </Paper>
                    )
                }
                
            </Box>
        </ClickAwayListener>
    );
}