import { CheckCircle, GppGood, Pending, Remove, RemoveCircleOutline, Search } from "@mui/icons-material";
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, Tooltip, TableFooter, TablePagination, Box, InputAdornment, TextField, Typography, Grid, Alert, Button } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { defaultWikiSupportMap, WikiSupportInfo } from "../../../../common/src/objects/WikiSupportMap";
import Teyora from "../../App";
import { LocalisedButtons } from "../LocalisedButtons";

/* 
    Called during launch, this function sends a request to the server to get the support info for
    Wikis on this Teyora instance.
*/
export function getWikiSupportInfo(): Promise<Record<string, WikiSupportInfo>> {
    return new Promise((resolve, reject) => {
        // Send a request to the server to get the supported Wikis
        fetch("/api/wikisupport").then(response => { 
            // If the response is not ok, reject the promise with empty data, else resolve it.
            if (response.status !== 200) resolve({}); else response.json().then(data => {
                resolve(data);
            });
        }).catch(err => {
            // Request error that should be handled
            console.error(err);
            reject(err);
        });
    });
}

interface SupportIconProps {
    supportLevel: number;
}

function SupportIcon(props: SupportIconProps): JSX.Element {
    const { t } = useTranslation();

    return (
        <Tooltip title={
            [
                t("ui:WikiSelector.supportLevel.0"),
                t("ui:WikiSelector.supportLevel.1"),
                t("ui:WikiSelector.supportLevel.2"),
                t("ui:WikiSelector.supportLevel.3"),
                t("ui:WikiSelector.supportLevel.4")
            ][props.supportLevel]
        }>
            {
                ([
                    // eslint-disable-next-line react/jsx-key
                    <Remove htmlColor="gray" />, // No support
                    // eslint-disable-next-line react/jsx-key
                    <RemoveCircleOutline htmlColor="gray"/>, // Partial support
                    // eslint-disable-next-line react/jsx-key
                    <Pending htmlColor="blue" />, // Incomplete/in progress
                    // eslint-disable-next-line react/jsx-key
                    <CheckCircle htmlColor="green"/>,
                    // eslint-disable-next-line react/jsx-key
                    <GppGood htmlColor="green"/>
                ])[props.supportLevel]
            }
        </Tooltip>
    );
}

interface WikiSelectorProps {
    // eslint-disable-next-line no-unused-vars
    onSelect: (wikiKey: string) => void;
}

export default function WikiSelector(props: WikiSelectorProps): JSX.Element {
    const { t } = useTranslation();
    const lB = LocalisedButtons();
    const [rowOffset, setRowOffset] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(15);
    const [filterText, setFilterText] = React.useState(t("ui:WikiSelector.DefaultLocaleSearchValue").toLocaleLowerCase());



    // eslint-disable-next-line prefer-const
    let rows:JSX.Element[] = [];

    // Get filered Wikikeys
    const wikiKeys = Object.keys(Teyora.TY.WikiSupport).filter(key => {
        const wiki = Teyora.TY.WikiSupport[key];
        // Filter if it contains the language name, the Wiki name, or the URL, or key (enwiki, etc.)
        return (wiki.langName && (wiki.langName + " " + wiki.name).toLocaleLowerCase().includes(filterText)) ||
            (wiki.url && wiki.url.toLocaleLowerCase().includes(filterText)) ||
            (wiki.name && wiki.name.toLocaleLowerCase().includes(filterText)) ||
            (key.toLocaleLowerCase().includes(filterText));
    }); 

    for(let i = rowOffset; (rows.length <= rowsPerPage) && (i < wikiKeys.length); i++) {
        const wikiKey = wikiKeys[i];
        const wiki = Teyora.TY.WikiSupport[wikiKey];
        if (!wiki.supportMap) wiki.supportMap = defaultWikiSupportMap;
        rows.push(
            <TableRow
                key={wikiKey}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
                <TableCell component="th" scope="row">
                    {(wiki.langName !== "Special") && wiki.langName} {wiki.name}
                    <br />
                    <a href={wiki.url} target="_blank" rel="noopener noreferrer">{wiki.url}</a>
                </TableCell>
                <TableCell align="right"><SupportIcon supportLevel={wiki.supportMap.supportsORES} /></TableCell>
                <TableCell align="right"><SupportIcon supportLevel={wiki.supportMap.supportsWarning} /></TableCell>
                <TableCell align="right"><SupportIcon supportLevel={wiki.supportMap.supportsReporting} /></TableCell>
                <TableCell align="right"><SupportIcon supportLevel={wiki.supportMap.supportsBlocking} /></TableCell>
                <TableCell align="right"><SupportIcon supportLevel={wiki.supportMap.supportsRevert} /></TableCell>
                <TableCell align="right"><SupportIcon supportLevel={wiki.supportMap.supportsDiscussions} /></TableCell>
                <TableCell align="right">
                    <Button onClick={()=>props.onSelect(wikiKey)}>
                        {lB.SELECT}
                    </Button>
                </TableCell>
            </TableRow>
        );
    }

    return (
        <Box>
            <Grid container spacing={2}>
                <Grid item xs={6} md={8}>
                    {/* Select Wiki */}
                    <Typography variant="h6">{t("ui:WikiSelector.title")}</Typography>
                    <Typography variant="body2">{t("ui:WikiSelector.description")}</Typography>
                </Grid>
                <Grid item xs={6} md={4}>
                    {/* Search box */}
                    <TextField
                        label={lB.SEARCH}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Search />
                                </InputAdornment>
                            ),
                            value: filterText
                        }}
                        variant="outlined"
                        fullWidth={true}
                        
                        onChange={(e) => {  
                            // Set the filter text                          
                            setFilterText(e.target.value.toLocaleLowerCase());
                            setRowOffset(0);
                        }}

                    />
                </Grid>
            </Grid>
        
            <Box sx={{
                paddingTop: "2vh" 
            }}>
                {   // If the results are empty, show a message, else show the table
                    rows.length === 0 ? <Alert severity="info" variant="outlined">
                        {t("ui:WikiSelector.NoWikisFound")}
                    </Alert> :
                        <TableContainer component={Paper} sx={{ maxHeight: "50vh"}}>
                            <Table stickyHeader>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>{t("ui:WikiSelector.WikiNameRow")}</TableCell>
                                        <TableCell align="right">{t("ui:WikiSelector.supportsORES")}</TableCell>
                                        <TableCell align="right">{t("ui:WikiSelector.supportsWarning")}</TableCell>
                                        <TableCell align="right">{t("ui:WikiSelector.supportsReporting")}</TableCell>
                                        <TableCell align="right">{t("ui:WikiSelector.supportsBlocking")}</TableCell>
                                        <TableCell align="right">{t("ui:WikiSelector.supportsRevert")}</TableCell>
                                        <TableCell align="right">{t("ui:WikiSelector.supportsDiscussions")}</TableCell>
                                        <TableCell align="right">{/* Empty cosmetic cell for select */}</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        rows
                                    }
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            rowsPerPageOptions={[15, 30, 50]}
                                            colSpan={8}
                                            count={wikiKeys.length}
                                            rowsPerPage={rowsPerPage}
                                            page={Math.round(rowOffset / rowsPerPage)}
                                            labelRowsPerPage={t("ui:WikiSelector.wikisPerPage")}
                                            labelDisplayedRows={(trans) => t("ui:WikiSelector.wikiListInfo", trans)}
                                
                                            onPageChange={(event, page) => {
                                                if ((page * rowsPerPage) > rowOffset) {
                                                // Increase
                                                    if (rowOffset + rowsPerPage > wikiKeys.length) {
                                                        setRowOffset(wikiKeys.length - rowsPerPage);
                                                    } else {
                                                        setRowOffset(rowOffset + rowsPerPage);
                                                    }
                                                } else {
                                                // Decrease
                                                    if (rowOffset - rowsPerPage < 0) setRowOffset(0);
                                                    setRowOffset(rowOffset - rowsPerPage);
                                                }
                                            }}

                                            onRowsPerPageChange={event => {
                                                setRowOffset(0);
                                                setRowsPerPage(+event.target.value);
                                            }}
                                        />
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </TableContainer>
                }
            </Box>
        </Box>
  
    );
}