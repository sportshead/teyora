// A full page divs, to be displayed under dialog only interfaces
import React from "react";

interface FullPageImageProps {
    image: string;
}

export default class FullPageImage extends React.Component<FullPageImageProps> {
    render() {
        return (
            <div className="full-page-image" style={{backgroundImage: `url("${this.props.image}")`}}></div>
        );
    }
}
