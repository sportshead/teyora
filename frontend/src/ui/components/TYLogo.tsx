import * as React from "react";
import {TeyoraWordmark, TeyoraWordmarkDark, TeyoraWordmarkWhite} from "../../Resources";
import { useTheme } from "@mui/material/styles";

/**
 * An image element containing the Teyora wordmark.
 **/
export function TYWordmark() : JSX.Element {
    return <img className={"TY-logo TY-logo-increasedSize"} src={TeyoraWordmark} alt={"Teyora logo"}/>;
}

/**
 * An image element containing the Teyora wordmark (for dark backgrounds).
 **/
export function TYWordmarkDark() : JSX.Element {
    return <img className={"TY-logo TY-logo-dark"} src={TeyoraWordmarkDark} alt={"Teyora logo"}/>;
}

/**
 * An image element containing the Teyora wordmark (for dark backgrounds).
 **/
export function TYWordmarkWhite() : JSX.Element {
    return <img className={"TY-logo TY-logo-increasedSize"} src={TeyoraWordmarkWhite} alt={"Teyora logo"}/>;
}

/**
 * An image element containing the Teyora wordmark (switches depending on theme type).
 **/
export function TYWordmarkAuto() : JSX.Element {
    const classes = useTheme();
    if (classes.palette.mode === "dark")
        return TYWordmarkWhite(); // Don't use the colored logo in UI, it looks terrible
    else
        return TYWordmarkDark();
}

/**
 * An image element containing the Teyora wordmark (switches depending on theme type).
 *
 * Haven't actually tested this wordmark yet.
 **/
export function TYWordmarkPrimaryContrasted() : JSX.Element {
    const classes = useTheme();

    /* Parse color to RGB */
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    const color = classes.palette.primary.main.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);

    /* Check luminance and return respective logo. */
    if (result) {
        return ((0.2126 * +(parseInt(result[1], 16)))
            + (0.7152 * +(parseInt(result[2], 16)))
            + (0.0722 * +(parseInt(result[3], 16)))) > 128 ?
            TYWordmarkDark() : TYWordmarkWhite();
    } else
        return TYWordmarkDark();
}