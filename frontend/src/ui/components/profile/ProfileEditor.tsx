// The profile editor element
import { Alert, Box, Button, Dialog, DialogContent, DialogTitle, Grid, IconButton, Paper, Stack, TextField, Tooltip, Typography } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { ProfileEntry } from "../../../../../backend/src/db/dbcollections/profile";
import { UserEntry } from "../../../../../backend/src/db/dbcollections/user";
import ProfilePicture, {ProfilePictures} from "./ProfilePicture";
import UserChips from "./UserChips";
import ClickToChangeText from "../ClickToChangeText";
import Username from "./Username";
import Teyora from "../../../App";
import TeyoraUI from "../../TeyoraUI";

interface ProfilePictureSelectorProps {
    open: boolean;
    onClose: () => void;
    onSelect: (SelectedPicture: string) => void;
}

export function ProfilePictureSelector(props: ProfilePictureSelectorProps): JSX.Element {
    const { t } = useTranslation();
    const { open, onClose, onSelect } = props;
    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>{t("ui:ProfileEditor.profilePictureSelector.title")}</DialogTitle>
            <DialogContent>
                <Grid container>
                    {ProfilePictures.map((picture, index) => (
                        <Grid item xs={2} key={index}>
                            <IconButton onClick={()=>{
                                onSelect(picture);
                            }}>
                                <ProfilePicture
                                    profilePicture={ picture }
                                    username={ "Selector" } // this isn't seen, but it's required
                                    size={64}
                                />
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>
            </DialogContent>
        </Dialog>
    );
}

interface ProfileEditorProps {
    user: UserEntry,
    profile: ProfileEntry,
    onSave: (profile: ProfileEntry) => void
}

export default function ProfileEditor(props: ProfileEditorProps): JSX.Element {
    // If no profile is provided, create a new one (needs typing)
    if (props.profile.profile == null) props.profile.profile = {};

    const { t } = useTranslation();
    const [profile, setProfile] = React.useState(props.profile);
    const [PfpSelectOpen, setPfpSelectOpen] = React.useState(false);
    const [bio, setBio] = React.useState(props.profile.profile.bio || "");


    // Finally, return the selector
    return (
        // Wrapper
        <Paper>
            {/* Profile picture selector */}
            <ProfilePictureSelector
                open={PfpSelectOpen}
                onSelect={(SelectedPicture: string) => {
                    profile.profile.profilePicture = SelectedPicture;
                    setPfpSelectOpen(false);
                    setProfile(profile);
                }}
                onClose={() => {
                    setPfpSelectOpen(false);
                }}
            />

            {/* Top box (profile picture, name, tags, pronouns) */}
            <Box sx={ {
                padding: "1rem",
                width: "100%"
            }}>
                <Grid container spacing={1}>
                    {/* Profile picture container */}
                    <Grid item
                        xs={1}
                        sx={
                            {
                                justifyContent: "center",
                                alignItems: "center",
                                display: "flex"
                            }       
                        }
                    >   
                        {/* Actual Profile picture */}
                        <Tooltip title={t("ui:ProfileEditor.setProfilePicture")}>
                            <IconButton onClick={()=>{
                                setPfpSelectOpen(true);
                            }}>
                                <ProfilePicture
                                    profilePicture={ profile.profile ? profile.profile.profilePicture : undefined }
                                    username={props.user.username.split(":")[1]}
                                    size={64}
                                />
                            </IconButton>
                        </Tooltip>
                    </Grid>

                    {/* Name, tags and pronouns */}
                    <Grid item xs={11}>
                        {/* Nick name */}
                        <ClickToChangeText
                            title={t("ui:ProfileEditor.nickname")}
                            text={profile.profile.nickname || props.user.username.split(":")[1]}
                            onChange={(newText: string) => {
                                // If the user isn't approved, show a TeyoraUI alert
                                if (!props.user.isApproved) {
                                    TeyoraUI.showAlertBox(
                                        t("ui:ProfileEditor.NotApproved"),
                                        t("ui:ProfileEditor.NotApprovedTitle"),
                                    );
                                    return;
                                }

                                // eslint-disable-next-line prefer-const
                                let newP = profile;
                                newP.profile.nickname = newText;
                                setProfile(newP);
                            }}
                            characterLimit={40} // Based on Wiki usernames, data validated server side
                        >
                            <Username username={props.user.username.split(":")[1]} profile={profile} />
                        </ClickToChangeText>

                        <UserChips user={props.user} />
                        
                        {/* Only approved users can change bio and nickname */}
                        { (Teyora.TY.CurrentUser.isApproved) ? (
                            <TextField
                                fullWidth={true}
                                label={t("ui:ProfileEditor.bio")}
                                margin="dense"
                                multiline
                                rows={4}
                                maxRows={4}
                                value={bio}
                                onChange={(e) => {
                                    if (e.target.value.length > 10000) throw new Error("Bio is way too long"); // this basically acts as a hard limit
                                    setBio(e.target.value);
                                }}
                            />
                        ) : (
                            <Alert severity="info" sx={{paddingTop:"1vh"}}>
                                {t("ui:ProfileEditor.NotApproved")}
                            </Alert>
                        )}

                        <Stack
                            direction="column"
                            justifyContent="center"
                            alignItems="flex-end"
                            spacing={2}
                        >
                            { // Bio remaining length (approved users only)
                                (Teyora.TY.CurrentUser.isApproved) && (
                                    <Typography variant="body2" sx={{
                                        color: bio.length > 240 ? "red" : "textSecondary",
                                    }}>
                                        {240 - bio.length}
                                    </Typography>
                                )
                            }
                            
                            {/* Save button */}
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    // Basic validation
                                    if (bio.length > 240) {
                                        TeyoraUI.showAlertBox(
                                            t("ui:ProfileEditor.bioTooLong"),
                                            t("ui:ProfileEditor.bioTooLongTitle"),
                                        );
                                        return;
                                    }
                                    
                                    // Set the bio
                                    profile.profile.bio = bio;

                                    // Save the profile
                                    props.onSave(profile);
                                }}
                            >
                                {t("ui:ProfileEditor.save")}
                            </Button>
                        </Stack>

                    </Grid>
                </Grid>
            </Box>
        </Paper>
    );
}