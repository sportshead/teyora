import { Stack, Typography } from "@mui/material";
import * as React from "react";
import { ProfileEntry } from "../../../../../backend/src/db/dbcollections/profile";

interface usernameProps {
    username: string;
    profile?: ProfileEntry;
}

export default function Username(props: usernameProps): JSX.Element {
    const { username, profile } = props;
    return (
        <Stack spacing={1} direction={"row"}>
            <Typography variant="h5">
                {profile.profile.nickname || username}
            </Typography>

            { // If they have a nickname, show username in small
                profile.profile.nickname && (<Typography variant="body2">
                    {username}
                </Typography>)
            }
        </Stack>
    );
}