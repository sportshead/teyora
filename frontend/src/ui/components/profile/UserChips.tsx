// Generate chips for the current user
// Todo: Add option for Wiki information rather than just Teyora user information, however this will only
// work in patrol mode
import * as React from "react";
import { Chip, Stack, Tooltip } from "@mui/material";
import { UserEntry } from "../../../../../backend/src/db/dbcollections/user";
import { useTranslation } from "react-i18next";
import { AccountCircle, AdminPanelSettings, Domain, Lock, LockOpen, ManageAccounts, NoAccounts, Verified } from "@mui/icons-material";

interface UserChipsProps {
    user: UserEntry
}

export default function UserChips(props: UserChipsProps): JSX.Element {
    const { t } = useTranslation();
    const user = props.user;

    // In order of appearance - online chip is dealt with below
    const chipTests = [
        // Suspended chip
        {
            test: user.isSuspended,
            label: t("ui:UserChips.suspended"),
            icon: (<NoAccounts />),
            description: t("ui:UserChips.suspendedDescription")
        },

        // Verified chip
        {
            test: user.isVerified,
            icon: <Verified />,
            label: t("ui:UserChips.verified"),
            description: t("ui:UserChips.verifiedDescription")
        },

        // SuperOverlord chip
        {
            test: user.userID === "SuperOverlord",
            label: t("ui:UserChips.superOverlord"),
            icon: (<Domain />),
            description: t("ui:UserChips.superOverlordDescription")
        },

        // Overlord chip
        {
            check: user.isOverlord,
            label: t("ui:UserChips.overlord"),
            icon: (<ManageAccounts />),
            description: t("ui:UserChips.overlordDescription")
        },

        // Admin chip
        {
            test: user.isAdmin,
            label: t("ui:UserChips.admin"),
            icon: (<AdminPanelSettings />),
            description: t("ui:UserChips.adminDescription")
        },

        // Approved chip
        {
            test: user.isApproved,
            label: t("ui:UserChips.approved"),
            icon: (<LockOpen />),
            description: t("ui:UserChips.approvedDescription")
        },

        // Registered chip (i.e. passed first time setup)
        {
            test: !user.isLocked,
            label: t("ui:UserChips.unlocked"),
            icon: (<AccountCircle />),
            description: t("ui:UserChips.unlockedDescription")
        },

        // Locked chip
        {
            test: user.isLocked,
            label: t("ui:UserChips.locked"),
            icon: (<Lock />),
            description: t("ui:UserChips.lockedDescription")
        },


    ];

    return (
        <Stack
            direction="row"
            spacing={1}
            sx={{
                paddingTop: "1vh",
                paddingBottom: "1vh",
            }}
        >
            {/* Show an online chip if the last online date is within the past 5 min */}
            {
                user.lastOnline && new Date(user.lastOnline).getTime() > Date.now() - (5 * 60 * 1000)
                    ? (
                        <Tooltip title={t("ui:UserChips.onlineDescription")}>
                            <Chip
                                label={t("ui:UserChips.online")}
                                variant="outlined"
                                color="success"
                            />
                        </Tooltip>
                    )
                    : null
            }
            
            {chipTests.map((chipTest) => {
                if (chipTest.test) {
                    return (
                        <Tooltip title={chipTest.description}>
                            <Chip label={chipTest.label} icon={chipTest.icon} variant={"outlined"} />
                        </Tooltip>
                    );
                }
            })}
        </Stack>
    );
}