/*
Contains the list of available profile pictures and a generator for one
*/
import { Avatar } from "@mui/material";
import { amber, blue, blueGrey, deepOrange, deepPurple, green, grey, indigo, lightGreen, lime, orange, pink, red } from "@mui/material/colors";
import * as React from "react";

// IDs of the available profile pictures
export const ProfilePictures: string[] = [
    "ACoolTower",
    "BackseatDriver",
    "BrightonKite",
    "Caves",
    "ChooChoo",
    "Church",
    "Countryside",
    "DefoShouldBeHere",
    "DontLeafMe",
    "DownhillFromHere",
    "DumpRamp",
    "Flower1",
    "Flower2",
    "FromTheBridge",
    "GravesDownSouth",
    "HeyLookAnotherSunset",
    "Midnight",
    "NotRunDown",
    "OrangeTriangles",
    "Photographer",
    "Plant",
    "Pride",
    "Ramp",
    "RedTunnel",
    "RingRing",
    "Road",
    "Roots",
    "StreetCat",
    "Sunset",
    "SwanCanal",
    "TimeToGo",
    "Tube",
    "Tubes",
    "WhereAmI",
    "YellowCar"
];

// For no profile picture, a list of first letters and their corresponding colours
const ProfileColours: Record<string, string> =({
    A: indigo[500],
    B: blue[500],
    C: deepOrange[500],
    D: pink[500],
    E: deepPurple[500],
    F: lime[500],
    G: green[500],
    H: lightGreen[500],
    I: amber[500],
    J: orange[500],
    K: red[500],
    L: grey[500],
    M: blueGrey[500],
    N: indigo[500],
    O: blue[500],
    P: deepOrange[500],
    Q: pink[500],
    R: deepPurple[500],
    S: lime[500],
    T: green[500],
    U: lightGreen[500],
    V: amber[500],
    W: orange[500],
    X: red[500],
    Y: grey[500],
    Z: blueGrey[500],
    other: grey[500]
});

// Finally, the profile picture element

interface ProfilePictureProps {
    username: string;
    profilePicture?: string;
    size: number;
}

export default function ProfilePicture(props: ProfilePictureProps): JSX.Element {
    const { username, profilePicture, size } = props;
    const profileColour = ProfileColours[username.charAt(0).toUpperCase()] || ProfileColours.other;

    return <Avatar
        alt={username}
        sx={{
            bgcolor: profileColour,
            height: size,
            width: size
        }}
        src={profilePicture ? `/images/profilePictures/${profilePicture}.jpg` : undefined}
    >
        {!profilePicture && username.charAt(0).toUpperCase()}
    </Avatar>;
}