// A bodged confetti animation
// Typing isn't strict here, as copied from https://github.com/ulitcos/react-canvas-confetti#readme
// MIT License
// Copyright (c) 2020 Ruslan Krokhin

import React from "react";
import ReactCanvasConfetti from "react-canvas-confetti";

interface CelebrationProps {
    animationInstance?: any;
}

export default class Realistic extends React.Component {
    animationInstance: any;

    constructor(props: CelebrationProps) {
        super(props);
        this.animationInstance = null;
    }

    makeShot = (particleRatio: number, opts: any) => {
        this.animationInstance && this.animationInstance({
            ...opts,
            origin: { y: 0.7 },
            particleCount: Math.floor(200 * particleRatio),
        });
    };

    fire = () => {
        this.makeShot(0.25, {
            spread: 26,
            startVelocity: 55,
        });

        this.makeShot(0.2, {
            spread: 60,
        });

        this.makeShot(0.35, {
            spread: 100,
            decay: 0.91,
            scalar: 0.8,
        });

        this.makeShot(0.1, {
            spread: 120,
            startVelocity: 25,
            decay: 0.92,
            scalar: 1.2,
        });

        this.makeShot(0.1, {
            spread: 120,
            startVelocity: 45,
        });
    };

    handlerFire = () => {
        this.fire();
    };

    getInstance = (instance: any) => {
        this.animationInstance = instance;
    };

    render() {
        // Fire after 0.5 seconds
        setTimeout(this.handlerFire, 500);

        return (
            <>
                <ReactCanvasConfetti refConfetti={this.getInstance} style={
                    {
                        position: "fixed",
                        pointerEvents: "none",
                        width: "100%",
                        height: "100%",
                        top: 0,
                        left: 0
                    }
                }/>
            </>
        );
    }
}