import {createTheme, DeprecatedThemeOptions, Theme as MUITheme, adaptV4Theme, ThemeOptions} from "@mui/material/styles";

export const LightTheme : DeprecatedThemeOptions = {
    /* YellowxGrey Teyora themes (default) - see https://material.io/resources/color */
    palette: {
        mode: "light",
        primary: { // Yellow
            light: "#fffd61",
            main: "#ffca28",
            dark: "#c79a00",
            contrastText: "#000"
        },
        secondary: { // Blue-grey
            light: "#718792",
            main: "#455a64",
            dark: "#1c313a",
            contrastText: "#fff"
        }
    }
};

// A light theme with a red primary color and a pink secondary color
export const LightThemePink : DeprecatedThemeOptions = {
    palette: {
        mode: "light",
        primary: {
            light: "#ff7961",
            main: "#f44336",
            dark: "#ba000d",
            contrastText: "#fff"
        },
        secondary: {
            light: "#ff4081",
            main: "#e91e63",
            dark: "#9b0007",
            contrastText: "#000"
        }
    }
};

// A light theme that uses a random primary and secondary color
export const LightThemeRandom : DeprecatedThemeOptions = {
    palette: {
        mode: "light",
        primary: {
            light: "#" + Math.floor(Math.random() * 16777215).toString(16),
            main: "#" + Math.floor(Math.random() * 16777215).toString(16),
            dark: "#" + Math.floor(Math.random() * 16777215).toString(16),
            contrastText: "#000"
        },
        secondary: {
            light: "#" + Math.floor(Math.random() * 16777215).toString(16),
            main: "#" + Math.floor(Math.random() * 16777215).toString(16),
            dark: "#" + Math.floor(Math.random() * 16777215).toString(16),
            contrastText: "#fff"
        }
    }
};

export const DarkTheme : ThemeOptions = {
    palette: {
        mode: "dark",
        primary: {
            main: "#ffc107",
        },
        secondary: {
            main: "#424242",
        },
        background: {
            default: "#212121",
        },
    }
};

enum TeyoraTheme {
    LightTheme,
    DarkTheme
}

// Sets the theme to light or dark based on what the browser prefers
export const DefaultTheme = (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) ? TeyoraTheme.DarkTheme : TeyoraTheme.LightTheme;

export default TeyoraTheme;

// Todo: Swap for user preference for this workspace
export function getTheme(theme : TeyoraTheme) : MUITheme {
    switch (theme) {
        case TeyoraTheme.LightTheme:
            return createTheme(adaptV4Theme(LightTheme));
        case TeyoraTheme.DarkTheme:
            return createTheme(DarkTheme);
        default:
            return createTheme(DarkTheme);
    }
} 