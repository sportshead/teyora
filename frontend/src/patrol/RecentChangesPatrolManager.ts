import { TeyoraEditInfo } from "../../../common/src/objects/TeyoraEditInfo";
import RecentChangesWebSocketClient from "../ws/RecentChangesWebSocketClient";

export default class RecentChangesPatrolManager {

    /** The WebSocket client **/
    recentChangeSocketClient : RecentChangesWebSocketClient;
    /** The recent change entries **/
    entries : TeyoraEditInfo[] = [];

    private entryAddedCallbacks : ((entry : TeyoraEditInfo) => void)[] = [];
    private entryRemovedCallbacks : ((entry : TeyoraEditInfo) => void)[] = [];

    constructor() {
        const websocketURL =
            `${window.location.protocol === "https:" ? "wss" : "ws"}://${window.location.host}/ws/teyora`;
        this.recentChangeSocketClient = new RecentChangesWebSocketClient(
            new WebSocket(websocketURL),
            this
        );
    }

    enumerate() : TeyoraEditInfo[] {
        return this.entries;
    }

    addEntry(entry : TeyoraEditInfo) : void {
        // If entries list is too long (50 edits), pop the last item
        if (this.entries.length > 49) this.entries.pop();
        
        this.entries.splice(0, 0, entry);
        this.entryAddedCallbacks.forEach(v => {
            v(entry);
        });
    }

    removeEntry(entry : TeyoraEditInfo) : void {
        this.entries.filter(e => e !== entry);
        this.entryRemovedCallbacks.forEach(v => {
            v(entry);
        });
    }

    registerCallback(
        action : "add" | "remove",
        callback : ((entry : TeyoraEditInfo) => void)) : void {
        switch(action) {
            case "add":
                this.entryAddedCallbacks.push(callback);
                break;
            case "remove":
                this.entryRemovedCallbacks.push(callback);
                break;
        }
    }

    // Leijurv, save me from this hell.

    deregisterCallback(
        action : "add" | "remove",
        callback : ((entry : TeyoraEditInfo) => void)) : void {
        switch(action) {
            case "add":
                this.entryAddedCallbacks = this.entryAddedCallbacks.filter(c => c !== callback);
                break;
            case "remove":
                this.entryRemovedCallbacks = this.entryRemovedCallbacks.filter(c => c !== callback);
                break;
        }
    }

}