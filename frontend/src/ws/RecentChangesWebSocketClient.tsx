import WSMS from "../../../common/src/ws/WebSocketMessageSerializer";
import {Direction, messageMatcherServerToClient} from "../../../common/src/ws/WebSocketMessage";
import {TY_FRONTEND_IDENTIFIER, TY_FRONTEND_VERSION} from "../../../common/src/Constants";
import {TY_GIT_HASH} from "../../../common/src/DynamicConstants";
import RecentChangesPatrolManager from "../patrol/RecentChangesPatrolManager";
import TeyoraUI from "../ui/TeyoraUI";

/**
 * This class handles all WebSocket communication with the Teyora Backend (TYB).
 *
 * All of the messages serialized and deserialized here are defined the in TY
 * Common files. This makes standardization between client and server easy.
 *
 * This should be loaded in as soon as the page is loaded, since there are
 * controls that can adjust what or how much data is transferred.
 */
export default class RecentChangesWebSocketClient {

    /** The websocket connection with TYB. **/
    ws : WebSocket;
    /** The RecentChangesPatrolManger handling the recent changes. **/
    patrolManager : RecentChangesPatrolManager;

    /**
     * Creates a new RecentChangesWebSocketClient.
     *
     * @param ws The websocket used for data transfer.
     * @param patrolManager The parent recent changes patrol manager.
     */
    constructor(ws : WebSocket, patrolManager : RecentChangesPatrolManager) {
        this.ws = ws;
        this.patrolManager = patrolManager;

        /* When we receive a message, */
        this.ws.addEventListener("message", (messageEvent) => {
            this.wsConnectionMessage(messageEvent);
        });

        /* When the connection is closed unexpectedly, */
        this.ws.addEventListener("close", () => {
            // This might be critical, so we may want to turn this into an error dialog
            TeyoraUI.showSnackbar(
                "The Teyora live feed connection has been closed. Click the play button to try and reconnect.",
                { variant: "error" }
            );
        });

        /* When the socket opens, */
        this.ws.addEventListener("open", () => {
            console.log("Opened websocket connection.");
            console.log("Sending handshake...");

            // Send a handshake to ensure compatibility between the TYB and
            // the client.
            ws.send(WSMS.serializeClient({
                direction: Direction.ClientToServer,
                type: "handshake",
                clientVersion: TY_FRONTEND_VERSION,
                clientIdentifier: TY_FRONTEND_IDENTIFIER,
                clientCommitHash: TY_GIT_HASH,

                // TODO: Authentication here.
                token: "",
                targetWikis:[ "simplewiki", "eswiki", "zhwiki" ], // use only testwiki or enwiki
            }));
        });
    }

    /** The message matcher used to handle messages to the WebSocket. **/
    messageMatcher = messageMatcherServerToClient<void>({
        handshake: (message) => {
            console.log(`Connected to Teyora Backend v${message.serverIdentifier}`);
            TeyoraUI.showSnackbar(
                "Welcome, you're logged in and connected to Teyora.",
                { variant: "success" }
            );
        },

        handshakedone: (message) => {
            if (message.warnings.length !== 0) {
                console.warn("Handshake warnings caught.");
                console.dir(message.warnings);
            }
        },

        error: (message) => {
            console.error("Websocket experienced error.");
            if (message.fatal) {
                console.error("RCWebSocket error was fatal. Won't continue without user intervention.");
                // TODO Show popup to user.
            } else {
                console.error(message.reason);
                console.dir(message.extra);
                TeyoraUI.showSnackbar(
                    "The Teyora live feed connection reported an error. Click the play button to try and reconnect.",
                    { variant: "error" }
                );
            }
        },

        // For live edit feeds
        liveEdit: (message) => {
            // TODO: handle hiding edits here and also logging out requests
            this.patrolManager.addEntry(message.change);
        }
    });

    /**
     * This function is callled when the WS gets a message. Since we can assume
     * that the message is always serialized, we'll just directly serialize it.
     * @param messageEvent The WS MessageEvent.
     */
    wsConnectionMessage(messageEvent : MessageEvent) : void {
        this.messageMatcher(WSMS.deserializeClient(messageEvent.data));
    }

}