import * as React from "react";
import * as ReactDOM from "react-dom";
import { ErrorBoundary } from "react-error-boundary";
import * as Sentry from "@sentry/browser"; // Sentry error reporting
import AppCrash from "./ui/dialogs/AppCrash";
import PublicHomePage from "./ui/auth/PublicHomePage";
import AuthorizationManager from "./auth/AuthorizationManager";
import { ThemeProvider } from "@mui/material/styles";
import TeyoraTheme, {DefaultTheme, getTheme} from "./ui/Themes";
import { TY_FRONTEND_SPLASH_DELAY, TY_SENTRY_DSN } from "../../common/src/Constants"; // constants
import SettingsManager from "./settings/SettingsManager";
import { SnackbarProvider } from "notistack";
import SnackbarHandler from "./ui/components/SnackbarHandler";
import TeyoraUI from "./ui/TeyoraUI";
import { UserEntry } from "../../backend/src/db/dbcollections/user";
import MessageBox from "./ui/dialogs/MessageBox";
import MessageBoxQueue from "./ui/dialogs/MessageBoxQueue";
import i18n, { initLangs } from "./i18n"; // Internationalisation
import FirstTimeSetup from "./ui/auth/FirstTimeSetup/Main";
import { LocalisedButtons } from "./ui/LocalisedButtons";
import { WikiSupportInfo } from "../../common/src/objects/WikiSupportMap";
import { getWikiSupportInfo } from "./ui/components/WikiSelector";
import { Backdrop, CircularProgress, CssBaseline } from "@mui/material";
import { ProfileEntry } from "../../backend/src/db/dbcollections/profile";

// Interfaces

/**
 *  The properties interface used by the {@link Teyora} component.
 **/
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ITYProps {
    authManager: AuthorizationManager,
    currentUser: UserEntry,
    currentProfile: ProfileEntry,
    wikiSupport: Record<string, WikiSupportInfo>,
}

/**
 * The state interface used by the {@link Teyora} component.
 **/
interface ITYState {

    theme : TeyoraTheme;
    activeWindow : JSX.Element;
    isFullPageLoaderOpen : boolean;

}

/**
 * The properties interface used by Teyora windows. This allows windows the opportunity to
 * grab properties from the main Teyora "app".
 * 
 * className
 * message (some dialogs only)
 **/
export interface ITYWindowProps {

    className? : string;
    message? : string;
    // eslint-disable-next-line no-unused-vars
    t? : (key : string) => string;
    i18n?: typeof i18n

}

/**
 * The main Teyora class. This class represents the entire Teyora app as a
 * whole, and manages renders to the browser screen.
 **/
export default class Teyora extends React.Component<ITYProps, ITYState> {

    static TY : Teyora;

    public authManager : AuthorizationManager;

    public MsgBoxQueue: MessageBoxQueue;

    public CurrentUser: UserEntry;

    public CurrentProfile: ProfileEntry;

    public WikiSupport: Record<string, WikiSupportInfo>;

    // eslint-disable-next-line no-unused-vars
    public SetFullPageLoaderOpen: (setOpen : boolean) => void = (setOpen) => {
        this.setState({isFullPageLoaderOpen: setOpen});
    };


    private dead : boolean;

    static setTeyoraGlobal(TY : Teyora) : void {
        Teyora.TY = TY;
    }

    constructor(props : ITYProps, context : unknown) {
        super(props, context);

        // Set currentuser on props
        this.CurrentUser = props.currentUser;

        // Set current profile on props
        this.CurrentProfile = props.currentProfile;

        // Set WikiSupport on props
        this.WikiSupport = props.wikiSupport;

        // Set authManager on props
        this.authManager = props.authManager;

        // There can only be one Teyora.
        if (Teyora.TY != null && !Teyora.TY.dead) {
            console.error("Two Teyora instances cannot exist at the same time!");
            this.dead = true;
        } else if (Teyora.TY != null) {
            console.error("Old but dead Teyora instance detected. That instance will be replaced.");
            Teyora.setTeyoraGlobal(this);
        } else {
            Teyora.setTeyoraGlobal(this);
        }

        // Set the state.
        this.state = {
            theme: DefaultTheme,
            activeWindow: this.determineFirstWindow() ,
            isFullPageLoaderOpen: false,
        };
    }

    /**
     * Determines the first window that Teyora will show the user. If the user is already
     * authorized (a.k.a. they have logged in with their Wikipedia account), then they should
     * be automatically forwarded to the patrol screen.
     *
     * @returns The first window to show the user (a JSX Element).
     **/
    public determineFirstWindow() : JSX.Element {
        // If not authorized, show the login window.
        if (this.CurrentUser == null) {
            return <PublicHomePage />;
        }

        if (this.CurrentUser.isSuspended) return <this.AccountSuspendedError />;

        // Check profile exists, or account is locked (i.e. not proven eligible)
        if (this.CurrentUser.isLocked || Object.keys(this.CurrentProfile).length === 0) return <FirstTimeSetup />;

        // We're logged in
        return <FirstTimeSetup />; // temporary
        
        //return <UIPatrolWindow />;
        
        // Backup in case no other checks pass (very rare but will handle)
        throw new Error("No first window could be determined.");
    }

    public AccountSuspendedError() : JSX.Element {
        return <PublicHomePage loginError="login:siteNotices.suspendedAccount" hideLoginButton={true} />;
    }

    // Application components.

    /**
     * Renders the current active element.
     **/
    public render() : JSX.Element {
        // Planned actions:
        // 1. [ ] Load/generate user settings
        // 2. [ ] Download required cached materials
        //    a. [ ] Download misc. cached materials
        //    b. [X] Inject MUI theme into DOM.
        // 3. [X] Setup interface
        // 4. [X] Render!
        //
        // Loading screen will be removed after render (see below).

        // Inject the MUI theme into DOM.
        TeyoraUI.injectTheme();

        // Create a new messagebox queue
        this.MsgBoxQueue = new MessageBoxQueue();

        // Show Teyora!
        return <ThemeProvider theme={getTheme(this.state.theme)}>
            <CssBaseline />
            <SnackbarProvider maxSnack={3}>
                <SnackbarHandler/>
                {this.state.activeWindow}
                <MessageBox queue={this.MsgBoxQueue} />

                {/* Full page loader */}
                <Backdrop
                    sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={this.state.isFullPageLoaderOpen}
                >
                    <CircularProgress color="inherit" />
                </Backdrop>
            </SnackbarProvider>
        </ThemeProvider>;
    }

}
  

// Actual rendering bit (epic swag)
(async () => {
    if (!location.protocol.includes("http")) throw Error("Protocol must be http(s)! Aborting.");
    
    let sentryEnabled = false;
    // If enabled, start Sentry error reporting.
    // This is really crappy so this will be changed in future.
    switch (localStorage.getItem("sentryEnabled")) {
        case "true":
            Sentry.init({
                dsn: TY_SENTRY_DSN,
            });
            sentryEnabled = true;
            break;
        case "false":
            break;
        default:
            // Ask if the user wants to opt in to Sentry after the splash screen.
            setTimeout(async () => {
                const lB = LocalisedButtons();
                if(await TeyoraUI.showAlertBox(i18n.t("ui:optInToSentry.text"), i18n.t("ui:optInToSentry.title"), [ lB.NO, lB.YES ]) === lB.YES) {
                    localStorage.setItem("sentryEnabled", "true");
                    location.reload();
                } else {
                    localStorage.setItem("sentryEnabled", "false");
                }
            }, 5000);
            break;
    }

    // Loader Authorisation Manager
    const loaderAuthManager = new AuthorizationManager();

    // Get the current user
    const CurrentUser = await loaderAuthManager.getUser();

    // If the user is logged in, add to sentry
    if (CurrentUser != null && sentryEnabled) Sentry.setUser({ 
        id: CurrentUser.userID,
        username: CurrentUser.username,

    });

    // Set the current profile - empty if not logged in
    const CurrentProfile = (CurrentUser == null ? {} as ProfileEntry : await loaderAuthManager.getProfile());

    // Load the wiki support info
    const WikiSupport = await getWikiSupportInfo();

    // Check for settings here.
    await SettingsManager.loadSettings();

    // Wait for the current language to load
    await initLangs();

    // Render Teyora wrapped in an error handler
    ReactDOM.render(
        (   
            <ErrorBoundary FallbackComponent={(error)=><AppCrash message={error.error.message}></AppCrash>}>
                <Teyora
                    authManager={loaderAuthManager}
                    currentUser={CurrentUser}
                    currentProfile={CurrentProfile}
                    wikiSupport={WikiSupport}
                />
            </ErrorBoundary>
        ),
        document.getElementById("app")
    );

    document.getElementById("loading")
        .addEventListener("transitionend", function () {
            this.parentElement.removeChild(this);
        });

    // Show the splash screen for a set amount of time.
    setTimeout(() => {
        document.getElementById("loading").classList.remove("active");
    }, TY_FRONTEND_SPLASH_DELAY);
})();
