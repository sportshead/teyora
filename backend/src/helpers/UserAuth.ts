// Handles creating and modifying users
// Also handles checking if the user has permission to do things

import TeyoraDB from "../db/TeyoraDB";
import { UserEntry } from "../db/dbcollections/user";
import AdminLog from "./AdminLog";
import { MangoResponse } from "nano";
import { Request, Response } from "express";
import JWTVerify, { CurrentRequestInfo } from "./JWTVerify";
import { WikiSupport } from "../info/wikis/WikiSupport";
import WMFOAuthDriver from "../api/WMFOAuthDriver";
import { ProfileEntry, UserProfile } from "../db/dbcollections/profile";

// Treat this as "can run as admin/whilst suspended, etc."
// These are DELIBERATELY not optional, so every request has clear permissions
export interface UserSecurityRequirements {
    SuperOverlordOnly: boolean;
    OverlordOnly: boolean;
    AdminOnly: boolean;
    VerifiedOnly: boolean;
    ApprovedUsersOnly: boolean;
    LockedUsersCanRun: boolean; // For users who haven't been proved eligible
    SuspendedCanRun: boolean;
}

export default class UserAuth {
    // All functions take in a user object, and a DB object

    // Create a user in the database, will create a SuperOverlord if one doesn't exist
    // DO NOT ALLOW USERS TO CALL THIS, it's for internal use only
    // No permissions here, users may be upgraded later
    public static createUser(
            db: TeyoraDB,
            username: string,
            creationSource: string,
            userSource: string,
            preferredLocale?: string,
    ): Promise<UserEntry> {
        return new Promise(async (resolve, reject) =>{
            // Create a new account
            let userInfo = await UserAuth.getUserNPVByUsername(db, username);
            if (userInfo != null) reject(new Error("User already exists"));

            // Find the superoverlord if they exist
            const SuperOverlord = await UserAuth.getUserNPV(db, "SuperOverlord");

            // Ceate a new user

            // Generate a random 16 string for user ID
            // If the superoverlord doesn't exist, set the user to be the superoverlord
            const userID = SuperOverlord == null ? "SuperOverlord" : Math.random().toString(36).substring(2, 18);
            
            await db.User.insert(new UserEntry(
                username, // Username
                userID, // User ID
                userSource, // User Source
                preferredLocale, // Preferred Locale
                new Date(), // Last Online now, account is created
                SuperOverlord == null, // Is Admin (if no superoverlord, they are)
                false, // Is Suspended
                SuperOverlord == null, // Is Verified (if no superoverlord, they are)
                false, // Is Approved, always false, this is sorted later in first login
                true, // Is Locked until proven eligible
                SuperOverlord == null, // Is Overlord
                new Date(),
                creationSource
            ));

            // Set userinfo to the new user
            userInfo = await UserAuth.getUserNPV(db, userID);

            // Now log the creation

            await AdminLog.createLogEvent(db, userInfo, userInfo, true, "Teyora", "CreateUser", "User created");

            resolve(userInfo);
            
        });
    }

    // Get a user without checking if they can
    // Internal Use only (No Permission Verification)
    public static async getUserNPV(db: TeyoraDB, userID: string): Promise<UserEntry> {
        const UserQuery: MangoResponse<UserEntry> = await db.User.find({
            "selector": {
                "userID": userID
            }
        });

        if (UserQuery.docs.length == 0) return null;
        return UserQuery.docs[0];
    }

    // Same as above, but with a username
    public static async getUserNPVByUsername(db: TeyoraDB, username: string): Promise<UserEntry> {
        const UserQuery: MangoResponse<UserEntry> = await db.User.find({
            "selector": {
                "username": username
            }
        });

        if (UserQuery.docs.length == 0) return null;
        return UserQuery.docs[0];
    }

    // Modify a user in the database (No permission verification) - changing user ID should be impossible
    // WARNING: This CAN result in privilege escalation. Do not call it directly unless necessary, or let the user set it.
    public static async modifyUserNPV(db: TeyoraDB, user: UserEntry): Promise<UserEntry> {
        const UserQuery: MangoResponse<UserEntry> = await db.User.find({
            "selector": {
                "userID": user.userID,
            }
        });

        if (UserQuery.docs.length == 0) throw new Error("User not found");

        // Make the _rev and _id match for the update
        user._rev = UserQuery.docs[0]._rev;
        user._id = UserQuery.docs[0]._id;

        // Now update the user
        const updateResult = await db.User.insert(user);
        if (!updateResult.ok) throw new Error("Failed to update user");

        // We already have the user, so just return the rev
        user._rev = updateResult.rev;

        return user;
    }

    // Verifies if the user can run the request and returns a user (KEEP SECURITY IN MIND HERE)
    // This also updates if the user is online
    public static async getAndVerifyRequestInfo(
        db: TeyoraDB,
        req: Request,
        res: Response,
        security: UserSecurityRequirements,
    ): Promise<CurrentRequestInfo | boolean> {
        // First we need to verify the JWT that should be in request cookies
        const jwtToken = JWTVerify.pullJWT(req, res);
        if (jwtToken === "ERROR") return false; // A response has already been sent
        try {
            // Now verify the JWT
            const CRI: CurrentRequestInfo = await JWTVerify.verify(db, req);

            const user = CRI.user;

            // Update the user's last online time
            user.lastOnline = new Date();
            await UserAuth.modifyUserNPV(db, user);

            // Verify if the user can run the request

            // If the user is suspended, reject
            if (user.isSuspended && !security.SuspendedCanRun) return false;

            // If the user is locked, reject
            if (user.isLocked && !security.LockedUsersCanRun) return false;

            // Let SuperOverlord run everything from here
            if (user.userID === "SuperOverlord") return CRI;

            // If the request is SuperOverlord only, reject as the SuperOverlord has all permissions
            if (security.SuperOverlordOnly) return false;

            // If the request is Overlord only, reject if the user isn't an overlord
            if (security.OverlordOnly && !user.isOverlord) return false;

            // If the request is Admin only, reject if the user isn't an admin or overlord
            if (security.AdminOnly && (!user.isAdmin || !user.isOverlord)) return false;

            // If the request is Verified only, reject if the user isn't verified
            if (security.VerifiedOnly && (!user.isVerified || !user.isAdmin || !user.isOverlord)) return false;

            // If the request is Approved only, reject if the user isn't approved
            if (security.ApprovedUsersOnly && (!user.isApproved || !user.isAdmin || !user.isOverlord)) return false;

            // No other limitations, return the info
            return CRI;
        } catch (error) {
            return false;
        }
    }

    // Unlock/auto-approve a user
    // WARNING: Privilege escalation possible - DO NOT set anything except isLocked/IsApproved
    public static async unlockUser(db: TeyoraDB, WMFOAuth: WMFOAuthDriver, CRI: CurrentRequestInfo,  WikiDB: string): Promise<UserEntry> {
        const user = CRI.user;
        if (!user.isLocked && user.isApproved) return user; // There's nothing to do

        // Get wiki info
        const WikiInfo = WikiSupport[WikiDB];
        if (WikiInfo == null) throw new Error("Invalid wiki");

        // Get user rights from the wiki
        const authxios = WMFOAuth.getAuthedAxios(CRI.WMFOAuthToken);

        let profileRequest;
        try {
            profileRequest = (await authxios.get(`${WikiInfo.url}/w/api.php?action=query&meta=userinfo&uiprop=rights&format=json`)).data;
        } catch (error) {
            throw new Error("Failed to get user info from wiki");
        } 
        if (profileRequest.query == null ||
            profileRequest.query.userinfo == null ||
            profileRequest.query.userinfo.rights == null) throw new Error("Invalid response from wiki");

        const editorRights: string[] = profileRequest.query.userinfo.rights;
        
        // Users can be unlocked if they are auto approved
        if (user.isLocked && editorRights.includes("autoconfirmed")) {
            // Unlock the user
            user.isLocked = false;

            // Log the unlock
            await AdminLog.createLogEvent(db, user, user, true, "Teyora", "UnlockUser", "User unlocked with " + WikiDB);
        }

        // Auto approval, if user isn't approved and rights include "rollback"
        if (!user.isApproved && editorRights.includes("rollback")) {
            // Approve the user
            user.isApproved = true;

            // Log the approval
            await AdminLog.createLogEvent(db, user, user, true, "Teyora", "ApproveUser", "User approved with " + WikiDB);
        }

        // Update the user with new details
        const newUser = await UserAuth.modifyUserNPV(db, user);

        return newUser;
    }

    // Get the user's profile (No permission verification)
    public static async getUserProfileNPV(db: TeyoraDB, user: UserEntry): Promise<ProfileEntry> {
        // Get the user's profile
        const profile = await db.Profile.find({
            "selector": {
                "userID": user.userID
            }
        });

        if (profile.docs.length == 0) return {} as ProfileEntry; // No profile
        return profile.docs[0];
    }

    // Set the user's profile (No permission verification)
    public static async setUserProfileNPV(db: TeyoraDB, user: UserEntry, profile: ProfileEntry): Promise<UserProfile> {
        // Get the user's profile
        const profileQuery = await db.Profile.find({
            "selector": {
                "userID": user.userID
            }
        });

        // If the user doesn't have a profile, create one
        if (profileQuery.docs.length == 0) {
            const profileResult = await db.Profile.insert(profile);
            if (!profileResult.ok) throw new Error("Failed to create profile");
        } else {
            // Make the _rev and _id match for the update
            profile._rev = profileQuery.docs[0]._rev;
            profile._id = profileQuery.docs[0]._id;

            // Now update the profile
            const updateResult = await db.Profile.insert(profile);
            if (!updateResult.ok) throw new Error("Failed to update profile");
        }

        return profile.profile; // Return the actual profile, this is what's useful - ProfileEntry is just for querys
    }



}