import path from "path";
import express from "express";
import cookieParser from "cookie-parser";
import Logger from "bunyan";
import bformat from "bunyan-format";
import moment from "moment";
import * as Sentry from "@sentry/node";
import * as http from "http";
import { AddressInfo } from "net";
import {
    TY_BACKEND_IDENTIFIER,
    TY_DEBUG
} from "../../common/src/Constants";
import {TY_BACKEND_SENTRY_DSN, TY_ROOT_PATH} from "../../common/src/backend/BackendConstants";
import APIRouter from "./api/APIRouter";
import TeyoraDB from "./db/TeyoraDB";
import { UserEntry } from "./db/dbcollections/user";
import UserAuth from "./helpers/UserAuth";

/**
 * The Teyora Backend server. This is where the magic happens.
 */
export default class TeyoraBackendServer {

    /** The `bunyan` logger. **/
    public log : Logger;
    /** The timestamp when the TYB Server started. **/
    startTime : number;

    /** The Express application **/
    private app : express.Express;

    /** The current TeyoraDB **/
    public db : TeyoraDB;

    /**
     *  A collection of observers mapped by name.
     *
     *  An observer is a class which either listens to events or regularly checks an
     *  endpoint, and updates whatever components need updating when the observer
     *  detects a change.
     **/
    private observers: Record<string, any> = {}; // No parent class for observers yet.

    /**
     *  The HTTP listener.
     *
     *  Note to homebrew Teyora operators: If you want Teyora to use HTTPS,
     *  you can do that by using {@link https.Server} instead.
     **/
    private httpListener? : http.Server;
    /**
     * The Teyora routers.
     *
     * Each router is responsible for an action on a certain group of endpoints. For
     * exaple, all request in the `/api` path will be passed to the API router. This
     * way, certain middleware can be isolated to specific parts of the server.
     **/
    private routers : { [key : string] : express.Router } = {};

    /**
     * Creates a new TeyoraBackendServer.
     */
    constructor() {
        // Set the `bunyan` log.
        // Still debating whether a filestream log is required.
        const formatOut = bformat({ outputMode: "short" });
        this.log = Logger.createLogger({
            name: "Teyora Backend",
            src: true,
            streams: [
                {
                    level: TY_DEBUG ? "debug" : "info",
                    stream: formatOut
                }
            ]
        });
    }

    /**
     * Verify environment variables.
     *
     * @returns `true` on success, `string` with reason on failure.
     */
    verifyEnvironment() : true | string {
        return (TY_ROOT_PATH == null) ?
            "Teyora root path cannot be found. Is Teyora in the right directory, or is the anchor file present?"
            : true;
    }

    /**
     * Start up the Express routers to be used.
     */
    async initializeExpressRouters() : Promise<express.Router[]> {
        // API router
        this.routers["api"] = new APIRouter(this.db).getRouter();
        return Object.values(this.routers);
    }

    /**
     * Creates an Express app and assigns the proper middleware.
     */
    async initializeExpressServer() : Promise<void> {
        // This starts the express server
        this.app = express();

        // Sentry Error handling pt1
        this.app.use(Sentry.Handlers.requestHandler());

        this.app.use(cookieParser());

        this.app.use(express.json());

        this.app.use(express.urlencoded({ extended: true }));

        for (const router of (await this.initializeExpressRouters()))
            this.app.use(router);

        this.app.use("/", express.static(path.resolve(TY_ROOT_PATH, "static")));

        // Sentry Error handling pt2
        this.app.use(Sentry.Handlers.errorHandler());

        // Error handling message, typing not needed
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        this.app.use((err: any, req:any, res:any, next:any) => {
            this.log.error(err);
            res.status(500).json({
                error: "There was an error processing your request, which has been reported for investigation."
            });
        });
    }

    /**
     * Starts the Teyora Backend Server.
     */
    async startup() : Promise<void> {
        this.startTime = Date.now();
        this.log.info(`[!] Teyora Backend Server started at ${moment().format()}.`);
        this.log.info(`Teyora Backend v${TY_BACKEND_IDENTIFIER}`);

        let environmentOK;
        if ((environmentOK = this.verifyEnvironment()) !== true) {
            this.log.fatal(`Cannot continue startup. Reason: ${environmentOK}`);
        }

        // Initialise Sentry
        Sentry.init({
            dsn: TY_BACKEND_SENTRY_DSN,
        });

        // Log into the database
        this.log.info("Connecting to database...");
        this.db = new TeyoraDB();
        try {
            await this.db.connect();
        } catch (error) {
            // Couldn't connect
            this.log.error(error);
            this.log.fatal("Could not connect to database. Exiting.");
            process.exit(1);
            return; // Just in case
        }

        // Find superoverlord
        const SuperOverlord: UserEntry = await UserAuth.getUserNPV(this.db, "SuperOverlord");

        if (SuperOverlord == null) {
            this.log.warn("Database connection successful, however the SuperOverlord was not found. The first user to log in will be made the SuperOverlord.");
        } else {
            this.log.info("Database connection successful. SuperOverlord is: " + SuperOverlord.username);
        }

        // Create the Express app and respective routers.
        this.log.info("Creating the Express server...");
        await this.initializeExpressServer();

        // Wikimedia Cloud will automatically encrypt internet requests, so we
        // don't need to worry about running HTTPS. This is a HTTP server that
        // only runs locally and is normally firewalled anyway
        this.log.info(`Running HTTP listener on port ${process.env.PORT ?? 45990}...`);
        this.httpListener = http.createServer(this.app);
        this.httpListener.listen(process.env.PORT ?? 45990, async () => {
            this.log.info(`HTTP is listening on port ${
                (this.httpListener.address() as AddressInfo).port
            }.`);
        });
    }

    /**
     * Stops the Teyora Backend Server.
     */
    async shutdown() : Promise<void> {
        if (this.httpListener != null && this.httpListener.listening) {
            this.log.info("Shutting down web server...");

            if (this.httpListener != null)
                this.httpListener.close();

            this.log.info("Web server closed.");
        }

        this.log.info();
        this.log.info(`Total Execution Time: ${moment(Date.now() - this.startTime).format()}`);
        this.log.info();
        this.log.info(`Shutdown complete (${moment().format()})`);
    }

}

// main code
const rwBackendServer = new TeyoraBackendServer();
rwBackendServer.startup();