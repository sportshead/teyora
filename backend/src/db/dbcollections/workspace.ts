import Nano from "nano";
export interface iWorkspaceEntry extends Nano.MaybeDocument {
    teyoraWorkspaceID: string, // The workspace ID, used to identify the workspace on Teyora
    workspaceOwner: string, // The userID of the user who owns the workspace
    workspaceName: string, // The name of the workspace
    workspaceDescription: string, // The description of the workspace
    workspaceParticipants: Array<any>, // The user ID participants of the workspace
    workspaceIcon: string, // The icon of the workspace
    workspaceConfig: any, // The config of the workspace
    workspaceCreationDate: Date, // The date the workspace was created
    workspaceLastModified: Date, // The date the workspace was last modified
    workspaceLastActive: Date, // The date the workspace was last active
    workspaceIsLocked: boolean, // Whether the workspace is locked or not
    workspaceIsPublic: boolean, // Whether the workspace is public or not
    workspaceIsDeleted: boolean, // Whether the workspace is deleted or not
    workspaceIsJoinable: boolean, // Whether the workspace is joinable or invite only
}

export class WorkspaceEntry implements iWorkspaceEntry {
    _id?: string;
    _rev?: string;
    teyoraWorkspaceID: string;
    workspaceOwner: string;
    workspaceName: string;
    workspaceDescription: string;
    workspaceParticipants: Array<any>;
    workspaceIcon: string;
    workspaceConfig: any;
    workspaceCreationDate: Date;
    workspaceLastModified: Date;
    workspaceLastActive: Date;
    workspaceIsLocked: boolean;
    workspaceIsPublic: boolean;
    workspaceIsDeleted: boolean;
    workspaceIsJoinable: boolean;
    
    constructor(
        teyoraWorkspaceID: string,
        workspaceOwner: string,
        workspaceName: string,
        workspaceDescription: string,
        workspaceParticipants: Array<any>,
        workspaceIcon: string,
        workspaceConfig: any,
        workspaceCreationDate: Date,
        workspaceLastModified: Date,
        workspaceLastActive: Date,
        workspaceIsLocked: boolean,
        workspaceIsPublic: boolean,
        workspaceIsDeleted: boolean,
        workspaceIsJoinable: boolean
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.teyoraWorkspaceID = teyoraWorkspaceID;
        this.workspaceOwner = workspaceOwner;
        this.workspaceName = workspaceName;
        this.workspaceDescription = workspaceDescription;
        this.workspaceParticipants = workspaceParticipants;
        this.workspaceIcon = workspaceIcon;
        this.workspaceConfig = workspaceConfig;
        this.workspaceCreationDate = workspaceCreationDate;
        this.workspaceLastModified = workspaceLastModified;
        this.workspaceLastActive = workspaceLastActive;
        this.workspaceIsLocked = workspaceIsLocked;
        this.workspaceIsPublic = workspaceIsPublic;
        this.workspaceIsDeleted = workspaceIsDeleted;
        this.workspaceIsJoinable = workspaceIsJoinable;
    }
    
    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}