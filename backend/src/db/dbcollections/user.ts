import Nano from "nano";
export interface iUserEntry extends Nano.MaybeDocument {
    username: string, // The username of the user, used by those who don't know UID i.e. (WMF:Ed6767)
    userID: string, // A random userID, used to identify the user, for configs etc.
    userSource: string, // The source of the user, e.g. WMF, Fandom, etc.
    // Don't put any profile or config info here, as it is stored in the profile/config model
    preferredLocale: string, // For translations: the preferred locale of the user, e.g. "en-US"
    lastOnline: Date, // The last time the server knew the user was online
    isAdmin: boolean, // Whether the user is an admin - this grants super access so be careful!
    isSuspended: boolean, // Whether the user is suspended - this lets them log in but not edit
    isVerified: boolean, // Whether the user is verified as a real person
    isApproved: boolean, // Whether the user has full access following approval, this may be automatically set
    isLocked: boolean, // Whether the user is locked, i.e. they haven't been verified as eligible for Teyora
    isOverlord: boolean, // Whether the user is an overlord - this grants super access to all users and to add/remove admins
    creationDate: Date, // The date the user was created
    creationSource: string, // The source of the user's creation, e.g. Teyora Website or manually added
}

export class UserEntry implements iUserEntry {
    _id?: string;
    _rev?: string;
    username: string;
    userID: string;
    userSource: string;
    preferredLocale: string;
    lastOnline: Date;
    isAdmin: boolean;
    isSuspended: boolean;
    isVerified: boolean;
    isApproved: boolean;
    isLocked: boolean;
    isOverlord: boolean;
    creationDate: Date;
    creationSource: string;
    
    constructor(
        username: string,
        userID: string,
        userSource: string,
        preferredLocale: string,
        lastOnline: Date,
        isAdmin: boolean,
        isSuspended: boolean,
        isVerified: boolean,
        isApproved: boolean,
        isLocked: boolean,
        isOverlord: boolean,
        creationDate: Date,
        creationSource: string
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.username = username;
        this.userID = userID;
        this.userSource = userSource;
        this.preferredLocale = preferredLocale;
        this.lastOnline = lastOnline;
        this.isAdmin = isAdmin;
        this.isSuspended = isSuspended;
        this.isVerified = isVerified;
        this.isApproved = isApproved;
        this.isLocked = isLocked;
        this.isOverlord = isOverlord;
        this.creationDate = creationDate;
        this.creationSource = creationSource;
    }
    
    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}