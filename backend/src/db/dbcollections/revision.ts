import Nano from "nano";
export interface iRevisionEntry extends Nano.MaybeDocument {
    teyoraRevisionID: string, // The revision ID, used to identify the revision on Teyora
    informationPublic: boolean, // Whether the revision is public or not, only admins can change this, may be automatically set
    wikiID: string, // The wikiID of the revision, e.g. enwiki
    revisionID: string, // The revision ID, used to identify the revision on this wiki
    reviewedBy: Array<any>, // The users who have reviewed the revision (also mirrored in review status)
    reviewStatus: number, // The status of the revision on Teyora,
                            //e.g. 0 = not reviewed, 1 = approved, 2 = reverted, 3 = concerned, 4 = reported
    reviewInfo: any, // The information about the review, e.g. the user who approved/reverted/concerned/reported
    reviewDate: Date, // The date the revision was reviewed last
    reviewSource: string, // The source of the review, e.g. Teyora Website or manually added
    lastReviewed: Date, // The last time the revision was reviewed
}


export class RevisionEntry implements iRevisionEntry {
    _id?: string;
    _rev?: string;
    teyoraRevisionID: string;
    informationPublic: boolean;
    wikiID: string;
    revisionID: string;
    reviewedBy: Array<any>;
    reviewStatus: number;
    reviewInfo: any;
    reviewDate: Date;
    reviewSource: string;
    lastReviewed: Date;
    
    constructor(
        teyoraRevisionID: string,
        informationPublic: boolean,
        wikiID: string,
        revisionID: string,
        reviewedBy: Array<any>,
        reviewStatus: number,
        reviewInfo: any,
        reviewDate: Date,
        reviewSource: string,
        lastReviewed: Date
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.teyoraRevisionID = teyoraRevisionID;
        this.informationPublic = informationPublic;
        this.wikiID = wikiID;
        this.revisionID = revisionID;
        this.reviewedBy = reviewedBy;
        this.reviewStatus = reviewStatus;
        this.reviewInfo = reviewInfo;
        this.reviewDate = reviewDate;
        this.reviewSource = reviewSource;
        this.lastReviewed = lastReviewed;
    }
    
    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}