/**
 * A list of all availible Wikimedia wikis, and the features they support, such as ORES, Warning, Reporting, Blocking, Revert, Discussions, TalkPageMessages etc.
 * These are overrides, so if no support map is defined or a key is not defined, the default support map is used.
 * 
 * To generate a new support map, copy the code from scripts/WikiSupportFormatter.js into a browser console and run it.
 * 
 * TODO: Add ORES wikis and let this replace the old support map, condense this and probably make it backend only because it's a lot of data.
 */

import { WikiSupportInfo } from "../../../../common/src/objects/WikiSupportMap";

export const WikiSupport: Record<string, WikiSupportInfo> = {
    "abwiki": {
        "langCode": "ab",
        "langName": "Аҧсшәа",
        "langNameEN": "Abkhazian",
        "name": "Авикипедиа",
        "url": "https://ab.wikipedia.org"
    },
    "acewiki": {
        "langCode": "ace",
        "langName": "Acèh",
        "langNameEN": "Achinese",
        "name": "Wikipedia",
        "url": "https://ace.wikipedia.org"
    },
    "adywiki": {
        "langCode": "ady",
        "langName": "адыгабзэ",
        "langNameEN": "Adyghe",
        "name": "Википедие",
        "url": "https://ady.wikipedia.org"
    },
    "afwiki": {
        "langCode": "af",
        "langName": "Afrikaans",
        "langNameEN": "Afrikaans",
        "name": "Wikipedia",
        "url": "https://af.wikipedia.org"
    },
    "afwiktionary": {
        "langCode": "af",
        "langName": "Afrikaans",
        "langNameEN": "Afrikaans",
        "name": "Wiktionary",
        "url": "https://af.wiktionary.org"
    },
    "afwikibooks": {
        "langCode": "af",
        "langName": "Afrikaans",
        "langNameEN": "Afrikaans",
        "name": "Wikibooks",
        "url": "https://af.wikibooks.org"
    },
    "afwikiquote": {
        "langCode": "af",
        "langName": "Afrikaans",
        "langNameEN": "Afrikaans",
        "name": "Wikiquote",
        "url": "https://af.wikiquote.org"
    },
    "akwiki": {
        "langCode": "ak",
        "langName": "Akan",
        "langNameEN": "Akan",
        "name": "Wikipedia",
        "url": "https://ak.wikipedia.org"
    },
    "alswiki": {
        "langCode": "als",
        "langName": "Alemannisch",
        "langNameEN": "Alemannisch",
        "name": "Wikipedia",
        "url": "https://als.wikipedia.org"
    },
    "altwiki": {
        "langCode": "alt",
        "langName": "алтай тил",
        "langNameEN": "Southern Altai",
        "name": "Википедия",
        "url": "https://alt.wikipedia.org"
    },
    "amwiki": {
        "langCode": "am",
        "langName": "አማርኛ",
        "langNameEN": "Amharic",
        "name": "ውክፔዲያ",
        "url": "https://am.wikipedia.org"
    },
    "amwiktionary": {
        "langCode": "am",
        "langName": "አማርኛ",
        "langNameEN": "Amharic",
        "name": "Wiktionary",
        "url": "https://am.wiktionary.org"
    },
    "amiwiki": {
        "langCode": "ami",
        "langName": "Pangcah",
        "langNameEN": "Amis",
        "name": "Wikipedia",
        "url": "https://ami.wikipedia.org"
    },
    "anwiki": {
        "langCode": "an",
        "langName": "aragonés",
        "langNameEN": "Aragonese",
        "name": "Wikipedia",
        "url": "https://an.wikipedia.org"
    },
    "anwiktionary": {
        "langCode": "an",
        "langName": "aragonés",
        "langNameEN": "Aragonese",
        "name": "Wiktionary",
        "url": "https://an.wiktionary.org"
    },
    "angwiki": {
        "langCode": "ang",
        "langName": "Ænglisc",
        "langNameEN": "Old English",
        "name": "Wikipǣdia",
        "url": "https://ang.wikipedia.org"
    },
    "angwiktionary": {
        "langCode": "ang",
        "langName": "Ænglisc",
        "langNameEN": "Old English",
        "name": "Wikiwordbōc",
        "url": "https://ang.wiktionary.org"
    },
    "arwiki": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكيبيديا",
        "url": "https://ar.wikipedia.org"
    },
    "arwiktionary": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكاموس",
        "url": "https://ar.wiktionary.org"
    },
    "arwikibooks": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكي_الكتب",
        "url": "https://ar.wikibooks.org"
    },
    "arwikinews": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكي_الأخبار",
        "url": "https://ar.wikinews.org"
    },
    "arwikiquote": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكي_الاقتباس",
        "url": "https://ar.wikiquote.org"
    },
    "arwikisource": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكي_مصدر",
        "url": "https://ar.wikisource.org"
    },
    "arwikiversity": {
        "langCode": "ar",
        "langName": "العربية",
        "langNameEN": "Arabic",
        "name": "ويكي الجامعة",
        "url": "https://ar.wikiversity.org"
    },
    "arcwiki": {
        "langCode": "arc",
        "langName": "ܐܪܡܝܐ",
        "langNameEN": "Aramaic",
        "name": "ܘܝܩܝܦܕܝܐ",
        "url": "https://arc.wikipedia.org"
    },
    "arywiki": {
        "langCode": "ary",
        "langName": "الدارجة",
        "langNameEN": "Moroccan Arabic",
        "name": "ويكيپيديا",
        "url": "https://ary.wikipedia.org"
    },
    "arzwiki": {
        "langCode": "arz",
        "langName": "مصرى",
        "langNameEN": "Egyptian Arabic",
        "name": "ويكيبيديا",
        "url": "https://arz.wikipedia.org"
    },
    "aswiki": {
        "langCode": "as",
        "langName": "অসমীয়া",
        "langNameEN": "Assamese",
        "name": "অসমীয়া ৱিকিপিডিয়া",
        "url": "https://as.wikipedia.org"
    },
    "aswikisource": {
        "langCode": "as",
        "langName": "অসমীয়া",
        "langNameEN": "Assamese",
        "name": "ৱিকিউৎস",
        "url": "https://as.wikisource.org"
    },
    "astwiki": {
        "langCode": "ast",
        "langName": "asturianu",
        "langNameEN": "Asturian",
        "name": "Wikipedia",
        "url": "https://ast.wikipedia.org"
    },
    "astwiktionary": {
        "langCode": "ast",
        "langName": "asturianu",
        "langNameEN": "Asturian",
        "name": "Wikcionariu",
        "url": "https://ast.wiktionary.org"
    },
    "atjwiki": {
        "langCode": "atj",
        "langName": "Atikamekw",
        "langNameEN": "Atikamekw",
        "name": "Wikipetcia",
        "url": "https://atj.wikipedia.org"
    },
    "avwiki": {
        "langCode": "av",
        "langName": "авар",
        "langNameEN": "Avaric",
        "name": "Wikipedia",
        "url": "https://av.wikipedia.org"
    },
    "avkwiki": {
        "langCode": "avk",
        "langName": "Kotava",
        "langNameEN": "Kotava",
        "name": "Wikipedia",
        "url": "https://avk.wikipedia.org"
    },
    "awawiki": {
        "langCode": "awa",
        "langName": "अवधी",
        "langNameEN": "Awadhi",
        "name": "विकिपीडिया",
        "url": "https://awa.wikipedia.org"
    },
    "aywiki": {
        "langCode": "ay",
        "langName": "Aymar aru",
        "langNameEN": "Aymara",
        "name": "Wikipedia",
        "url": "https://ay.wikipedia.org"
    },
    "aywiktionary": {
        "langCode": "ay",
        "langName": "Aymar aru",
        "langNameEN": "Aymara",
        "name": "Wiktionary",
        "url": "https://ay.wiktionary.org"
    },
    "azwiki": {
        "langCode": "az",
        "langName": "azərbaycanca",
        "langNameEN": "Azerbaijani",
        "name": "Vikipediya",
        "url": "https://az.wikipedia.org"
    },
    "azwiktionary": {
        "langCode": "az",
        "langName": "azərbaycanca",
        "langNameEN": "Azerbaijani",
        "name": "Wiktionary",
        "url": "https://az.wiktionary.org"
    },
    "azwikibooks": {
        "langCode": "az",
        "langName": "azərbaycanca",
        "langNameEN": "Azerbaijani",
        "name": "Vikikitab",
        "url": "https://az.wikibooks.org"
    },
    "azwikiquote": {
        "langCode": "az",
        "langName": "azərbaycanca",
        "langNameEN": "Azerbaijani",
        "name": "Vikisitat",
        "url": "https://az.wikiquote.org"
    },
    "azwikisource": {
        "langCode": "az",
        "langName": "azərbaycanca",
        "langNameEN": "Azerbaijani",
        "name": "Vikimənbə",
        "url": "https://az.wikisource.org"
    },
    "azbwiki": {
        "langCode": "azb",
        "langName": "تۆرکجه",
        "langNameEN": "South Azerbaijani",
        "name": "ویکی‌پدیا",
        "url": "https://azb.wikipedia.org"
    },
    "bawiki": {
        "langCode": "ba",
        "langName": "башҡортса",
        "langNameEN": "Bashkir",
        "name": "Википедия",
        "url": "https://ba.wikipedia.org"
    },
    "bawikibooks": {
        "langCode": "ba",
        "langName": "башҡортса",
        "langNameEN": "Bashkir",
        "name": "Викидәреслек",
        "url": "https://ba.wikibooks.org"
    },
    "banwiki": {
        "langCode": "ban",
        "langName": "Basa Bali",
        "langNameEN": "Balinese",
        "name": "Wikipédia",
        "url": "https://ban.wikipedia.org"
    },
    "banwikisource": {
        "langCode": "ban",
        "langName": "Basa Bali",
        "langNameEN": "Balinese",
        "name": "Wikisource",
        "url": "https://ban.wikisource.org"
    },
    "barwiki": {
        "langCode": "bar",
        "langName": "Boarisch",
        "langNameEN": "Bavarian",
        "name": "Wikipedia",
        "url": "https://bar.wikipedia.org"
    },
    "bat_smgwiki": {
        "langCode": "bat-smg",
        "langName": "žemaitėška",
        "langNameEN": "Samogitian",
        "name": "Wikipedia",
        "url": "https://bat-smg.wikipedia.org"
    },
    "bclwiki": {
        "langCode": "bcl",
        "langName": "Bikol Central",
        "langNameEN": "Central Bikol",
        "name": "Wikipedia",
        "url": "https://bcl.wikipedia.org"
    },
    "bclwiktionary": {
        "langCode": "bcl",
        "langName": "Bikol Central",
        "langNameEN": "Central Bikol",
        "name": "Wiksyunaryo",
        "url": "https://bcl.wiktionary.org"
    },
    "bewiki": {
        "langCode": "be",
        "langName": "беларуская",
        "langNameEN": "Belarusian",
        "name": "Вікіпедыя",
        "url": "https://be.wikipedia.org"
    },
    "bewiktionary": {
        "langCode": "be",
        "langName": "беларуская",
        "langNameEN": "Belarusian",
        "name": "Вікіслоўнік",
        "url": "https://be.wiktionary.org"
    },
    "bewikibooks": {
        "langCode": "be",
        "langName": "беларуская",
        "langNameEN": "Belarusian",
        "name": "Вікікнігі",
        "url": "https://be.wikibooks.org"
    },
    "bewikiquote": {
        "langCode": "be",
        "langName": "беларуская",
        "langNameEN": "Belarusian",
        "name": "Wikiquote",
        "url": "https://be.wikiquote.org"
    },
    "bewikisource": {
        "langCode": "be",
        "langName": "беларуская",
        "langNameEN": "Belarusian",
        "name": "Вікікрыніцы",
        "url": "https://be.wikisource.org"
    },
    "be_x_oldwiki": {
        "langCode": "be-x-old",
        "langName": "беларуская (тарашкевіца)",
        "langNameEN": "Belarusian (Taraškievica orthography)",
        "name": "Вікіпэдыя",
        "url": "https://be-tarask.wikipedia.org"
    },
    "bgwiki": {
        "langCode": "bg",
        "langName": "български",
        "langNameEN": "Bulgarian",
        "name": "Уикипедия",
        "url": "https://bg.wikipedia.org"
    },
    "bgwiktionary": {
        "langCode": "bg",
        "langName": "български",
        "langNameEN": "Bulgarian",
        "name": "Уикиречник",
        "url": "https://bg.wiktionary.org"
    },
    "bgwikibooks": {
        "langCode": "bg",
        "langName": "български",
        "langNameEN": "Bulgarian",
        "name": "Уикикниги",
        "url": "https://bg.wikibooks.org"
    },
    "bgwikiquote": {
        "langCode": "bg",
        "langName": "български",
        "langNameEN": "Bulgarian",
        "name": "Уикицитат",
        "url": "https://bg.wikiquote.org"
    },
    "bgwikisource": {
        "langCode": "bg",
        "langName": "български",
        "langNameEN": "Bulgarian",
        "name": "Уикиизточник",
        "url": "https://bg.wikisource.org"
    },
    "bhwiki": {
        "langCode": "bh",
        "langName": "भोजपुरी",
        "langNameEN": "Bhojpuri",
        "name": "विकिपीडिया",
        "url": "https://bh.wikipedia.org"
    },
    "biwiki": {
        "langCode": "bi",
        "langName": "Bislama",
        "langNameEN": "Bislama",
        "name": "Wikipedia",
        "url": "https://bi.wikipedia.org"
    },
    "bjnwiki": {
        "langCode": "bjn",
        "langName": "Banjar",
        "langNameEN": "Banjar",
        "name": "Wikipidia",
        "url": "https://bjn.wikipedia.org"
    },
    "bmwiki": {
        "langCode": "bm",
        "langName": "bamanankan",
        "langNameEN": "Bambara",
        "name": "Wikipedia",
        "url": "https://bm.wikipedia.org"
    },
    "bnwiki": {
        "langCode": "bn",
        "langName": "বাংলা",
        "langNameEN": "Bangla",
        "name": "উইকিপিডিয়া",
        "url": "https://bn.wikipedia.org"
    },
    "bnwiktionary": {
        "langCode": "bn",
        "langName": "বাংলা",
        "langNameEN": "Bangla",
        "name": "উইকিঅভিধান",
        "url": "https://bn.wiktionary.org"
    },
    "bnwikibooks": {
        "langCode": "bn",
        "langName": "বাংলা",
        "langNameEN": "Bangla",
        "name": "উইকিবই",
        "url": "https://bn.wikibooks.org"
    },
    "bnwikisource": {
        "langCode": "bn",
        "langName": "বাংলা",
        "langNameEN": "Bangla",
        "name": "উইকিসংকলন",
        "url": "https://bn.wikisource.org"
    },
    "bnwikivoyage": {
        "langCode": "bn",
        "langName": "বাংলা",
        "langNameEN": "Bangla",
        "name": "উইকিভ্রমণ",
        "url": "https://bn.wikivoyage.org"
    },
    "bowiki": {
        "langCode": "bo",
        "langName": "བོད་ཡིག",
        "langNameEN": "Tibetan",
        "name": "Wikipedia",
        "url": "https://bo.wikipedia.org"
    },
    "bpywiki": {
        "langCode": "bpy",
        "langName": "বিষ্ণুপ্রিয়া মণিপুরী",
        "langNameEN": "Bishnupriya",
        "name": "উইকিপিডিয়া",
        "url": "https://bpy.wikipedia.org"
    },
    "brwiki": {
        "langCode": "br",
        "langName": "brezhoneg",
        "langNameEN": "Breton",
        "name": "Wikipedia",
        "url": "https://br.wikipedia.org"
    },
    "brwiktionary": {
        "langCode": "br",
        "langName": "brezhoneg",
        "langNameEN": "Breton",
        "name": "Wikeriadur",
        "url": "https://br.wiktionary.org"
    },
    "brwikiquote": {
        "langCode": "br",
        "langName": "brezhoneg",
        "langNameEN": "Breton",
        "name": "Wikiarroud",
        "url": "https://br.wikiquote.org"
    },
    "brwikisource": {
        "langCode": "br",
        "langName": "brezhoneg",
        "langNameEN": "Breton",
        "name": "Wikimammenn",
        "url": "https://br.wikisource.org"
    },
    "bswiki": {
        "langCode": "bs",
        "langName": "bosanski",
        "langNameEN": "Bosnian",
        "name": "Wikipedia",
        "url": "https://bs.wikipedia.org"
    },
    "bswiktionary": {
        "langCode": "bs",
        "langName": "bosanski",
        "langNameEN": "Bosnian",
        "name": "Wikirječnik",
        "url": "https://bs.wiktionary.org"
    },
    "bswikibooks": {
        "langCode": "bs",
        "langName": "bosanski",
        "langNameEN": "Bosnian",
        "name": "Wikiknjige",
        "url": "https://bs.wikibooks.org"
    },
    "bswikinews": {
        "langCode": "bs",
        "langName": "bosanski",
        "langNameEN": "Bosnian",
        "name": "Wikivijesti",
        "url": "https://bs.wikinews.org"
    },
    "bswikiquote": {
        "langCode": "bs",
        "langName": "bosanski",
        "langNameEN": "Bosnian",
        "name": "Wikicitati",
        "url": "https://bs.wikiquote.org"
    },
    "bswikisource": {
        "langCode": "bs",
        "langName": "bosanski",
        "langNameEN": "Bosnian",
        "name": "Wikizvor",
        "url": "https://bs.wikisource.org"
    },
    "bugwiki": {
        "langCode": "bug",
        "langName": "ᨅᨔ ᨕᨘᨁᨗ",
        "langNameEN": "Buginese",
        "name": "Wikipedia",
        "url": "https://bug.wikipedia.org"
    },
    "bxrwiki": {
        "langCode": "bxr",
        "langName": "буряад",
        "langNameEN": "Russia Buriat",
        "name": "Wikipedia",
        "url": "https://bxr.wikipedia.org"
    },
    "cawiki": {
        "langCode": "ca",
        "langName": "català",
        "langNameEN": "Catalan",
        "name": "Viquipèdia",
        "url": "https://ca.wikipedia.org"
    },
    "cawiktionary": {
        "langCode": "ca",
        "langName": "català",
        "langNameEN": "Catalan",
        "name": "Viccionari",
        "url": "https://ca.wiktionary.org"
    },
    "cawikibooks": {
        "langCode": "ca",
        "langName": "català",
        "langNameEN": "Catalan",
        "name": "Viquillibres",
        "url": "https://ca.wikibooks.org"
    },
    "cawikinews": {
        "langCode": "ca",
        "langName": "català",
        "langNameEN": "Catalan",
        "name": "Viquinotícies",
        "url": "https://ca.wikinews.org"
    },
    "cawikiquote": {
        "langCode": "ca",
        "langName": "català",
        "langNameEN": "Catalan",
        "name": "Viquidites",
        "url": "https://ca.wikiquote.org"
    },
    "cawikisource": {
        "langCode": "ca",
        "langName": "català",
        "langNameEN": "Catalan",
        "name": "Viquitexts",
        "url": "https://ca.wikisource.org"
    },
    "cbk_zamwiki": {
        "langCode": "cbk-zam",
        "langName": "Chavacano de Zamboanga",
        "langNameEN": "Chavacano",
        "name": "Wikipedia",
        "url": "https://cbk-zam.wikipedia.org"
    },
    "cdowiki": {
        "langCode": "cdo",
        "langName": "Mìng-dĕ̤ng-ngṳ̄",
        "langNameEN": "Min Dong Chinese",
        "name": "Wikipedia",
        "url": "https://cdo.wikipedia.org"
    },
    "cewiki": {
        "langCode": "ce",
        "langName": "нохчийн",
        "langNameEN": "Chechen",
        "name": "Википеди",
        "url": "https://ce.wikipedia.org"
    },
    "cebwiki": {
        "langCode": "ceb",
        "langName": "Cebuano",
        "langNameEN": "Cebuano",
        "name": "Wikipedia",
        "url": "https://ceb.wikipedia.org"
    },
    "chwiki": {
        "langCode": "ch",
        "langName": "Chamoru",
        "langNameEN": "Chamorro",
        "name": "Wikipedia",
        "url": "https://ch.wikipedia.org"
    },
    "chrwiki": {
        "langCode": "chr",
        "langName": "ᏣᎳᎩ",
        "langNameEN": "Cherokee",
        "name": "Wikipedia",
        "url": "https://chr.wikipedia.org"
    },
    "chrwiktionary": {
        "langCode": "chr",
        "langName": "ᏣᎳᎩ",
        "langNameEN": "Cherokee",
        "name": "Wiktionary",
        "url": "https://chr.wiktionary.org"
    },
    "chywiki": {
        "langCode": "chy",
        "langName": "Tsetsêhestâhese",
        "langNameEN": "Cheyenne",
        "name": "Tsétsêhéstâhese Wikipedia",
        "url": "https://chy.wikipedia.org"
    },
    "ckbwiki": {
        "langCode": "ckb",
        "langName": "کوردی",
        "langNameEN": "Central Kurdish",
        "name": "ویکیپیدیا",
        "url": "https://ckb.wikipedia.org"
    },
    "cowiki": {
        "langCode": "co",
        "langName": "corsu",
        "langNameEN": "Corsican",
        "name": "Wikipedia",
        "url": "https://co.wikipedia.org"
    },
    "cowiktionary": {
        "langCode": "co",
        "langName": "corsu",
        "langNameEN": "Corsican",
        "name": "Wiktionary",
        "url": "https://co.wiktionary.org"
    },
    "crwiki": {
        "langCode": "cr",
        "langName": "Nēhiyawēwin / ᓀᐦᐃᔭᐍᐏᐣ",
        "langNameEN": "Cree",
        "name": "Wikipedia",
        "url": "https://cr.wikipedia.org"
    },
    "crhwiki": {
        "langCode": "crh",
        "langName": "qırımtatarca",
        "langNameEN": "Crimean Tatar",
        "name": "Vikipediya",
        "url": "https://crh.wikipedia.org"
    },
    "cswiki": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikipedie",
        "url": "https://cs.wikipedia.org"
    },
    "cswiktionary": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikislovník",
        "url": "https://cs.wiktionary.org"
    },
    "cswikibooks": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikiknihy",
        "url": "https://cs.wikibooks.org"
    },
    "cswikinews": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikizprávy",
        "url": "https://cs.wikinews.org"
    },
    "cswikiquote": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikicitáty",
        "url": "https://cs.wikiquote.org"
    },
    "cswikisource": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikizdroje",
        "url": "https://cs.wikisource.org"
    },
    "cswikiversity": {
        "langCode": "cs",
        "langName": "čeština",
        "langNameEN": "Czech",
        "name": "Wikiverzita",
        "url": "https://cs.wikiversity.org"
    },
    "csbwiki": {
        "langCode": "csb",
        "langName": "kaszëbsczi",
        "langNameEN": "Kashubian",
        "name": "Wikipedia",
        "url": "https://csb.wikipedia.org"
    },
    "csbwiktionary": {
        "langCode": "csb",
        "langName": "kaszëbsczi",
        "langNameEN": "Kashubian",
        "name": "Wiktionary",
        "url": "https://csb.wiktionary.org"
    },
    "cuwiki": {
        "langCode": "cu",
        "langName": "словѣньскъ / ⰔⰎⰑⰂⰡⰐⰠⰔⰍⰟ",
        "langNameEN": "Church Slavic",
        "name": "Википєдїꙗ",
        "url": "https://cu.wikipedia.org"
    },
    "cvwiki": {
        "langCode": "cv",
        "langName": "чӑвашла",
        "langNameEN": "Chuvash",
        "name": "Википеди",
        "url": "https://cv.wikipedia.org"
    },
    "cvwikibooks": {
        "langCode": "cv",
        "langName": "чӑвашла",
        "langNameEN": "Chuvash",
        "name": "Wikibooks",
        "url": "https://cv.wikibooks.org"
    },
    "cywiki": {
        "langCode": "cy",
        "langName": "Cymraeg",
        "langNameEN": "Welsh",
        "name": "Wicipedia",
        "url": "https://cy.wikipedia.org"
    },
    "cywiktionary": {
        "langCode": "cy",
        "langName": "Cymraeg",
        "langNameEN": "Welsh",
        "name": "Wiciadur",
        "url": "https://cy.wiktionary.org"
    },
    "cywikibooks": {
        "langCode": "cy",
        "langName": "Cymraeg",
        "langNameEN": "Welsh",
        "name": "Wicilyfrau",
        "url": "https://cy.wikibooks.org"
    },
    "cywikiquote": {
        "langCode": "cy",
        "langName": "Cymraeg",
        "langNameEN": "Welsh",
        "name": "Wikiquote",
        "url": "https://cy.wikiquote.org"
    },
    "cywikisource": {
        "langCode": "cy",
        "langName": "Cymraeg",
        "langNameEN": "Welsh",
        "name": "Wicidestun",
        "url": "https://cy.wikisource.org"
    },
    "dawiki": {
        "langCode": "da",
        "langName": "dansk",
        "langNameEN": "Danish",
        "name": "Wikipedia",
        "url": "https://da.wikipedia.org"
    },
    "dawiktionary": {
        "langCode": "da",
        "langName": "dansk",
        "langNameEN": "Danish",
        "name": "Wiktionary",
        "url": "https://da.wiktionary.org"
    },
    "dawikibooks": {
        "langCode": "da",
        "langName": "dansk",
        "langNameEN": "Danish",
        "name": "Wikibooks",
        "url": "https://da.wikibooks.org"
    },
    "dawikiquote": {
        "langCode": "da",
        "langName": "dansk",
        "langNameEN": "Danish",
        "name": "Wikiquote",
        "url": "https://da.wikiquote.org"
    },
    "dawikisource": {
        "langCode": "da",
        "langName": "dansk",
        "langNameEN": "Danish",
        "name": "Wikisource",
        "url": "https://da.wikisource.org"
    },
    "dagwiki": {
        "langCode": "dag",
        "langName": "dagbanli",
        "langNameEN": "Dagbani",
        "name": "Dagbani Wikipedia",
        "url": "https://dag.wikipedia.org"
    },
    "dewiki": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikipedia",
        "url": "https://de.wikipedia.org"
    },
    "dewiktionary": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wiktionary",
        "url": "https://de.wiktionary.org"
    },
    "dewikibooks": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikibooks",
        "url": "https://de.wikibooks.org"
    },
    "dewikinews": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikinews",
        "url": "https://de.wikinews.org"
    },
    "dewikiquote": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikiquote",
        "url": "https://de.wikiquote.org"
    },
    "dewikisource": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikisource",
        "url": "https://de.wikisource.org"
    },
    "dewikiversity": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikiversity",
        "url": "https://de.wikiversity.org"
    },
    "dewikivoyage": {
        "langCode": "de",
        "langName": "Deutsch",
        "langNameEN": "German",
        "name": "Wikivoyage",
        "url": "https://de.wikivoyage.org"
    },
    "dinwiki": {
        "langCode": "din",
        "langName": "Thuɔŋjäŋ",
        "langNameEN": "Dinka",
        "name": "Wikipedia",
        "url": "https://din.wikipedia.org"
    },
    "diqwiki": {
        "langCode": "diq",
        "langName": "Zazaki",
        "langNameEN": "Zazaki",
        "name": "Wikipedia",
        "url": "https://diq.wikipedia.org"
    },
    "diqwiktionary": {
        "langCode": "diq",
        "langName": "Zazaki",
        "langNameEN": "Zazaki",
        "name": "Wikiqısebend",
        "url": "https://diq.wiktionary.org"
    },
    "dsbwiki": {
        "langCode": "dsb",
        "langName": "dolnoserbski",
        "langNameEN": "Lower Sorbian",
        "name": "Wikipedija",
        "url": "https://dsb.wikipedia.org"
    },
    "dtywiki": {
        "langCode": "dty",
        "langName": "डोटेली",
        "langNameEN": "Doteli",
        "name": "विकिपिडिया",
        "url": "https://dty.wikipedia.org"
    },
    "dvwiki": {
        "langCode": "dv",
        "langName": "ދިވެހިބަސް",
        "langNameEN": "Divehi",
        "name": "ވިކިޕީޑިއާ",
        "url": "https://dv.wikipedia.org"
    },
    "dvwiktionary": {
        "langCode": "dv",
        "langName": "ދިވެހިބަސް",
        "langNameEN": "Divehi",
        "name": "ވިކިރަދީފު",
        "url": "https://dv.wiktionary.org"
    },
    "dzwiki": {
        "langCode": "dz",
        "langName": "ཇོང་ཁ",
        "langNameEN": "Dzongkha",
        "name": "Wikipedia",
        "url": "https://dz.wikipedia.org"
    },
    "eewiki": {
        "langCode": "ee",
        "langName": "eʋegbe",
        "langNameEN": "Ewe",
        "name": "Wikipedia",
        "url": "https://ee.wikipedia.org"
    },
    "elwiki": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιπαίδεια",
        "url": "https://el.wikipedia.org"
    },
    "elwiktionary": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιλεξικό",
        "url": "https://el.wiktionary.org"
    },
    "elwikibooks": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιβιβλία",
        "url": "https://el.wikibooks.org"
    },
    "elwikinews": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικινέα",
        "url": "https://el.wikinews.org"
    },
    "elwikiquote": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιφθέγματα",
        "url": "https://el.wikiquote.org"
    },
    "elwikisource": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιθήκη",
        "url": "https://el.wikisource.org"
    },
    "elwikiversity": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιεπιστήμιο",
        "url": "https://el.wikiversity.org"
    },
    "elwikivoyage": {
        "langCode": "el",
        "langName": "Ελληνικά",
        "langNameEN": "Greek",
        "name": "Βικιταξίδια",
        "url": "https://el.wikivoyage.org"
    },
    "emlwiki": {
        "langCode": "eml",
        "langName": "emiliàn e rumagnòl",
        "langNameEN": "Emiliano-Romagnolo",
        "name": "Wikipedia",
        "url": "https://eml.wikipedia.org"
    },
    "enwiki": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikipedia",
        "url": "https://en.wikipedia.org"
    },
    "enwiktionary": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wiktionary",
        "url": "https://en.wiktionary.org"
    },
    "enwikibooks": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikibooks",
        "url": "https://en.wikibooks.org"
    },
    "enwikinews": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikinews",
        "url": "https://en.wikinews.org"
    },
    "enwikiquote": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikiquote",
        "url": "https://en.wikiquote.org"
    },
    "enwikisource": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikisource",
        "url": "https://en.wikisource.org"
    },
    "enwikiversity": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikiversity",
        "url": "https://en.wikiversity.org"
    },
    "enwikivoyage": {
        "langCode": "en",
        "langName": "English",
        "langNameEN": "English",
        "name": "Wikivoyage",
        "url": "https://en.wikivoyage.org"
    },
    "eowiki": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikipedio",
        "url": "https://eo.wikipedia.org"
    },
    "eowiktionary": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikivortaro",
        "url": "https://eo.wiktionary.org"
    },
    "eowikibooks": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikilibroj",
        "url": "https://eo.wikibooks.org"
    },
    "eowikinews": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikinovaĵoj",
        "url": "https://eo.wikinews.org"
    },
    "eowikiquote": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikicitaro",
        "url": "https://eo.wikiquote.org"
    },
    "eowikisource": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikifontaro",
        "url": "https://eo.wikisource.org"
    },
    "eowikivoyage": {
        "langCode": "eo",
        "langName": "Esperanto",
        "langNameEN": "Esperanto",
        "name": "Vikivojaĝo",
        "url": "https://eo.wikivoyage.org"
    },
    "eswiki": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikipedia",
        "url": "https://es.wikipedia.org"
    },
    "eswiktionary": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikcionario",
        "url": "https://es.wiktionary.org"
    },
    "eswikibooks": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikilibros",
        "url": "https://es.wikibooks.org"
    },
    "eswikinews": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikinoticias",
        "url": "https://es.wikinews.org"
    },
    "eswikiquote": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikiquote",
        "url": "https://es.wikiquote.org"
    },
    "eswikisource": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikisource",
        "url": "https://es.wikisource.org"
    },
    "eswikiversity": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikiversidad",
        "url": "https://es.wikiversity.org"
    },
    "eswikivoyage": {
        "langCode": "es",
        "langName": "español",
        "langNameEN": "Spanish",
        "name": "Wikiviajes",
        "url": "https://es.wikivoyage.org"
    },
    "etwiki": {
        "langCode": "et",
        "langName": "eesti",
        "langNameEN": "Estonian",
        "name": "Vikipeedia",
        "url": "https://et.wikipedia.org"
    },
    "etwiktionary": {
        "langCode": "et",
        "langName": "eesti",
        "langNameEN": "Estonian",
        "name": "Vikisõnastik",
        "url": "https://et.wiktionary.org"
    },
    "etwikibooks": {
        "langCode": "et",
        "langName": "eesti",
        "langNameEN": "Estonian",
        "name": "Vikiõpikud",
        "url": "https://et.wikibooks.org"
    },
    "etwikiquote": {
        "langCode": "et",
        "langName": "eesti",
        "langNameEN": "Estonian",
        "name": "Vikitsitaadid",
        "url": "https://et.wikiquote.org"
    },
    "etwikisource": {
        "langCode": "et",
        "langName": "eesti",
        "langNameEN": "Estonian",
        "name": "Vikitekstid",
        "url": "https://et.wikisource.org"
    },
    "euwiki": {
        "langCode": "eu",
        "langName": "euskara",
        "langNameEN": "Basque",
        "name": "Wikipedia",
        "url": "https://eu.wikipedia.org"
    },
    "euwiktionary": {
        "langCode": "eu",
        "langName": "euskara",
        "langNameEN": "Basque",
        "name": "Wiktionary",
        "url": "https://eu.wiktionary.org"
    },
    "euwikibooks": {
        "langCode": "eu",
        "langName": "euskara",
        "langNameEN": "Basque",
        "name": "Wikibooks",
        "url": "https://eu.wikibooks.org"
    },
    "euwikiquote": {
        "langCode": "eu",
        "langName": "euskara",
        "langNameEN": "Basque",
        "name": "Wikiquote",
        "url": "https://eu.wikiquote.org"
    },
    "euwikisource": {
        "langCode": "eu",
        "langName": "euskara",
        "langNameEN": "Basque",
        "name": "Wikiteka",
        "url": "https://eu.wikisource.org"
    },
    "extwiki": {
        "langCode": "ext",
        "langName": "estremeñu",
        "langNameEN": "Extremaduran",
        "name": "Güiquipeya",
        "url": "https://ext.wikipedia.org"
    },
    "fawiki": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌پدیا",
        "url": "https://fa.wikipedia.org"
    },
    "fawiktionary": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌واژه",
        "url": "https://fa.wiktionary.org"
    },
    "fawikibooks": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌کتاب",
        "url": "https://fa.wikibooks.org"
    },
    "fawikinews": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌خبر",
        "url": "https://fa.wikinews.org"
    },
    "fawikiquote": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌گفتاورد",
        "url": "https://fa.wikiquote.org"
    },
    "fawikisource": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌نبشته",
        "url": "https://fa.wikisource.org"
    },
    "fawikivoyage": {
        "langCode": "fa",
        "langName": "فارسی",
        "langNameEN": "Persian",
        "name": "ویکی‌سفر",
        "url": "https://fa.wikivoyage.org"
    },
    "ffwiki": {
        "langCode": "ff",
        "langName": "Fulfulde",
        "langNameEN": "Fulah",
        "name": "Wikipedia",
        "url": "https://ff.wikipedia.org"
    },
    "fiwiki": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikipedia",
        "url": "https://fi.wikipedia.org"
    },
    "fiwiktionary": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikisanakirja",
        "url": "https://fi.wiktionary.org"
    },
    "fiwikibooks": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikikirjasto",
        "url": "https://fi.wikibooks.org"
    },
    "fiwikinews": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikiuutiset",
        "url": "https://fi.wikinews.org"
    },
    "fiwikiquote": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikisitaatit",
        "url": "https://fi.wikiquote.org"
    },
    "fiwikisource": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikiaineisto",
        "url": "https://fi.wikisource.org"
    },
    "fiwikiversity": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikiopisto",
        "url": "https://fi.wikiversity.org"
    },
    "fiwikivoyage": {
        "langCode": "fi",
        "langName": "suomi",
        "langNameEN": "Finnish",
        "name": "Wikimatkat",
        "url": "https://fi.wikivoyage.org"
    },
    "fiu_vrowiki": {
        "langCode": "fiu-vro",
        "langName": "võro",
        "langNameEN": "võro",
        "name": "Wikipedia",
        "url": "https://fiu-vro.wikipedia.org"
    },
    "fjwiki": {
        "langCode": "fj",
        "langName": "Na Vosa Vakaviti",
        "langNameEN": "Fijian",
        "name": "Wikipedia",
        "url": "https://fj.wikipedia.org"
    },
    "fjwiktionary": {
        "langCode": "fj",
        "langName": "Na Vosa Vakaviti",
        "langNameEN": "Fijian",
        "name": "Wiktionary",
        "url": "https://fj.wiktionary.org"
    },
    "fowiki": {
        "langCode": "fo",
        "langName": "føroyskt",
        "langNameEN": "Faroese",
        "name": "Wikipedia",
        "url": "https://fo.wikipedia.org"
    },
    "fowiktionary": {
        "langCode": "fo",
        "langName": "føroyskt",
        "langNameEN": "Faroese",
        "name": "Wiktionary",
        "url": "https://fo.wiktionary.org"
    },
    "fowikisource": {
        "langCode": "fo",
        "langName": "føroyskt",
        "langNameEN": "Faroese",
        "name": "Wikiheimild",
        "url": "https://fo.wikisource.org"
    },
    "frwiki": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikipédia",
        "url": "https://fr.wikipedia.org"
    },
    "frwiktionary": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wiktionnaire",
        "url": "https://fr.wiktionary.org"
    },
    "frwikibooks": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikilivres",
        "url": "https://fr.wikibooks.org"
    },
    "frwikinews": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikinews",
        "url": "https://fr.wikinews.org"
    },
    "frwikiquote": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikiquote",
        "url": "https://fr.wikiquote.org"
    },
    "frwikisource": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikisource",
        "url": "https://fr.wikisource.org"
    },
    "frwikiversity": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikiversité",
        "url": "https://fr.wikiversity.org"
    },
    "frwikivoyage": {
        "langCode": "fr",
        "langName": "français",
        "langNameEN": "French",
        "name": "Wikivoyage",
        "url": "https://fr.wikivoyage.org"
    },
    "frpwiki": {
        "langCode": "frp",
        "langName": "arpetan",
        "langNameEN": "Arpitan",
        "name": "Vouiquipèdia",
        "url": "https://frp.wikipedia.org"
    },
    "frrwiki": {
        "langCode": "frr",
        "langName": "Nordfriisk",
        "langNameEN": "Northern Frisian",
        "name": "Wikipedia",
        "url": "https://frr.wikipedia.org"
    },
    "furwiki": {
        "langCode": "fur",
        "langName": "furlan",
        "langNameEN": "Friulian",
        "name": "Vichipedie",
        "url": "https://fur.wikipedia.org"
    },
    "fywiki": {
        "langCode": "fy",
        "langName": "Frysk",
        "langNameEN": "Western Frisian",
        "name": "Wikipedy",
        "url": "https://fy.wikipedia.org"
    },
    "fywiktionary": {
        "langCode": "fy",
        "langName": "Frysk",
        "langNameEN": "Western Frisian",
        "name": "Wikiwurdboek",
        "url": "https://fy.wiktionary.org"
    },
    "fywikibooks": {
        "langCode": "fy",
        "langName": "Frysk",
        "langNameEN": "Western Frisian",
        "name": "Wikibooks",
        "url": "https://fy.wikibooks.org"
    },
    "gawiki": {
        "langCode": "ga",
        "langName": "Gaeilge",
        "langNameEN": "Irish",
        "name": "Vicipéid",
        "url": "https://ga.wikipedia.org"
    },
    "gawiktionary": {
        "langCode": "ga",
        "langName": "Gaeilge",
        "langNameEN": "Irish",
        "name": "Vicífhoclóir",
        "url": "https://ga.wiktionary.org"
    },
    "gagwiki": {
        "langCode": "gag",
        "langName": "Gagauz",
        "langNameEN": "Gagauz",
        "name": "Vikipediya",
        "url": "https://gag.wikipedia.org"
    },
    "ganwiki": {
        "langCode": "gan",
        "langName": "贛語",
        "langNameEN": "Gan Chinese",
        "name": "維基百科",
        "url": "https://gan.wikipedia.org"
    },
    "gcrwiki": {
        "langCode": "gcr",
        "langName": "kriyòl gwiyannen",
        "langNameEN": "Guianan Creole",
        "name": "Wikipédja",
        "url": "https://gcr.wikipedia.org"
    },
    "gdwiki": {
        "langCode": "gd",
        "langName": "Gàidhlig",
        "langNameEN": "Scottish Gaelic",
        "name": "Uicipeid",
        "url": "https://gd.wikipedia.org"
    },
    "gdwiktionary": {
        "langCode": "gd",
        "langName": "Gàidhlig",
        "langNameEN": "Scottish Gaelic",
        "name": "Wiktionary",
        "url": "https://gd.wiktionary.org"
    },
    "glwiki": {
        "langCode": "gl",
        "langName": "galego",
        "langNameEN": "Galician",
        "name": "Wikipedia",
        "url": "https://gl.wikipedia.org"
    },
    "glwiktionary": {
        "langCode": "gl",
        "langName": "galego",
        "langNameEN": "Galician",
        "name": "Wiktionary",
        "url": "https://gl.wiktionary.org"
    },
    "glwikibooks": {
        "langCode": "gl",
        "langName": "galego",
        "langNameEN": "Galician",
        "name": "Wikibooks",
        "url": "https://gl.wikibooks.org"
    },
    "glwikiquote": {
        "langCode": "gl",
        "langName": "galego",
        "langNameEN": "Galician",
        "name": "Wikiquote",
        "url": "https://gl.wikiquote.org"
    },
    "glwikisource": {
        "langCode": "gl",
        "langName": "galego",
        "langNameEN": "Galician",
        "name": "Wikisource",
        "url": "https://gl.wikisource.org"
    },
    "glkwiki": {
        "langCode": "glk",
        "langName": "گیلکی",
        "langNameEN": "Gilaki",
        "name": "Wikipedia",
        "url": "https://glk.wikipedia.org"
    },
    "gnwiki": {
        "langCode": "gn",
        "langName": "Avañe'ẽ",
        "langNameEN": "Guarani",
        "name": "Vikipetã",
        "url": "https://gn.wikipedia.org"
    },
    "gnwiktionary": {
        "langCode": "gn",
        "langName": "Avañe'ẽ",
        "langNameEN": "Guarani",
        "name": "Wiktionary",
        "url": "https://gn.wiktionary.org"
    },
    "gomwiki": {
        "langCode": "gom",
        "langName": "गोंयची कोंकणी / Gõychi Konknni",
        "langNameEN": "Goan Konkani",
        "name": "विकिपीडिया",
        "url": "https://gom.wikipedia.org"
    },
    "gomwiktionary": {
        "langCode": "gom",
        "langName": "गोंयची कोंकणी / Gõychi Konknni",
        "langNameEN": "Goan Konkani",
        "name": "Wiktionary",
        "url": "https://gom.wiktionary.org"
    },
    "gorwiki": {
        "langCode": "gor",
        "langName": "Bahasa Hulontalo",
        "langNameEN": "Gorontalo",
        "name": "Wikipedia",
        "url": "https://gor.wikipedia.org"
    },
    "gotwiki": {
        "langCode": "got",
        "langName": "𐌲𐌿𐍄𐌹𐍃𐌺",
        "langNameEN": "Gothic",
        "name": "Wikipedia",
        "url": "https://got.wikipedia.org"
    },
    "guwiki": {
        "langCode": "gu",
        "langName": "ગુજરાતી",
        "langNameEN": "Gujarati",
        "name": "વિકિપીડિયા",
        "url": "https://gu.wikipedia.org"
    },
    "guwiktionary": {
        "langCode": "gu",
        "langName": "ગુજરાતી",
        "langNameEN": "Gujarati",
        "name": "વિકિકોશ",
        "url": "https://gu.wiktionary.org"
    },
    "guwikiquote": {
        "langCode": "gu",
        "langName": "ગુજરાતી",
        "langNameEN": "Gujarati",
        "name": "વિકિસૂક્તિ",
        "url": "https://gu.wikiquote.org"
    },
    "guwikisource": {
        "langCode": "gu",
        "langName": "ગુજરાતી",
        "langNameEN": "Gujarati",
        "name": "વિકિસ્રોત",
        "url": "https://gu.wikisource.org"
    },
    "gvwiki": {
        "langCode": "gv",
        "langName": "Gaelg",
        "langNameEN": "Manx",
        "name": "Wikipedia",
        "url": "https://gv.wikipedia.org"
    },
    "gvwiktionary": {
        "langCode": "gv",
        "langName": "Gaelg",
        "langNameEN": "Manx",
        "name": "Wiktionary",
        "url": "https://gv.wiktionary.org"
    },
    "hawiki": {
        "langCode": "ha",
        "langName": "Hausa",
        "langNameEN": "Hausa",
        "name": "Wikipedia",
        "url": "https://ha.wikipedia.org"
    },
    "hawiktionary": {
        "langCode": "ha",
        "langName": "Hausa",
        "langNameEN": "Hausa",
        "name": "Wiktionary",
        "url": "https://ha.wiktionary.org"
    },
    "hakwiki": {
        "langCode": "hak",
        "langName": "客家語/Hak-kâ-ngî",
        "langNameEN": "Hakka Chinese",
        "name": "Wikipedia",
        "url": "https://hak.wikipedia.org"
    },
    "hawwiki": {
        "langCode": "haw",
        "langName": "Hawaiʻi",
        "langNameEN": "Hawaiian",
        "name": "Wikipedia",
        "url": "https://haw.wikipedia.org"
    },
    "hewiki": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקיפדיה",
        "url": "https://he.wikipedia.org"
    },
    "hewiktionary": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקימילון",
        "url": "https://he.wiktionary.org"
    },
    "hewikibooks": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקיספר",
        "url": "https://he.wikibooks.org"
    },
    "hewikinews": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקיחדשות",
        "url": "https://he.wikinews.org"
    },
    "hewikiquote": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקיציטוט",
        "url": "https://he.wikiquote.org"
    },
    "hewikisource": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקיטקסט",
        "url": "https://he.wikisource.org"
    },
    "hewikivoyage": {
        "langCode": "he",
        "langName": "עברית",
        "langNameEN": "Hebrew",
        "name": "ויקימסע",
        "url": "https://he.wikivoyage.org"
    },
    "hiwiki": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विकिपीडिया",
        "url": "https://hi.wikipedia.org"
    },
    "hiwiktionary": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विक्षनरी",
        "url": "https://hi.wiktionary.org"
    },
    "hiwikibooks": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विकिपुस्तक",
        "url": "https://hi.wikibooks.org"
    },
    "hiwikiquote": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विकिसूक्ति",
        "url": "https://hi.wikiquote.org"
    },
    "hiwikisource": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विकिस्रोत",
        "url": "https://hi.wikisource.org"
    },
    "hiwikiversity": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विकिविश्वविद्यालय",
        "url": "https://hi.wikiversity.org"
    },
    "hiwikivoyage": {
        "langCode": "hi",
        "langName": "हिन्दी",
        "langNameEN": "Hindi",
        "name": "विकियात्रा",
        "url": "https://hi.wikivoyage.org"
    },
    "hifwiki": {
        "langCode": "hif",
        "langName": "Fiji Hindi",
        "langNameEN": "Fiji Hindi",
        "name": "Wikipedia",
        "url": "https://hif.wikipedia.org"
    },
    "hifwiktionary": {
        "langCode": "hif",
        "langName": "Fiji Hindi",
        "langNameEN": "Fiji Hindi",
        "name": "Sabdkosh",
        "url": "https://hif.wiktionary.org"
    },
    "hrwiki": {
        "langCode": "hr",
        "langName": "hrvatski",
        "langNameEN": "Croatian",
        "name": "Wikipedija",
        "url": "https://hr.wikipedia.org"
    },
    "hrwiktionary": {
        "langCode": "hr",
        "langName": "hrvatski",
        "langNameEN": "Croatian",
        "name": "Wiktionary",
        "url": "https://hr.wiktionary.org"
    },
    "hrwikibooks": {
        "langCode": "hr",
        "langName": "hrvatski",
        "langNameEN": "Croatian",
        "name": "Wikibooks",
        "url": "https://hr.wikibooks.org"
    },
    "hrwikiquote": {
        "langCode": "hr",
        "langName": "hrvatski",
        "langNameEN": "Croatian",
        "name": "Wikicitat",
        "url": "https://hr.wikiquote.org"
    },
    "hrwikisource": {
        "langCode": "hr",
        "langName": "hrvatski",
        "langNameEN": "Croatian",
        "name": "Wikizvor",
        "url": "https://hr.wikisource.org"
    },
    "hsbwiki": {
        "langCode": "hsb",
        "langName": "hornjoserbsce",
        "langNameEN": "Upper Sorbian",
        "name": "Wikipedija",
        "url": "https://hsb.wikipedia.org"
    },
    "hsbwiktionary": {
        "langCode": "hsb",
        "langName": "hornjoserbsce",
        "langNameEN": "Upper Sorbian",
        "name": "Wikisłownik",
        "url": "https://hsb.wiktionary.org"
    },
    "htwiki": {
        "langCode": "ht",
        "langName": "Kreyòl ayisyen",
        "langNameEN": "Haitian Creole",
        "name": "Wikipedya",
        "url": "https://ht.wikipedia.org"
    },
    "huwiki": {
        "langCode": "hu",
        "langName": "magyar",
        "langNameEN": "Hungarian",
        "name": "Wikipédia",
        "url": "https://hu.wikipedia.org"
    },
    "huwiktionary": {
        "langCode": "hu",
        "langName": "magyar",
        "langNameEN": "Hungarian",
        "name": "Wikiszótár",
        "url": "https://hu.wiktionary.org"
    },
    "huwikibooks": {
        "langCode": "hu",
        "langName": "magyar",
        "langNameEN": "Hungarian",
        "name": "Wikikönyvek",
        "url": "https://hu.wikibooks.org"
    },
    "huwikiquote": {
        "langCode": "hu",
        "langName": "magyar",
        "langNameEN": "Hungarian",
        "name": "Wikidézet",
        "url": "https://hu.wikiquote.org"
    },
    "huwikisource": {
        "langCode": "hu",
        "langName": "magyar",
        "langNameEN": "Hungarian",
        "name": "Wikiforrás",
        "url": "https://hu.wikisource.org"
    },
    "hywiki": {
        "langCode": "hy",
        "langName": "հայերեն",
        "langNameEN": "Armenian",
        "name": "Վիքիպեդիա",
        "url": "https://hy.wikipedia.org"
    },
    "hywiktionary": {
        "langCode": "hy",
        "langName": "հայերեն",
        "langNameEN": "Armenian",
        "name": "Վիքիբառարան",
        "url": "https://hy.wiktionary.org"
    },
    "hywikibooks": {
        "langCode": "hy",
        "langName": "հայերեն",
        "langNameEN": "Armenian",
        "name": "Վիքիգրքեր",
        "url": "https://hy.wikibooks.org"
    },
    "hywikiquote": {
        "langCode": "hy",
        "langName": "հայերեն",
        "langNameEN": "Armenian",
        "name": "Վիքիքաղվածք",
        "url": "https://hy.wikiquote.org"
    },
    "hywikisource": {
        "langCode": "hy",
        "langName": "հայերեն",
        "langNameEN": "Armenian",
        "name": "Վիքիդարան",
        "url": "https://hy.wikisource.org"
    },
    "hywwiki": {
        "langCode": "hyw",
        "langName": "Արեւմտահայերէն",
        "langNameEN": "Western Armenian",
        "name": "Ուիքիփետիա",
        "url": "https://hyw.wikipedia.org"
    },
    "iawiki": {
        "langCode": "ia",
        "langName": "interlingua",
        "langNameEN": "Interlingua",
        "name": "Wikipedia",
        "url": "https://ia.wikipedia.org"
    },
    "iawiktionary": {
        "langCode": "ia",
        "langName": "interlingua",
        "langNameEN": "Interlingua",
        "name": "Wiktionario",
        "url": "https://ia.wiktionary.org"
    },
    "iawikibooks": {
        "langCode": "ia",
        "langName": "interlingua",
        "langNameEN": "Interlingua",
        "name": "Wikibooks",
        "url": "https://ia.wikibooks.org"
    },
    "idwiki": {
        "langCode": "id",
        "langName": "Bahasa Indonesia",
        "langNameEN": "Indonesian",
        "name": "Wikipedia",
        "url": "https://id.wikipedia.org"
    },
    "idwiktionary": {
        "langCode": "id",
        "langName": "Bahasa Indonesia",
        "langNameEN": "Indonesian",
        "name": "Wiktionary",
        "url": "https://id.wiktionary.org"
    },
    "idwikibooks": {
        "langCode": "id",
        "langName": "Bahasa Indonesia",
        "langNameEN": "Indonesian",
        "name": "Wikibuku",
        "url": "https://id.wikibooks.org"
    },
    "idwikiquote": {
        "langCode": "id",
        "langName": "Bahasa Indonesia",
        "langNameEN": "Indonesian",
        "name": "Wikiquote",
        "url": "https://id.wikiquote.org"
    },
    "idwikisource": {
        "langCode": "id",
        "langName": "Bahasa Indonesia",
        "langNameEN": "Indonesian",
        "name": "Wikisource",
        "url": "https://id.wikisource.org"
    },
    "iewiki": {
        "langCode": "ie",
        "langName": "Interlingue",
        "langNameEN": "Interlingue",
        "name": "Wikipedia",
        "url": "https://ie.wikipedia.org"
    },
    "iewiktionary": {
        "langCode": "ie",
        "langName": "Interlingue",
        "langNameEN": "Interlingue",
        "name": "Wiktionary",
        "url": "https://ie.wiktionary.org"
    },
    "igwiki": {
        "langCode": "ig",
        "langName": "Igbo",
        "langNameEN": "Igbo",
        "name": "Wikipedia",
        "url": "https://ig.wikipedia.org"
    },
    "ikwiki": {
        "langCode": "ik",
        "langName": "Iñupiak",
        "langNameEN": "Inupiaq",
        "name": "Wikipedia",
        "url": "https://ik.wikipedia.org"
    },
    "ilowiki": {
        "langCode": "ilo",
        "langName": "Ilokano",
        "langNameEN": "Iloko",
        "name": "Wikipedia",
        "url": "https://ilo.wikipedia.org"
    },
    "inhwiki": {
        "langCode": "inh",
        "langName": "гӀалгӀай",
        "langNameEN": "Ingush",
        "name": "Википеди",
        "url": "https://inh.wikipedia.org"
    },
    "iowiki": {
        "langCode": "io",
        "langName": "Ido",
        "langNameEN": "Ido",
        "name": "Wikipedio",
        "url": "https://io.wikipedia.org"
    },
    "iowiktionary": {
        "langCode": "io",
        "langName": "Ido",
        "langNameEN": "Ido",
        "name": "Wikivortaro",
        "url": "https://io.wiktionary.org"
    },
    "iswiki": {
        "langCode": "is",
        "langName": "íslenska",
        "langNameEN": "Icelandic",
        "name": "Wikipedia",
        "url": "https://is.wikipedia.org"
    },
    "iswiktionary": {
        "langCode": "is",
        "langName": "íslenska",
        "langNameEN": "Icelandic",
        "name": "Wikiorðabók",
        "url": "https://is.wiktionary.org"
    },
    "iswikibooks": {
        "langCode": "is",
        "langName": "íslenska",
        "langNameEN": "Icelandic",
        "name": "Wikibækur",
        "url": "https://is.wikibooks.org"
    },
    "iswikiquote": {
        "langCode": "is",
        "langName": "íslenska",
        "langNameEN": "Icelandic",
        "name": "Wikivitnun",
        "url": "https://is.wikiquote.org"
    },
    "iswikisource": {
        "langCode": "is",
        "langName": "íslenska",
        "langNameEN": "Icelandic",
        "name": "Wikiheimild",
        "url": "https://is.wikisource.org"
    },
    "itwiki": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikipedia",
        "url": "https://it.wikipedia.org"
    },
    "itwiktionary": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikizionario",
        "url": "https://it.wiktionary.org"
    },
    "itwikibooks": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikibooks",
        "url": "https://it.wikibooks.org"
    },
    "itwikinews": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikinotizie",
        "url": "https://it.wikinews.org"
    },
    "itwikiquote": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikiquote",
        "url": "https://it.wikiquote.org"
    },
    "itwikisource": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikisource",
        "url": "https://it.wikisource.org"
    },
    "itwikiversity": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikiversità",
        "url": "https://it.wikiversity.org"
    },
    "itwikivoyage": {
        "langCode": "it",
        "langName": "italiano",
        "langNameEN": "Italian",
        "name": "Wikivoyage",
        "url": "https://it.wikivoyage.org"
    },
    "iuwiki": {
        "langCode": "iu",
        "langName": "ᐃᓄᒃᑎᑐᑦ/inuktitut",
        "langNameEN": "Inuktitut",
        "name": "ᐅᐃᑭᐱᑎᐊ",
        "url": "https://iu.wikipedia.org"
    },
    "iuwiktionary": {
        "langCode": "iu",
        "langName": "ᐃᓄᒃᑎᑐᑦ/inuktitut",
        "langNameEN": "Inuktitut",
        "name": "Wiktionary",
        "url": "https://iu.wiktionary.org"
    },
    "jawiki": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "Wikipedia",
        "url": "https://ja.wikipedia.org"
    },
    "jawiktionary": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "Wiktionary",
        "url": "https://ja.wiktionary.org"
    },
    "jawikibooks": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "Wikibooks",
        "url": "https://ja.wikibooks.org"
    },
    "jawikinews": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "ウィキニュース",
        "url": "https://ja.wikinews.org"
    },
    "jawikiquote": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "Wikiquote",
        "url": "https://ja.wikiquote.org"
    },
    "jawikisource": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "Wikisource",
        "url": "https://ja.wikisource.org"
    },
    "jawikiversity": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "ウィキバーシティ",
        "url": "https://ja.wikiversity.org"
    },
    "jawikivoyage": {
        "langCode": "ja",
        "langName": "日本語",
        "langNameEN": "Japanese",
        "name": "ウィキボヤージュ",
        "url": "https://ja.wikivoyage.org"
    },
    "jamwiki": {
        "langCode": "jam",
        "langName": "Patois",
        "langNameEN": "Jamaican Creole English",
        "name": "Wikipidia",
        "url": "https://jam.wikipedia.org"
    },
    "jbowiki": {
        "langCode": "jbo",
        "langName": "la .lojban.",
        "langNameEN": "Lojban",
        "name": "Wikipedia",
        "url": "https://jbo.wikipedia.org"
    },
    "jbowiktionary": {
        "langCode": "jbo",
        "langName": "la .lojban.",
        "langNameEN": "Lojban",
        "name": "Wiktionary",
        "url": "https://jbo.wiktionary.org"
    },
    "jvwiki": {
        "langCode": "jv",
        "langName": "Jawa",
        "langNameEN": "Javanese",
        "name": "Wikipédia",
        "url": "https://jv.wikipedia.org"
    },
    "jvwiktionary": {
        "langCode": "jv",
        "langName": "Jawa",
        "langNameEN": "Javanese",
        "name": "Wikisastra",
        "url": "https://jv.wiktionary.org"
    },
    "jvwikisource": {
        "langCode": "jv",
        "langName": "Jawa",
        "langNameEN": "Javanese",
        "name": "Wikisumber",
        "url": "https://jv.wikisource.org"
    },
    "kawiki": {
        "langCode": "ka",
        "langName": "ქართული",
        "langNameEN": "Georgian",
        "name": "ვიკიპედია",
        "url": "https://ka.wikipedia.org"
    },
    "kawiktionary": {
        "langCode": "ka",
        "langName": "ქართული",
        "langNameEN": "Georgian",
        "name": "ვიქსიკონი",
        "url": "https://ka.wiktionary.org"
    },
    "kawikibooks": {
        "langCode": "ka",
        "langName": "ქართული",
        "langNameEN": "Georgian",
        "name": "ვიკიწიგნები",
        "url": "https://ka.wikibooks.org"
    },
    "kawikiquote": {
        "langCode": "ka",
        "langName": "ქართული",
        "langNameEN": "Georgian",
        "name": "ვიკიციტატა",
        "url": "https://ka.wikiquote.org"
    },
    "kaawiki": {
        "langCode": "kaa",
        "langName": "Qaraqalpaqsha",
        "langNameEN": "Kara-Kalpak",
        "name": "Wikipedia",
        "url": "https://kaa.wikipedia.org"
    },
    "kabwiki": {
        "langCode": "kab",
        "langName": "Taqbaylit",
        "langNameEN": "Kabyle",
        "name": "Wikipedia",
        "url": "https://kab.wikipedia.org"
    },
    "kbdwiki": {
        "langCode": "kbd",
        "langName": "адыгэбзэ",
        "langNameEN": "Kabardian",
        "name": "Уикипедиэ",
        "url": "https://kbd.wikipedia.org"
    },
    "kbpwiki": {
        "langCode": "kbp",
        "langName": "Kabɩyɛ",
        "langNameEN": "Kabiye",
        "name": "Wikipediya",
        "url": "https://kbp.wikipedia.org"
    },
    "kgwiki": {
        "langCode": "kg",
        "langName": "Kongo",
        "langNameEN": "Kongo",
        "name": "Wikipedia",
        "url": "https://kg.wikipedia.org"
    },
    "kiwiki": {
        "langCode": "ki",
        "langName": "Gĩkũyũ",
        "langNameEN": "Kikuyu",
        "name": "Wikipedia",
        "url": "https://ki.wikipedia.org"
    },
    "kkwiki": {
        "langCode": "kk",
        "langName": "қазақша",
        "langNameEN": "Kazakh",
        "name": "Уикипедия",
        "url": "https://kk.wikipedia.org"
    },
    "kkwiktionary": {
        "langCode": "kk",
        "langName": "қазақша",
        "langNameEN": "Kazakh",
        "name": "Уикисөздік",
        "url": "https://kk.wiktionary.org"
    },
    "kkwikibooks": {
        "langCode": "kk",
        "langName": "қазақша",
        "langNameEN": "Kazakh",
        "name": "Уикикітап",
        "url": "https://kk.wikibooks.org"
    },
    "klwiki": {
        "langCode": "kl",
        "langName": "kalaallisut",
        "langNameEN": "Kalaallisut",
        "name": "Wikipedia",
        "url": "https://kl.wikipedia.org"
    },
    "klwiktionary": {
        "langCode": "kl",
        "langName": "kalaallisut",
        "langNameEN": "Kalaallisut",
        "name": "Wiktionary",
        "url": "https://kl.wiktionary.org"
    },
    "kmwiki": {
        "langCode": "km",
        "langName": "ភាសាខ្មែរ",
        "langNameEN": "Khmer",
        "name": "វិគីភីឌា",
        "url": "https://km.wikipedia.org"
    },
    "kmwiktionary": {
        "langCode": "km",
        "langName": "ភាសាខ្មែរ",
        "langNameEN": "Khmer",
        "name": "Wiktionary",
        "url": "https://km.wiktionary.org"
    },
    "kmwikibooks": {
        "langCode": "km",
        "langName": "ភាសាខ្មែរ",
        "langNameEN": "Khmer",
        "name": "Wikibooks",
        "url": "https://km.wikibooks.org"
    },
    "knwiki": {
        "langCode": "kn",
        "langName": "ಕನ್ನಡ",
        "langNameEN": "Kannada",
        "name": "ವಿಕಿಪೀಡಿಯ",
        "url": "https://kn.wikipedia.org"
    },
    "knwiktionary": {
        "langCode": "kn",
        "langName": "ಕನ್ನಡ",
        "langNameEN": "Kannada",
        "name": "ವಿಕ್ಷನರಿ",
        "url": "https://kn.wiktionary.org"
    },
    "knwikiquote": {
        "langCode": "kn",
        "langName": "ಕನ್ನಡ",
        "langNameEN": "Kannada",
        "name": "ವಿಕಿಕೋಟ್",
        "url": "https://kn.wikiquote.org"
    },
    "knwikisource": {
        "langCode": "kn",
        "langName": "ಕನ್ನಡ",
        "langNameEN": "Kannada",
        "name": "ವಿಕಿಸೋರ್ಸ್",
        "url": "https://kn.wikisource.org"
    },
    "kowiki": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키백과",
        "url": "https://ko.wikipedia.org"
    },
    "kowiktionary": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키낱말사전",
        "url": "https://ko.wiktionary.org"
    },
    "kowikibooks": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키책",
        "url": "https://ko.wikibooks.org"
    },
    "kowikinews": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키뉴스",
        "url": "https://ko.wikinews.org"
    },
    "kowikiquote": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키인용집",
        "url": "https://ko.wikiquote.org"
    },
    "kowikisource": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키문헌",
        "url": "https://ko.wikisource.org"
    },
    "kowikiversity": {
        "langCode": "ko",
        "langName": "한국어",
        "langNameEN": "Korean",
        "name": "위키배움터",
        "url": "https://ko.wikiversity.org"
    },
    "koiwiki": {
        "langCode": "koi",
        "langName": "перем коми",
        "langNameEN": "Komi-Permyak",
        "name": "Википедия",
        "url": "https://koi.wikipedia.org"
    },
    "krcwiki": {
        "langCode": "krc",
        "langName": "къарачай-малкъар",
        "langNameEN": "Karachay-Balkar",
        "name": "Википедия",
        "url": "https://krc.wikipedia.org"
    },
    "kswiki": {
        "langCode": "ks",
        "langName": "कॉशुर / کٲشُر",
        "langNameEN": "Kashmiri",
        "name": "وِکیٖپیٖڈیا",
        "url": "https://ks.wikipedia.org"
    },
    "kswiktionary": {
        "langCode": "ks",
        "langName": "कॉशुर / کٲشُر",
        "langNameEN": "Kashmiri",
        "name": "وِکیٖلۄغَتھ",
        "url": "https://ks.wiktionary.org"
    },
    "kshwiki": {
        "langCode": "ksh",
        "langName": "Ripoarisch",
        "langNameEN": "Colognian",
        "name": "Wikipedia",
        "url": "https://ksh.wikipedia.org"
    },
    "kuwiki": {
        "langCode": "ku",
        "langName": "kurdî",
        "langNameEN": "Kurdish",
        "name": "Wîkîpediya",
        "url": "https://ku.wikipedia.org"
    },
    "kuwiktionary": {
        "langCode": "ku",
        "langName": "kurdî",
        "langNameEN": "Kurdish",
        "name": "Wîkîferheng",
        "url": "https://ku.wiktionary.org"
    },
    "kuwikibooks": {
        "langCode": "ku",
        "langName": "kurdî",
        "langNameEN": "Kurdish",
        "name": "Wikibooks",
        "url": "https://ku.wikibooks.org"
    },
    "kuwikiquote": {
        "langCode": "ku",
        "langName": "kurdî",
        "langNameEN": "Kurdish",
        "name": "Wikiquote",
        "url": "https://ku.wikiquote.org"
    },
    "kvwiki": {
        "langCode": "kv",
        "langName": "коми",
        "langNameEN": "Komi",
        "name": "Wikipedia",
        "url": "https://kv.wikipedia.org"
    },
    "kwwiki": {
        "langCode": "kw",
        "langName": "kernowek",
        "langNameEN": "Cornish",
        "name": "Wikipedia",
        "url": "https://kw.wikipedia.org"
    },
    "kwwiktionary": {
        "langCode": "kw",
        "langName": "kernowek",
        "langNameEN": "Cornish",
        "name": "Wiktionary",
        "url": "https://kw.wiktionary.org"
    },
    "kywiki": {
        "langCode": "ky",
        "langName": "кыргызча",
        "langNameEN": "Kyrgyz",
        "name": "Wikipedia",
        "url": "https://ky.wikipedia.org"
    },
    "kywiktionary": {
        "langCode": "ky",
        "langName": "кыргызча",
        "langNameEN": "Kyrgyz",
        "name": "Wiktionary",
        "url": "https://ky.wiktionary.org"
    },
    "kywikibooks": {
        "langCode": "ky",
        "langName": "кыргызча",
        "langNameEN": "Kyrgyz",
        "name": "Wikibooks",
        "url": "https://ky.wikibooks.org"
    },
    "kywikiquote": {
        "langCode": "ky",
        "langName": "кыргызча",
        "langNameEN": "Kyrgyz",
        "name": "Wikiquote",
        "url": "https://ky.wikiquote.org"
    },
    "lawiki": {
        "langCode": "la",
        "langName": "Latina",
        "langNameEN": "Latin",
        "name": "Vicipaedia",
        "url": "https://la.wikipedia.org"
    },
    "lawiktionary": {
        "langCode": "la",
        "langName": "Latina",
        "langNameEN": "Latin",
        "name": "Victionarium",
        "url": "https://la.wiktionary.org"
    },
    "lawikibooks": {
        "langCode": "la",
        "langName": "Latina",
        "langNameEN": "Latin",
        "name": "Vicilibri",
        "url": "https://la.wikibooks.org"
    },
    "lawikiquote": {
        "langCode": "la",
        "langName": "Latina",
        "langNameEN": "Latin",
        "name": "Vicicitatio",
        "url": "https://la.wikiquote.org"
    },
    "lawikisource": {
        "langCode": "la",
        "langName": "Latina",
        "langNameEN": "Latin",
        "name": "Wikisource",
        "url": "https://la.wikisource.org"
    },
    "ladwiki": {
        "langCode": "lad",
        "langName": "Ladino",
        "langNameEN": "Ladino",
        "name": "Vikipedya",
        "url": "https://lad.wikipedia.org"
    },
    "lbwiki": {
        "langCode": "lb",
        "langName": "Lëtzebuergesch",
        "langNameEN": "Luxembourgish",
        "name": "Wikipedia",
        "url": "https://lb.wikipedia.org"
    },
    "lbwiktionary": {
        "langCode": "lb",
        "langName": "Lëtzebuergesch",
        "langNameEN": "Luxembourgish",
        "name": "Wiktionnaire",
        "url": "https://lb.wiktionary.org"
    },
    "lbewiki": {
        "langCode": "lbe",
        "langName": "лакку",
        "langNameEN": "Lak",
        "name": "Википедия",
        "url": "https://lbe.wikipedia.org"
    },
    "lezwiki": {
        "langCode": "lez",
        "langName": "лезги",
        "langNameEN": "Lezghian",
        "name": "Википедия",
        "url": "https://lez.wikipedia.org"
    },
    "lfnwiki": {
        "langCode": "lfn",
        "langName": "Lingua Franca Nova",
        "langNameEN": "Lingua Franca Nova",
        "name": "Vicipedia",
        "url": "https://lfn.wikipedia.org"
    },
    "lgwiki": {
        "langCode": "lg",
        "langName": "Luganda",
        "langNameEN": "Ganda",
        "name": "Wikipedia",
        "url": "https://lg.wikipedia.org"
    },
    "liwiki": {
        "langCode": "li",
        "langName": "Limburgs",
        "langNameEN": "Limburgish",
        "name": "Wikipedia",
        "url": "https://li.wikipedia.org"
    },
    "liwiktionary": {
        "langCode": "li",
        "langName": "Limburgs",
        "langNameEN": "Limburgish",
        "name": "Wiktionary",
        "url": "https://li.wiktionary.org"
    },
    "liwikibooks": {
        "langCode": "li",
        "langName": "Limburgs",
        "langNameEN": "Limburgish",
        "name": "Wikibeuk",
        "url": "https://li.wikibooks.org"
    },
    "liwikinews": {
        "langCode": "li",
        "langName": "Limburgs",
        "langNameEN": "Limburgish",
        "name": "Wikinuujs",
        "url": "https://li.wikinews.org"
    },
    "liwikiquote": {
        "langCode": "li",
        "langName": "Limburgs",
        "langNameEN": "Limburgish",
        "name": "Wikiquote",
        "url": "https://li.wikiquote.org"
    },
    "liwikisource": {
        "langCode": "li",
        "langName": "Limburgs",
        "langNameEN": "Limburgish",
        "name": "Wikibrónne",
        "url": "https://li.wikisource.org"
    },
    "lijwiki": {
        "langCode": "lij",
        "langName": "Ligure",
        "langNameEN": "Ligurian",
        "name": "Wikipedia",
        "url": "https://lij.wikipedia.org"
    },
    "lijwikisource": {
        "langCode": "lij",
        "langName": "Ligure",
        "langNameEN": "Ligurian",
        "name": "Wikivivàgna",
        "url": "https://lij.wikisource.org"
    },
    "lldwiki": {
        "langCode": "lld",
        "langName": "Ladin",
        "langNameEN": "Ladin",
        "name": "Wikipedia",
        "url": "https://lld.wikipedia.org"
    },
    "lmowiki": {
        "langCode": "lmo",
        "langName": "lombard",
        "langNameEN": "Lombard",
        "name": "Wikipedia",
        "url": "https://lmo.wikipedia.org"
    },
    "lmowiktionary": {
        "langCode": "lmo",
        "langName": "lombard",
        "langNameEN": "Lombard",
        "name": "Wiktionary",
        "url": "https://lmo.wiktionary.org"
    },
    "lnwiki": {
        "langCode": "ln",
        "langName": "lingála",
        "langNameEN": "Lingala",
        "name": "Wikipedia",
        "url": "https://ln.wikipedia.org"
    },
    "lnwiktionary": {
        "langCode": "ln",
        "langName": "lingála",
        "langNameEN": "Lingala",
        "name": "Wiktionary",
        "url": "https://ln.wiktionary.org"
    },
    "lowiki": {
        "langCode": "lo",
        "langName": "ລາວ",
        "langNameEN": "Lao",
        "name": "ວິກິພີເດຍ",
        "url": "https://lo.wikipedia.org"
    },
    "lowiktionary": {
        "langCode": "lo",
        "langName": "ລາວ",
        "langNameEN": "Lao",
        "name": "Wiktionary",
        "url": "https://lo.wiktionary.org"
    },
    "ltwiki": {
        "langCode": "lt",
        "langName": "lietuvių",
        "langNameEN": "Lithuanian",
        "name": "Vikipedija",
        "url": "https://lt.wikipedia.org"
    },
    "ltwiktionary": {
        "langCode": "lt",
        "langName": "lietuvių",
        "langNameEN": "Lithuanian",
        "name": "Vikižodynas",
        "url": "https://lt.wiktionary.org"
    },
    "ltwikibooks": {
        "langCode": "lt",
        "langName": "lietuvių",
        "langNameEN": "Lithuanian",
        "name": "Wikibooks",
        "url": "https://lt.wikibooks.org"
    },
    "ltwikiquote": {
        "langCode": "lt",
        "langName": "lietuvių",
        "langNameEN": "Lithuanian",
        "name": "Wikiquote",
        "url": "https://lt.wikiquote.org"
    },
    "ltwikisource": {
        "langCode": "lt",
        "langName": "lietuvių",
        "langNameEN": "Lithuanian",
        "name": "Vikišaltiniai",
        "url": "https://lt.wikisource.org"
    },
    "ltgwiki": {
        "langCode": "ltg",
        "langName": "latgaļu",
        "langNameEN": "Latgalian",
        "name": "Vikipedeja",
        "url": "https://ltg.wikipedia.org"
    },
    "lvwiki": {
        "langCode": "lv",
        "langName": "latviešu",
        "langNameEN": "Latvian",
        "name": "Vikipēdija",
        "url": "https://lv.wikipedia.org"
    },
    "lvwiktionary": {
        "langCode": "lv",
        "langName": "latviešu",
        "langNameEN": "Latvian",
        "name": "Wiktionary",
        "url": "https://lv.wiktionary.org"
    },
    "madwiki": {
        "langCode": "mad",
        "langName": "Madhurâ",
        "langNameEN": "Madurese",
        "name": "Wikipedia",
        "url": "https://mad.wikipedia.org"
    },
    "maiwiki": {
        "langCode": "mai",
        "langName": "मैथिली",
        "langNameEN": "Maithili",
        "name": "विकिपिडिया",
        "url": "https://mai.wikipedia.org"
    },
    "map_bmswiki": {
        "langCode": "map-bms",
        "langName": "Basa Banyumasan",
        "langNameEN": "Basa Banyumasan",
        "name": "Wikipedia",
        "url": "https://map-bms.wikipedia.org"
    },
    "mdfwiki": {
        "langCode": "mdf",
        "langName": "мокшень",
        "langNameEN": "Moksha",
        "name": "Википедиесь",
        "url": "https://mdf.wikipedia.org"
    },
    "mgwiki": {
        "langCode": "mg",
        "langName": "Malagasy",
        "langNameEN": "Malagasy",
        "name": "Wikipedia",
        "url": "https://mg.wikipedia.org"
    },
    "mgwiktionary": {
        "langCode": "mg",
        "langName": "Malagasy",
        "langNameEN": "Malagasy",
        "name": "Wiktionary",
        "url": "https://mg.wiktionary.org"
    },
    "mgwikibooks": {
        "langCode": "mg",
        "langName": "Malagasy",
        "langNameEN": "Malagasy",
        "name": "Wikibooks",
        "url": "https://mg.wikibooks.org"
    },
    "mhrwiki": {
        "langCode": "mhr",
        "langName": "олык марий",
        "langNameEN": "Eastern Mari",
        "name": "Википедий",
        "url": "https://mhr.wikipedia.org"
    },
    "miwiki": {
        "langCode": "mi",
        "langName": "Māori",
        "langNameEN": "Maori",
        "name": "Wikipedia",
        "url": "https://mi.wikipedia.org"
    },
    "miwiktionary": {
        "langCode": "mi",
        "langName": "Māori",
        "langNameEN": "Maori",
        "name": "Wiktionary",
        "url": "https://mi.wiktionary.org"
    },
    "minwiki": {
        "langCode": "min",
        "langName": "Minangkabau",
        "langNameEN": "Minangkabau",
        "name": "Wikipedia",
        "url": "https://min.wikipedia.org"
    },
    "minwiktionary": {
        "langCode": "min",
        "langName": "Minangkabau",
        "langNameEN": "Minangkabau",
        "name": "Wikikato",
        "url": "https://min.wiktionary.org"
    },
    "mkwiki": {
        "langCode": "mk",
        "langName": "македонски",
        "langNameEN": "Macedonian",
        "name": "Википедија",
        "url": "https://mk.wikipedia.org"
    },
    "mkwiktionary": {
        "langCode": "mk",
        "langName": "македонски",
        "langNameEN": "Macedonian",
        "name": "Викиречник",
        "url": "https://mk.wiktionary.org"
    },
    "mkwikibooks": {
        "langCode": "mk",
        "langName": "македонски",
        "langNameEN": "Macedonian",
        "name": "Wikibooks",
        "url": "https://mk.wikibooks.org"
    },
    "mkwikisource": {
        "langCode": "mk",
        "langName": "македонски",
        "langNameEN": "Macedonian",
        "name": "Wikisource",
        "url": "https://mk.wikisource.org"
    },
    "mlwiki": {
        "langCode": "ml",
        "langName": "മലയാളം",
        "langNameEN": "Malayalam",
        "name": "വിക്കിപീഡിയ",
        "url": "https://ml.wikipedia.org"
    },
    "mlwiktionary": {
        "langCode": "ml",
        "langName": "മലയാളം",
        "langNameEN": "Malayalam",
        "name": "വിക്കിനിഘണ്ടു",
        "url": "https://ml.wiktionary.org"
    },
    "mlwikibooks": {
        "langCode": "ml",
        "langName": "മലയാളം",
        "langNameEN": "Malayalam",
        "name": "വിക്കിപാഠശാല",
        "url": "https://ml.wikibooks.org"
    },
    "mlwikiquote": {
        "langCode": "ml",
        "langName": "മലയാളം",
        "langNameEN": "Malayalam",
        "name": "വിക്കിചൊല്ലുകൾ",
        "url": "https://ml.wikiquote.org"
    },
    "mlwikisource": {
        "langCode": "ml",
        "langName": "മലയാളം",
        "langNameEN": "Malayalam",
        "name": "വിക്കിഗ്രന്ഥശാല",
        "url": "https://ml.wikisource.org"
    },
    "mnwiki": {
        "langCode": "mn",
        "langName": "монгол",
        "langNameEN": "Mongolian",
        "name": "Википедиа",
        "url": "https://mn.wikipedia.org"
    },
    "mnwiktionary": {
        "langCode": "mn",
        "langName": "монгол",
        "langNameEN": "Mongolian",
        "name": "Wiktionary",
        "url": "https://mn.wiktionary.org"
    },
    "mniwiki": {
        "langCode": "mni",
        "langName": "ꯃꯤꯇꯩ ꯂꯣꯟ",
        "langNameEN": "Manipuri",
        "name": "ꯋꯤꯀꯤꯄꯦꯗꯤꯌꯥ",
        "url": "https://mni.wikipedia.org"
    },
    "mniwiktionary": {
        "langCode": "mni",
        "langName": "ꯃꯤꯇꯩ ꯂꯣꯟ",
        "langNameEN": "Manipuri",
        "name": "ꯋꯤꯛꯁꯟꯅꯔꯤ",
        "url": "https://mni.wiktionary.org"
    },
    "mnwwiki": {
        "langCode": "mnw",
        "langName": "ဘာသာ မန်",
        "langNameEN": "Mon",
        "name": "ဝဳကဳပဳဒဳယာ",
        "url": "https://mnw.wikipedia.org"
    },
    "mnwwiktionary": {
        "langCode": "mnw",
        "langName": "ဘာသာ မန်",
        "langNameEN": "Mon",
        "name": "ဝိက်ရှေန်နရဳ",
        "url": "https://mnw.wiktionary.org"
    },
    "mrwiki": {
        "langCode": "mr",
        "langName": "मराठी",
        "langNameEN": "Marathi",
        "name": "विकिपीडिया",
        "url": "https://mr.wikipedia.org"
    },
    "mrwiktionary": {
        "langCode": "mr",
        "langName": "मराठी",
        "langNameEN": "Marathi",
        "name": "Wiktionary",
        "url": "https://mr.wiktionary.org"
    },
    "mrwikibooks": {
        "langCode": "mr",
        "langName": "मराठी",
        "langNameEN": "Marathi",
        "name": "विकिबुक्स",
        "url": "https://mr.wikibooks.org"
    },
    "mrwikiquote": {
        "langCode": "mr",
        "langName": "मराठी",
        "langNameEN": "Marathi",
        "name": "Wikiquote",
        "url": "https://mr.wikiquote.org"
    },
    "mrwikisource": {
        "langCode": "mr",
        "langName": "मराठी",
        "langNameEN": "Marathi",
        "name": "विकिस्रोत",
        "url": "https://mr.wikisource.org"
    },
    "mrjwiki": {
        "langCode": "mrj",
        "langName": "кырык мары",
        "langNameEN": "Western Mari",
        "name": "Википеди",
        "url": "https://mrj.wikipedia.org"
    },
    "mswiki": {
        "langCode": "ms",
        "langName": "Bahasa Melayu",
        "langNameEN": "Malay",
        "name": "Wikipedia",
        "url": "https://ms.wikipedia.org"
    },
    "mswiktionary": {
        "langCode": "ms",
        "langName": "Bahasa Melayu",
        "langNameEN": "Malay",
        "name": "Wiktionary",
        "url": "https://ms.wiktionary.org"
    },
    "mswikibooks": {
        "langCode": "ms",
        "langName": "Bahasa Melayu",
        "langNameEN": "Malay",
        "name": "Wikibooks",
        "url": "https://ms.wikibooks.org"
    },
    "mtwiki": {
        "langCode": "mt",
        "langName": "Malti",
        "langNameEN": "Maltese",
        "name": "Wikipedija",
        "url": "https://mt.wikipedia.org"
    },
    "mtwiktionary": {
        "langCode": "mt",
        "langName": "Malti",
        "langNameEN": "Maltese",
        "name": "Wikizzjunarju",
        "url": "https://mt.wiktionary.org"
    },
    "mwlwiki": {
        "langCode": "mwl",
        "langName": "Mirandés",
        "langNameEN": "Mirandese",
        "name": "Biquipédia",
        "url": "https://mwl.wikipedia.org"
    },
    "mywiki": {
        "langCode": "my",
        "langName": "မြန်မာဘာသာ",
        "langNameEN": "Burmese",
        "name": "ဝီကီပီးဒီးယား",
        "url": "https://my.wikipedia.org"
    },
    "mywiktionary": {
        "langCode": "my",
        "langName": "မြန်မာဘာသာ",
        "langNameEN": "Burmese",
        "name": "ဝစ်ရှင်နရီ",
        "url": "https://my.wiktionary.org"
    },
    "myvwiki": {
        "langCode": "myv",
        "langName": "эрзянь",
        "langNameEN": "Erzya",
        "name": "Википедиясь",
        "url": "https://myv.wikipedia.org"
    },
    "mznwiki": {
        "langCode": "mzn",
        "langName": "مازِرونی",
        "langNameEN": "Mazanderani",
        "name": "ویکی‌پدیا",
        "url": "https://mzn.wikipedia.org"
    },
    "nawiki": {
        "langCode": "na",
        "langName": "Dorerin Naoero",
        "langNameEN": "Nauru",
        "name": "Wikipedia",
        "url": "https://na.wikipedia.org"
    },
    "nawiktionary": {
        "langCode": "na",
        "langName": "Dorerin Naoero",
        "langNameEN": "Nauru",
        "name": "Wiktionary",
        "url": "https://na.wiktionary.org"
    },
    "nahwiki": {
        "langCode": "nah",
        "langName": "Nāhuatl",
        "langNameEN": "Nāhuatl",
        "name": "Huiquipedia",
        "url": "https://nah.wikipedia.org"
    },
    "nahwiktionary": {
        "langCode": "nah",
        "langName": "Nāhuatl",
        "langNameEN": "Nāhuatl",
        "name": "Wiktionary",
        "url": "https://nah.wiktionary.org"
    },
    "napwiki": {
        "langCode": "nap",
        "langName": "Napulitano",
        "langNameEN": "Neapolitan",
        "name": "Wikipedia",
        "url": "https://nap.wikipedia.org"
    },
    "napwikisource": {
        "langCode": "nap",
        "langName": "Napulitano",
        "langNameEN": "Neapolitan",
        "name": "Wikisource",
        "url": "https://nap.wikisource.org"
    },
    "ndswiki": {
        "langCode": "nds",
        "langName": "Plattdüütsch",
        "langNameEN": "Low German",
        "name": "Wikipedia",
        "url": "https://nds.wikipedia.org"
    },
    "ndswiktionary": {
        "langCode": "nds",
        "langName": "Plattdüütsch",
        "langNameEN": "Low German",
        "name": "Wiktionary",
        "url": "https://nds.wiktionary.org"
    },
    "nds_nlwiki": {
        "langCode": "nds-nl",
        "langName": "Nedersaksies",
        "langNameEN": "Low Saxon",
        "name": "Wikipedia",
        "url": "https://nds-nl.wikipedia.org"
    },
    "newiki": {
        "langCode": "ne",
        "langName": "नेपाली",
        "langNameEN": "Nepali",
        "name": "विकिपिडिया",
        "url": "https://ne.wikipedia.org"
    },
    "newiktionary": {
        "langCode": "ne",
        "langName": "नेपाली",
        "langNameEN": "Nepali",
        "name": "Wiktionary",
        "url": "https://ne.wiktionary.org"
    },
    "newikibooks": {
        "langCode": "ne",
        "langName": "नेपाली",
        "langNameEN": "Nepali",
        "name": "विकिपुस्तक",
        "url": "https://ne.wikibooks.org"
    },
    "newwiki": {
        "langCode": "new",
        "langName": "नेपाल भाषा",
        "langNameEN": "Newari",
        "name": "Wikipedia",
        "url": "https://new.wikipedia.org"
    },
    "niawiki": {
        "langCode": "nia",
        "langName": "Li Niha",
        "langNameEN": "Nias",
        "name": "Wikipedia",
        "url": "https://nia.wikipedia.org"
    },
    "niawiktionary": {
        "langCode": "nia",
        "langName": "Li Niha",
        "langNameEN": "Nias",
        "name": "Wiktionary",
        "url": "https://nia.wiktionary.org"
    },
    "nlwiki": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "Wikipedia",
        "url": "https://nl.wikipedia.org"
    },
    "nlwiktionary": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "WikiWoordenboek",
        "url": "https://nl.wiktionary.org"
    },
    "nlwikibooks": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "Wikibooks",
        "url": "https://nl.wikibooks.org"
    },
    "nlwikinews": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "Wikinieuws",
        "url": "https://nl.wikinews.org"
    },
    "nlwikiquote": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "Wikiquote",
        "url": "https://nl.wikiquote.org"
    },
    "nlwikisource": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "Wikisource",
        "url": "https://nl.wikisource.org"
    },
    "nlwikivoyage": {
        "langCode": "nl",
        "langName": "Nederlands",
        "langNameEN": "Dutch",
        "name": "Wikivoyage",
        "url": "https://nl.wikivoyage.org"
    },
    "nnwiki": {
        "langCode": "nn",
        "langName": "norsk nynorsk",
        "langNameEN": "Norwegian Nynorsk",
        "name": "Wikipedia",
        "url": "https://nn.wikipedia.org"
    },
    "nnwiktionary": {
        "langCode": "nn",
        "langName": "norsk nynorsk",
        "langNameEN": "Norwegian Nynorsk",
        "name": "Wiktionary",
        "url": "https://nn.wiktionary.org"
    },
    "nnwikiquote": {
        "langCode": "nn",
        "langName": "norsk nynorsk",
        "langNameEN": "Norwegian Nynorsk",
        "name": "Wikiquote",
        "url": "https://nn.wikiquote.org"
    },
    "nowiki": {
        "langCode": "no",
        "langName": "norsk",
        "langNameEN": "Norwegian",
        "name": "Wikipedia",
        "url": "https://no.wikipedia.org"
    },
    "nowiktionary": {
        "langCode": "no",
        "langName": "norsk",
        "langNameEN": "Norwegian",
        "name": "Wiktionary",
        "url": "https://no.wiktionary.org"
    },
    "nowikibooks": {
        "langCode": "no",
        "langName": "norsk",
        "langNameEN": "Norwegian",
        "name": "Wikibøker",
        "url": "https://no.wikibooks.org"
    },
    "nowikinews": {
        "langCode": "no",
        "langName": "norsk",
        "langNameEN": "Norwegian",
        "name": "Wikinytt",
        "url": "https://no.wikinews.org"
    },
    "nowikiquote": {
        "langCode": "no",
        "langName": "norsk",
        "langNameEN": "Norwegian",
        "name": "Wikiquote",
        "url": "https://no.wikiquote.org"
    },
    "nowikisource": {
        "langCode": "no",
        "langName": "norsk",
        "langNameEN": "Norwegian",
        "name": "Wikikilden",
        "url": "https://no.wikisource.org"
    },
    "novwiki": {
        "langCode": "nov",
        "langName": "Novial",
        "langNameEN": "Novial",
        "name": "Wikipedia",
        "url": "https://nov.wikipedia.org"
    },
    "nqowiki": {
        "langCode": "nqo",
        "langName": "ߒߞߏ",
        "langNameEN": "N’Ko",
        "name": "ߥߞߌߔߘߋߞߎ",
        "url": "https://nqo.wikipedia.org"
    },
    "nrmwiki": {
        "langCode": "nrm",
        "langName": "Nouormand",
        "langNameEN": "Norman",
        "name": "Wikipedia",
        "url": "https://nrm.wikipedia.org"
    },
    "nsowiki": {
        "langCode": "nso",
        "langName": "Sesotho sa Leboa",
        "langNameEN": "Northern Sotho",
        "name": "Wikipedia",
        "url": "https://nso.wikipedia.org"
    },
    "nvwiki": {
        "langCode": "nv",
        "langName": "Diné bizaad",
        "langNameEN": "Navajo",
        "name": "Wikipedia",
        "url": "https://nv.wikipedia.org"
    },
    "nywiki": {
        "langCode": "ny",
        "langName": "Chi-Chewa",
        "langNameEN": "Nyanja",
        "name": "Wikipedia",
        "url": "https://ny.wikipedia.org"
    },
    "ocwiki": {
        "langCode": "oc",
        "langName": "occitan",
        "langNameEN": "Occitan",
        "name": "Wikipèdia",
        "url": "https://oc.wikipedia.org"
    },
    "ocwiktionary": {
        "langCode": "oc",
        "langName": "occitan",
        "langNameEN": "Occitan",
        "name": "Wikiccionari",
        "url": "https://oc.wiktionary.org"
    },
    "ocwikibooks": {
        "langCode": "oc",
        "langName": "occitan",
        "langNameEN": "Occitan",
        "name": "Wikilibres",
        "url": "https://oc.wikibooks.org"
    },
    "olowiki": {
        "langCode": "olo",
        "langName": "livvinkarjala",
        "langNameEN": "Livvi-Karelian",
        "name": "Wikipedii",
        "url": "https://olo.wikipedia.org"
    },
    "omwiki": {
        "langCode": "om",
        "langName": "Oromoo",
        "langNameEN": "Oromo",
        "name": "Wikipedia",
        "url": "https://om.wikipedia.org"
    },
    "omwiktionary": {
        "langCode": "om",
        "langName": "Oromoo",
        "langNameEN": "Oromo",
        "name": "Wiktionary",
        "url": "https://om.wiktionary.org"
    },
    "orwiki": {
        "langCode": "or",
        "langName": "ଓଡ଼ିଆ",
        "langNameEN": "Odia",
        "name": "ଉଇକିପିଡ଼ିଆ",
        "url": "https://or.wikipedia.org"
    },
    "orwiktionary": {
        "langCode": "or",
        "langName": "ଓଡ଼ିଆ",
        "langNameEN": "Odia",
        "name": "ଉଇକିଅଭିଧାନ",
        "url": "https://or.wiktionary.org"
    },
    "orwikisource": {
        "langCode": "or",
        "langName": "ଓଡ଼ିଆ",
        "langNameEN": "Odia",
        "name": "ଉଇକିପାଠାଗାର",
        "url": "https://or.wikisource.org"
    },
    "oswiki": {
        "langCode": "os",
        "langName": "ирон",
        "langNameEN": "Ossetic",
        "name": "Википеди",
        "url": "https://os.wikipedia.org"
    },
    "pawiki": {
        "langCode": "pa",
        "langName": "ਪੰਜਾਬੀ",
        "langNameEN": "Punjabi",
        "name": "ਵਿਕੀਪੀਡੀਆ",
        "url": "https://pa.wikipedia.org"
    },
    "pawiktionary": {
        "langCode": "pa",
        "langName": "ਪੰਜਾਬੀ",
        "langNameEN": "Punjabi",
        "name": "Wiktionary",
        "url": "https://pa.wiktionary.org"
    },
    "pawikibooks": {
        "langCode": "pa",
        "langName": "ਪੰਜਾਬੀ",
        "langNameEN": "Punjabi",
        "name": "Wikibooks",
        "url": "https://pa.wikibooks.org"
    },
    "pawikisource": {
        "langCode": "pa",
        "langName": "ਪੰਜਾਬੀ",
        "langNameEN": "Punjabi",
        "name": "ਵਿਕੀਸਰੋਤ",
        "url": "https://pa.wikisource.org"
    },
    "pagwiki": {
        "langCode": "pag",
        "langName": "Pangasinan",
        "langNameEN": "Pangasinan",
        "name": "Wikipedia",
        "url": "https://pag.wikipedia.org"
    },
    "pamwiki": {
        "langCode": "pam",
        "langName": "Kapampangan",
        "langNameEN": "Pampanga",
        "name": "Wikipedia",
        "url": "https://pam.wikipedia.org"
    },
    "papwiki": {
        "langCode": "pap",
        "langName": "Papiamentu",
        "langNameEN": "Papiamento",
        "name": "Wikipedia",
        "url": "https://pap.wikipedia.org"
    },
    "pcdwiki": {
        "langCode": "pcd",
        "langName": "Picard",
        "langNameEN": "Picard",
        "name": "Wikipedia",
        "url": "https://pcd.wikipedia.org"
    },
    "pdcwiki": {
        "langCode": "pdc",
        "langName": "Deitsch",
        "langNameEN": "Pennsylvania German",
        "name": "Wikipedia",
        "url": "https://pdc.wikipedia.org"
    },
    "pflwiki": {
        "langCode": "pfl",
        "langName": "Pälzisch",
        "langNameEN": "Palatine German",
        "name": "Wikipedia",
        "url": "https://pfl.wikipedia.org"
    },
    "piwiki": {
        "langCode": "pi",
        "langName": "पालि",
        "langNameEN": "Pali",
        "name": "Wikipedia",
        "url": "https://pi.wikipedia.org"
    },
    "pihwiki": {
        "langCode": "pih",
        "langName": "Norfuk / Pitkern",
        "langNameEN": "Norfuk / Pitkern",
        "name": "Wikipedia",
        "url": "https://pih.wikipedia.org"
    },
    "plwiki": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikipedia",
        "url": "https://pl.wikipedia.org"
    },
    "plwiktionary": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikisłownik",
        "url": "https://pl.wiktionary.org"
    },
    "plwikibooks": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikibooks",
        "url": "https://pl.wikibooks.org"
    },
    "plwikinews": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikinews",
        "url": "https://pl.wikinews.org"
    },
    "plwikiquote": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikicytaty",
        "url": "https://pl.wikiquote.org"
    },
    "plwikisource": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikiźródła",
        "url": "https://pl.wikisource.org"
    },
    "plwikivoyage": {
        "langCode": "pl",
        "langName": "polski",
        "langNameEN": "Polish",
        "name": "Wikipodróże",
        "url": "https://pl.wikivoyage.org"
    },
    "pmswiki": {
        "langCode": "pms",
        "langName": "Piemontèis",
        "langNameEN": "Piedmontese",
        "name": "Wikipedia",
        "url": "https://pms.wikipedia.org"
    },
    "pmswikisource": {
        "langCode": "pms",
        "langName": "Piemontèis",
        "langNameEN": "Piedmontese",
        "name": "Wikisource",
        "url": "https://pms.wikisource.org"
    },
    "pnbwiki": {
        "langCode": "pnb",
        "langName": "پنجابی",
        "langNameEN": "Western Punjabi",
        "name": "وکیپیڈیا",
        "url": "https://pnb.wikipedia.org"
    },
    "pnbwiktionary": {
        "langCode": "pnb",
        "langName": "پنجابی",
        "langNameEN": "Western Punjabi",
        "name": "وکشنری",
        "url": "https://pnb.wiktionary.org"
    },
    "pntwiki": {
        "langCode": "pnt",
        "langName": "Ποντιακά",
        "langNameEN": "Pontic",
        "name": "Βικιπαίδεια",
        "url": "https://pnt.wikipedia.org"
    },
    "pswiki": {
        "langCode": "ps",
        "langName": "پښتو",
        "langNameEN": "Pashto",
        "name": "ويکيپېډيا",
        "url": "https://ps.wikipedia.org"
    },
    "pswiktionary": {
        "langCode": "ps",
        "langName": "پښتو",
        "langNameEN": "Pashto",
        "name": "ويکيسيند",
        "url": "https://ps.wiktionary.org"
    },
    "pswikivoyage": {
        "langCode": "ps",
        "langName": "پښتو",
        "langNameEN": "Pashto",
        "name": "ويکيسفر",
        "url": "https://ps.wikivoyage.org"
    },
    "ptwiki": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikipédia",
        "url": "https://pt.wikipedia.org"
    },
    "ptwiktionary": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikcionário",
        "url": "https://pt.wiktionary.org"
    },
    "ptwikibooks": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikilivros",
        "url": "https://pt.wikibooks.org"
    },
    "ptwikinews": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikinotícias",
        "url": "https://pt.wikinews.org"
    },
    "ptwikiquote": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikiquote",
        "url": "https://pt.wikiquote.org"
    },
    "ptwikisource": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikisource",
        "url": "https://pt.wikisource.org"
    },
    "ptwikiversity": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikiversidade",
        "url": "https://pt.wikiversity.org"
    },
    "ptwikivoyage": {
        "langCode": "pt",
        "langName": "português",
        "langNameEN": "Portuguese",
        "name": "Wikivoyage",
        "url": "https://pt.wikivoyage.org"
    },
    "pwnwiki": {
        "langCode": "pwn",
        "langName": "pinayuanan",
        "langNameEN": "Paiwan",
        "name": "Wikipedia",
        "url": "https://pwn.wikipedia.org"
    },
    "quwiki": {
        "langCode": "qu",
        "langName": "Runa Simi",
        "langNameEN": "Quechua",
        "name": "Wikipedia",
        "url": "https://qu.wikipedia.org"
    },
    "quwiktionary": {
        "langCode": "qu",
        "langName": "Runa Simi",
        "langNameEN": "Quechua",
        "name": "Wiktionary",
        "url": "https://qu.wiktionary.org"
    },
    "rmwiki": {
        "langCode": "rm",
        "langName": "rumantsch",
        "langNameEN": "Romansh",
        "name": "Wikipedia",
        "url": "https://rm.wikipedia.org"
    },
    "rmywiki": {
        "langCode": "rmy",
        "langName": "romani čhib",
        "langNameEN": "Vlax Romani",
        "name": "Vikipidiya",
        "url": "https://rmy.wikipedia.org"
    },
    "rnwiki": {
        "langCode": "rn",
        "langName": "Kirundi",
        "langNameEN": "Rundi",
        "name": "Wikipedia",
        "url": "https://rn.wikipedia.org"
    },
    "rowiki": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikipedia",
        "url": "https://ro.wikipedia.org"
    },
    "rowiktionary": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikționar",
        "url": "https://ro.wiktionary.org"
    },
    "rowikibooks": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikimanuale",
        "url": "https://ro.wikibooks.org"
    },
    "rowikinews": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikiștiri",
        "url": "https://ro.wikinews.org"
    },
    "rowikiquote": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikicitat",
        "url": "https://ro.wikiquote.org"
    },
    "rowikisource": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikisource",
        "url": "https://ro.wikisource.org"
    },
    "rowikivoyage": {
        "langCode": "ro",
        "langName": "română",
        "langNameEN": "Romanian",
        "name": "Wikivoyage",
        "url": "https://ro.wikivoyage.org"
    },
    "roa_rupwiki": {
        "langCode": "roa-rup",
        "langName": "armãneashti",
        "langNameEN": "Aromanian",
        "name": "Wikipedia",
        "url": "https://roa-rup.wikipedia.org"
    },
    "roa_rupwiktionary": {
        "langCode": "roa-rup",
        "langName": "armãneashti",
        "langNameEN": "Aromanian",
        "name": "Wiktionary",
        "url": "https://roa-rup.wiktionary.org"
    },
    "roa_tarawiki": {
        "langCode": "roa-tara",
        "langName": "tarandíne",
        "langNameEN": "Tarantino",
        "name": "Wikipedia",
        "url": "https://roa-tara.wikipedia.org"
    },
    "ruwiki": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Википедия",
        "url": "https://ru.wikipedia.org"
    },
    "ruwiktionary": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Викисловарь",
        "url": "https://ru.wiktionary.org"
    },
    "ruwikibooks": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Викиучебник",
        "url": "https://ru.wikibooks.org"
    },
    "ruwikinews": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Викиновости",
        "url": "https://ru.wikinews.org"
    },
    "ruwikiquote": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Викицитатник",
        "url": "https://ru.wikiquote.org"
    },
    "ruwikisource": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Викитека",
        "url": "https://ru.wikisource.org"
    },
    "ruwikiversity": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Викиверситет",
        "url": "https://ru.wikiversity.org"
    },
    "ruwikivoyage": {
        "langCode": "ru",
        "langName": "русский",
        "langNameEN": "Russian",
        "name": "Wikivoyage",
        "url": "https://ru.wikivoyage.org"
    },
    "ruewiki": {
        "langCode": "rue",
        "langName": "русиньскый",
        "langNameEN": "Rusyn",
        "name": "Вікіпедія",
        "url": "https://rue.wikipedia.org"
    },
    "rwwiki": {
        "langCode": "rw",
        "langName": "Ikinyarwanda",
        "langNameEN": "Kinyarwanda",
        "name": "Wikipedia",
        "url": "https://rw.wikipedia.org"
    },
    "rwwiktionary": {
        "langCode": "rw",
        "langName": "Ikinyarwanda",
        "langNameEN": "Kinyarwanda",
        "name": "Wiktionary",
        "url": "https://rw.wiktionary.org"
    },
    "sawiki": {
        "langCode": "sa",
        "langName": "संस्कृतम्",
        "langNameEN": "Sanskrit",
        "name": "विकिपीडिया",
        "url": "https://sa.wikipedia.org"
    },
    "sawiktionary": {
        "langCode": "sa",
        "langName": "संस्कृतम्",
        "langNameEN": "Sanskrit",
        "name": "विकिशब्दकोशः",
        "url": "https://sa.wiktionary.org"
    },
    "sawikibooks": {
        "langCode": "sa",
        "langName": "संस्कृतम्",
        "langNameEN": "Sanskrit",
        "name": "विकिपुस्तकानि",
        "url": "https://sa.wikibooks.org"
    },
    "sawikiquote": {
        "langCode": "sa",
        "langName": "संस्कृतम्",
        "langNameEN": "Sanskrit",
        "name": "विकिसूक्तिः",
        "url": "https://sa.wikiquote.org"
    },
    "sawikisource": {
        "langCode": "sa",
        "langName": "संस्कृतम्",
        "langNameEN": "Sanskrit",
        "name": "विकिस्रोतः",
        "url": "https://sa.wikisource.org"
    },
    "sahwiki": {
        "langCode": "sah",
        "langName": "саха тыла",
        "langNameEN": "Sakha",
        "name": "Бикипиэдьийэ",
        "url": "https://sah.wikipedia.org"
    },
    "sahwikiquote": {
        "langCode": "sah",
        "langName": "саха тыла",
        "langNameEN": "Sakha",
        "name": "Биики_Домох",
        "url": "https://sah.wikiquote.org"
    },
    "sahwikisource": {
        "langCode": "sah",
        "langName": "саха тыла",
        "langNameEN": "Sakha",
        "name": "Бикитиэкэ",
        "url": "https://sah.wikisource.org"
    },
    "satwiki": {
        "langCode": "sat",
        "langName": "ᱥᱟᱱᱛᱟᱲᱤ",
        "langNameEN": "Santali",
        "name": "ᱣᱤᱠᱤᱯᱤᱰᱤᱭᱟ",
        "url": "https://sat.wikipedia.org"
    },
    "scwiki": {
        "langCode": "sc",
        "langName": "sardu",
        "langNameEN": "Sardinian",
        "name": "Wikipedia",
        "url": "https://sc.wikipedia.org"
    },
    "scnwiki": {
        "langCode": "scn",
        "langName": "sicilianu",
        "langNameEN": "Sicilian",
        "name": "Wikipedia",
        "url": "https://scn.wikipedia.org"
    },
    "scnwiktionary": {
        "langCode": "scn",
        "langName": "sicilianu",
        "langNameEN": "Sicilian",
        "name": "Wikizziunariu",
        "url": "https://scn.wiktionary.org"
    },
    "scowiki": {
        "langCode": "sco",
        "langName": "Scots",
        "langNameEN": "Scots",
        "name": "Wikipedia",
        "url": "https://sco.wikipedia.org"
    },
    "sdwiki": {
        "langCode": "sd",
        "langName": "سنڌي",
        "langNameEN": "Sindhi",
        "name": "وڪيپيڊيا",
        "url": "https://sd.wikipedia.org"
    },
    "sdwiktionary": {
        "langCode": "sd",
        "langName": "سنڌي",
        "langNameEN": "Sindhi",
        "name": "Wiktionary",
        "url": "https://sd.wiktionary.org"
    },
    "sewiki": {
        "langCode": "se",
        "langName": "davvisámegiella",
        "langNameEN": "Northern Sami",
        "name": "Wikipedia",
        "url": "https://se.wikipedia.org"
    },
    "sgwiki": {
        "langCode": "sg",
        "langName": "Sängö",
        "langNameEN": "Sango",
        "name": "Wikipedia",
        "url": "https://sg.wikipedia.org"
    },
    "sgwiktionary": {
        "langCode": "sg",
        "langName": "Sängö",
        "langNameEN": "Sango",
        "name": "Wiktionary",
        "url": "https://sg.wiktionary.org"
    },
    "shwiki": {
        "langCode": "sh",
        "langName": "srpskohrvatski / српскохрватски",
        "langNameEN": "Serbo-Croatian",
        "name": "Wikipedia",
        "url": "https://sh.wikipedia.org"
    },
    "shwiktionary": {
        "langCode": "sh",
        "langName": "srpskohrvatski / српскохрватски",
        "langNameEN": "Serbo-Croatian",
        "name": "Wiktionary",
        "url": "https://sh.wiktionary.org"
    },
    "shiwiki": {
        "langCode": "shi",
        "langName": "Taclḥit",
        "langNameEN": "Tachelhit",
        "name": "Wikipedia",
        "url": "https://shi.wikipedia.org"
    },
    "shnwiki": {
        "langCode": "shn",
        "langName": "ၽႃႇသႃႇတႆး ",
        "langNameEN": "Shan",
        "name": "ဝီႇၶီႇၽီးတီးယႃး",
        "url": "https://shn.wikipedia.org"
    },
    "shnwiktionary": {
        "langCode": "shn",
        "langName": "ၽႃႇသႃႇတႆး ",
        "langNameEN": "Shan",
        "name": "ဝိၵ်ႇသျိၼ်ႇၼရီႇ",
        "url": "https://shn.wiktionary.org"
    },
    "shywiktionary": {
        "langCode": "shy",
        "langName": "tacawit",
        "langNameEN": "Shawiya",
        "name": "Wikasegzawal",
        "url": "https://shy.wiktionary.org"
    },
    "siwiki": {
        "langCode": "si",
        "langName": "සිංහල",
        "langNameEN": "Sinhala",
        "name": "විකිපීඩියා",
        "url": "https://si.wikipedia.org"
    },
    "siwiktionary": {
        "langCode": "si",
        "langName": "සිංහල",
        "langNameEN": "Sinhala",
        "name": "Wiktionary",
        "url": "https://si.wiktionary.org"
    },
    "siwikibooks": {
        "langCode": "si",
        "langName": "සිංහල",
        "langNameEN": "Sinhala",
        "name": "Wikibooks",
        "url": "https://si.wikibooks.org"
    },
    "simplewiki": {
        "langCode": "simple",
        "langName": "Simple English",
        "langNameEN": "Simple English",
        "name": "Wikipedia",
        "url": "https://simple.wikipedia.org"
    },
    "simplewiktionary": {
        "langCode": "simple",
        "langName": "Simple English",
        "langNameEN": "Simple English",
        "name": "Wiktionary",
        "url": "https://simple.wiktionary.org"
    },
    "skwiki": {
        "langCode": "sk",
        "langName": "slovenčina",
        "langNameEN": "Slovak",
        "name": "Wikipédia",
        "url": "https://sk.wikipedia.org"
    },
    "skwiktionary": {
        "langCode": "sk",
        "langName": "slovenčina",
        "langNameEN": "Slovak",
        "name": "Wikislovník",
        "url": "https://sk.wiktionary.org"
    },
    "skwikibooks": {
        "langCode": "sk",
        "langName": "slovenčina",
        "langNameEN": "Slovak",
        "name": "Wikiknihy",
        "url": "https://sk.wikibooks.org"
    },
    "skwikiquote": {
        "langCode": "sk",
        "langName": "slovenčina",
        "langNameEN": "Slovak",
        "name": "Wikicitáty",
        "url": "https://sk.wikiquote.org"
    },
    "skwikisource": {
        "langCode": "sk",
        "langName": "slovenčina",
        "langNameEN": "Slovak",
        "name": "Wikizdroje",
        "url": "https://sk.wikisource.org"
    },
    "skrwiki": {
        "langCode": "skr",
        "langName": "سرائیکی",
        "langNameEN": "Saraiki",
        "name": "وکیپیڈیا",
        "url": "https://skr.wikipedia.org"
    },
    "skrwiktionary": {
        "langCode": "skr",
        "langName": "سرائیکی",
        "langNameEN": "Saraiki",
        "name": "وکشنری",
        "url": "https://skr.wiktionary.org"
    },
    "slwiki": {
        "langCode": "sl",
        "langName": "slovenščina",
        "langNameEN": "Slovenian",
        "name": "Wikipedija",
        "url": "https://sl.wikipedia.org"
    },
    "slwiktionary": {
        "langCode": "sl",
        "langName": "slovenščina",
        "langNameEN": "Slovenian",
        "name": "Wikislovar",
        "url": "https://sl.wiktionary.org"
    },
    "slwikibooks": {
        "langCode": "sl",
        "langName": "slovenščina",
        "langNameEN": "Slovenian",
        "name": "Wikiknjige",
        "url": "https://sl.wikibooks.org"
    },
    "slwikiquote": {
        "langCode": "sl",
        "langName": "slovenščina",
        "langNameEN": "Slovenian",
        "name": "Wikinavedek",
        "url": "https://sl.wikiquote.org"
    },
    "slwikisource": {
        "langCode": "sl",
        "langName": "slovenščina",
        "langNameEN": "Slovenian",
        "name": "Wikivir",
        "url": "https://sl.wikisource.org"
    },
    "slwikiversity": {
        "langCode": "sl",
        "langName": "slovenščina",
        "langNameEN": "Slovenian",
        "name": "Wikiverza",
        "url": "https://sl.wikiversity.org"
    },
    "smwiki": {
        "langCode": "sm",
        "langName": "Gagana Samoa",
        "langNameEN": "Samoan",
        "name": "Wikipedia",
        "url": "https://sm.wikipedia.org"
    },
    "smwiktionary": {
        "langCode": "sm",
        "langName": "Gagana Samoa",
        "langNameEN": "Samoan",
        "name": "Wiktionary",
        "url": "https://sm.wiktionary.org"
    },
    "smnwiki": {
        "langCode": "smn",
        "langName": "anarâškielâ",
        "langNameEN": "Inari Sami",
        "name": "Wikipedia",
        "url": "https://smn.wikipedia.org"
    },
    "snwiki": {
        "langCode": "sn",
        "langName": "chiShona",
        "langNameEN": "Shona",
        "name": "Wikipedia",
        "url": "https://sn.wikipedia.org"
    },
    "sowiki": {
        "langCode": "so",
        "langName": "Soomaaliga",
        "langNameEN": "Somali",
        "name": "Wikipedia",
        "url": "https://so.wikipedia.org"
    },
    "sowiktionary": {
        "langCode": "so",
        "langName": "Soomaaliga",
        "langNameEN": "Somali",
        "name": "Wiktionary",
        "url": "https://so.wiktionary.org"
    },
    "sqwiki": {
        "langCode": "sq",
        "langName": "shqip",
        "langNameEN": "Albanian",
        "name": "Wikipedia",
        "url": "https://sq.wikipedia.org"
    },
    "sqwiktionary": {
        "langCode": "sq",
        "langName": "shqip",
        "langNameEN": "Albanian",
        "name": "Wiktionary",
        "url": "https://sq.wiktionary.org"
    },
    "sqwikibooks": {
        "langCode": "sq",
        "langName": "shqip",
        "langNameEN": "Albanian",
        "name": "Wikibooks",
        "url": "https://sq.wikibooks.org"
    },
    "sqwikinews": {
        "langCode": "sq",
        "langName": "shqip",
        "langNameEN": "Albanian",
        "name": "Wikilajme",
        "url": "https://sq.wikinews.org"
    },
    "sqwikiquote": {
        "langCode": "sq",
        "langName": "shqip",
        "langNameEN": "Albanian",
        "name": "Wikiquote",
        "url": "https://sq.wikiquote.org"
    },
    "srwiki": {
        "langCode": "sr",
        "langName": "српски / srpski",
        "langNameEN": "Serbian",
        "name": "Википедија",
        "url": "https://sr.wikipedia.org"
    },
    "srwiktionary": {
        "langCode": "sr",
        "langName": "српски / srpski",
        "langNameEN": "Serbian",
        "name": "Викиречник",
        "url": "https://sr.wiktionary.org"
    },
    "srwikibooks": {
        "langCode": "sr",
        "langName": "српски / srpski",
        "langNameEN": "Serbian",
        "name": "Викикњиге",
        "url": "https://sr.wikibooks.org"
    },
    "srwikinews": {
        "langCode": "sr",
        "langName": "српски / srpski",
        "langNameEN": "Serbian",
        "name": "Викиновости",
        "url": "https://sr.wikinews.org"
    },
    "srwikiquote": {
        "langCode": "sr",
        "langName": "српски / srpski",
        "langNameEN": "Serbian",
        "name": "Викицитат",
        "url": "https://sr.wikiquote.org"
    },
    "srwikisource": {
        "langCode": "sr",
        "langName": "српски / srpski",
        "langNameEN": "Serbian",
        "name": "Викизворник",
        "url": "https://sr.wikisource.org"
    },
    "srnwiki": {
        "langCode": "srn",
        "langName": "Sranantongo",
        "langNameEN": "Sranan Tongo",
        "name": "Wikipedia",
        "url": "https://srn.wikipedia.org"
    },
    "sswiki": {
        "langCode": "ss",
        "langName": "SiSwati",
        "langNameEN": "Swati",
        "name": "Wikipedia",
        "url": "https://ss.wikipedia.org"
    },
    "sswiktionary": {
        "langCode": "ss",
        "langName": "SiSwati",
        "langNameEN": "Swati",
        "name": "Wiktionary",
        "url": "https://ss.wiktionary.org"
    },
    "stwiki": {
        "langCode": "st",
        "langName": "Sesotho",
        "langNameEN": "Southern Sotho",
        "name": "Wikipedia",
        "url": "https://st.wikipedia.org"
    },
    "stwiktionary": {
        "langCode": "st",
        "langName": "Sesotho",
        "langNameEN": "Southern Sotho",
        "name": "Wiktionary",
        "url": "https://st.wiktionary.org"
    },
    "stqwiki": {
        "langCode": "stq",
        "langName": "Seeltersk",
        "langNameEN": "Saterland Frisian",
        "name": "Wikipedia",
        "url": "https://stq.wikipedia.org"
    },
    "suwiki": {
        "langCode": "su",
        "langName": "Sunda",
        "langNameEN": "Sundanese",
        "name": "Wikipedia",
        "url": "https://su.wikipedia.org"
    },
    "suwiktionary": {
        "langCode": "su",
        "langName": "Sunda",
        "langNameEN": "Sundanese",
        "name": "Wiktionary",
        "url": "https://su.wiktionary.org"
    },
    "suwikiquote": {
        "langCode": "su",
        "langName": "Sunda",
        "langNameEN": "Sundanese",
        "name": "Wikiquote",
        "url": "https://su.wikiquote.org"
    },
    "svwiki": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikipedia",
        "url": "https://sv.wikipedia.org"
    },
    "svwiktionary": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wiktionary",
        "url": "https://sv.wiktionary.org"
    },
    "svwikibooks": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikibooks",
        "url": "https://sv.wikibooks.org"
    },
    "svwikinews": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikinews",
        "url": "https://sv.wikinews.org"
    },
    "svwikiquote": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikiquote",
        "url": "https://sv.wikiquote.org"
    },
    "svwikisource": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikisource",
        "url": "https://sv.wikisource.org"
    },
    "svwikiversity": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikiversity",
        "url": "https://sv.wikiversity.org"
    },
    "svwikivoyage": {
        "langCode": "sv",
        "langName": "svenska",
        "langNameEN": "Swedish",
        "name": "Wikivoyage",
        "url": "https://sv.wikivoyage.org"
    },
    "swwiki": {
        "langCode": "sw",
        "langName": "Kiswahili",
        "langNameEN": "Swahili",
        "name": "Wikipedia",
        "url": "https://sw.wikipedia.org"
    },
    "swwiktionary": {
        "langCode": "sw",
        "langName": "Kiswahili",
        "langNameEN": "Swahili",
        "name": "Wiktionary",
        "url": "https://sw.wiktionary.org"
    },
    "szlwiki": {
        "langCode": "szl",
        "langName": "ślůnski",
        "langNameEN": "Silesian",
        "name": "Wikipedia",
        "url": "https://szl.wikipedia.org"
    },
    "szywiki": {
        "langCode": "szy",
        "langName": "Sakizaya",
        "langNameEN": "Sakizaya",
        "name": "Wikipitiya",
        "url": "https://szy.wikipedia.org"
    },
    "tawiki": {
        "langCode": "ta",
        "langName": "தமிழ்",
        "langNameEN": "Tamil",
        "name": "விக்கிப்பீடியா",
        "url": "https://ta.wikipedia.org"
    },
    "tawiktionary": {
        "langCode": "ta",
        "langName": "தமிழ்",
        "langNameEN": "Tamil",
        "name": "விக்சனரி",
        "url": "https://ta.wiktionary.org"
    },
    "tawikibooks": {
        "langCode": "ta",
        "langName": "தமிழ்",
        "langNameEN": "Tamil",
        "name": "விக்கிநூல்கள்",
        "url": "https://ta.wikibooks.org"
    },
    "tawikinews": {
        "langCode": "ta",
        "langName": "தமிழ்",
        "langNameEN": "Tamil",
        "name": "விக்கிசெய்தி",
        "url": "https://ta.wikinews.org"
    },
    "tawikiquote": {
        "langCode": "ta",
        "langName": "தமிழ்",
        "langNameEN": "Tamil",
        "name": "விக்கிமேற்கோள்",
        "url": "https://ta.wikiquote.org"
    },
    "tawikisource": {
        "langCode": "ta",
        "langName": "தமிழ்",
        "langNameEN": "Tamil",
        "name": "விக்கிமூலம்",
        "url": "https://ta.wikisource.org"
    },
    "taywiki": {
        "langCode": "tay",
        "langName": "Tayal",
        "langNameEN": "Tayal",
        "name": "Wikipidia",
        "url": "https://tay.wikipedia.org"
    },
    "tcywiki": {
        "langCode": "tcy",
        "langName": "ತುಳು",
        "langNameEN": "Tulu",
        "name": "ವಿಕಿಪೀಡಿಯ",
        "url": "https://tcy.wikipedia.org"
    },
    "tewiki": {
        "langCode": "te",
        "langName": "తెలుగు",
        "langNameEN": "Telugu",
        "name": "వికీపీడియా",
        "url": "https://te.wikipedia.org"
    },
    "tewiktionary": {
        "langCode": "te",
        "langName": "తెలుగు",
        "langNameEN": "Telugu",
        "name": "విక్షనరీ",
        "url": "https://te.wiktionary.org"
    },
    "tewikibooks": {
        "langCode": "te",
        "langName": "తెలుగు",
        "langNameEN": "Telugu",
        "name": "Wikibooks",
        "url": "https://te.wikibooks.org"
    },
    "tewikiquote": {
        "langCode": "te",
        "langName": "తెలుగు",
        "langNameEN": "Telugu",
        "name": "వికీవ్యాఖ్య",
        "url": "https://te.wikiquote.org"
    },
    "tewikisource": {
        "langCode": "te",
        "langName": "తెలుగు",
        "langNameEN": "Telugu",
        "name": "వికీసోర్స్",
        "url": "https://te.wikisource.org"
    },
    "tetwiki": {
        "langCode": "tet",
        "langName": "tetun",
        "langNameEN": "Tetum",
        "name": "Wikipedia",
        "url": "https://tet.wikipedia.org"
    },
    "tgwiki": {
        "langCode": "tg",
        "langName": "тоҷикӣ",
        "langNameEN": "Tajik",
        "name": "Википедиа",
        "url": "https://tg.wikipedia.org"
    },
    "tgwiktionary": {
        "langCode": "tg",
        "langName": "тоҷикӣ",
        "langNameEN": "Tajik",
        "name": "Wiktionary",
        "url": "https://tg.wiktionary.org"
    },
    "tgwikibooks": {
        "langCode": "tg",
        "langName": "тоҷикӣ",
        "langNameEN": "Tajik",
        "name": "Wikibooks",
        "url": "https://tg.wikibooks.org"
    },
    "thwiki": {
        "langCode": "th",
        "langName": "ไทย",
        "langNameEN": "Thai",
        "name": "วิกิพีเดีย",
        "url": "https://th.wikipedia.org"
    },
    "thwiktionary": {
        "langCode": "th",
        "langName": "ไทย",
        "langNameEN": "Thai",
        "name": "Wiktionary",
        "url": "https://th.wiktionary.org"
    },
    "thwikibooks": {
        "langCode": "th",
        "langName": "ไทย",
        "langNameEN": "Thai",
        "name": "วิกิตำรา",
        "url": "https://th.wikibooks.org"
    },
    "thwikiquote": {
        "langCode": "th",
        "langName": "ไทย",
        "langNameEN": "Thai",
        "name": "วิกิคำคม",
        "url": "https://th.wikiquote.org"
    },
    "thwikisource": {
        "langCode": "th",
        "langName": "ไทย",
        "langNameEN": "Thai",
        "name": "วิกิซอร์ซ",
        "url": "https://th.wikisource.org"
    },
    "tiwiki": {
        "langCode": "ti",
        "langName": "ትግርኛ",
        "langNameEN": "Tigrinya",
        "name": "ዊኪፔዲያ",
        "url": "https://ti.wikipedia.org"
    },
    "tiwiktionary": {
        "langCode": "ti",
        "langName": "ትግርኛ",
        "langNameEN": "Tigrinya",
        "name": "ዊኪ-መዝገበ-ቃላት",
        "url": "https://ti.wiktionary.org"
    },
    "tkwiki": {
        "langCode": "tk",
        "langName": "Türkmençe",
        "langNameEN": "Turkmen",
        "name": "Wikipediýa",
        "url": "https://tk.wikipedia.org"
    },
    "tkwiktionary": {
        "langCode": "tk",
        "langName": "Türkmençe",
        "langNameEN": "Turkmen",
        "name": "Wikisözlük",
        "url": "https://tk.wiktionary.org"
    },
    "tlwiki": {
        "langCode": "tl",
        "langName": "Tagalog",
        "langNameEN": "Tagalog",
        "name": "Wikipedia",
        "url": "https://tl.wikipedia.org"
    },
    "tlwiktionary": {
        "langCode": "tl",
        "langName": "Tagalog",
        "langNameEN": "Tagalog",
        "name": "Wiktionary",
        "url": "https://tl.wiktionary.org"
    },
    "tlwikibooks": {
        "langCode": "tl",
        "langName": "Tagalog",
        "langNameEN": "Tagalog",
        "name": "Wikibooks",
        "url": "https://tl.wikibooks.org"
    },
    "tnwiki": {
        "langCode": "tn",
        "langName": "Setswana",
        "langNameEN": "Tswana",
        "name": "Wikipedia",
        "url": "https://tn.wikipedia.org"
    },
    "tnwiktionary": {
        "langCode": "tn",
        "langName": "Setswana",
        "langNameEN": "Tswana",
        "name": "Wiktionary",
        "url": "https://tn.wiktionary.org"
    },
    "towiki": {
        "langCode": "to",
        "langName": "lea faka-Tonga",
        "langNameEN": "Tongan",
        "name": "Wikipedia",
        "url": "https://to.wikipedia.org"
    },
    "tpiwiki": {
        "langCode": "tpi",
        "langName": "Tok Pisin",
        "langNameEN": "Tok Pisin",
        "name": "Wikipedia",
        "url": "https://tpi.wikipedia.org"
    },
    "tpiwiktionary": {
        "langCode": "tpi",
        "langName": "Tok Pisin",
        "langNameEN": "Tok Pisin",
        "name": "Wiktionary",
        "url": "https://tpi.wiktionary.org"
    },
    "trwiki": {
        "langCode": "tr",
        "langName": "Türkçe",
        "langNameEN": "Turkish",
        "name": "Vikipedi",
        "url": "https://tr.wikipedia.org"
    },
    "trwiktionary": {
        "langCode": "tr",
        "langName": "Türkçe",
        "langNameEN": "Turkish",
        "name": "Vikisözlük",
        "url": "https://tr.wiktionary.org"
    },
    "trwikibooks": {
        "langCode": "tr",
        "langName": "Türkçe",
        "langNameEN": "Turkish",
        "name": "Vikikitap",
        "url": "https://tr.wikibooks.org"
    },
    "trwikiquote": {
        "langCode": "tr",
        "langName": "Türkçe",
        "langNameEN": "Turkish",
        "name": "Vikisöz",
        "url": "https://tr.wikiquote.org"
    },
    "trwikisource": {
        "langCode": "tr",
        "langName": "Türkçe",
        "langNameEN": "Turkish",
        "name": "Vikikaynak",
        "url": "https://tr.wikisource.org"
    },
    "trwikivoyage": {
        "langCode": "tr",
        "langName": "Türkçe",
        "langNameEN": "Turkish",
        "name": "Vikigezgin",
        "url": "https://tr.wikivoyage.org"
    },
    "trvwiki": {
        "langCode": "trv",
        "langName": "Seediq",
        "langNameEN": "Taroko",
        "name": "Wikipidiya",
        "url": "https://trv.wikipedia.org"
    },
    "tswiki": {
        "langCode": "ts",
        "langName": "Xitsonga",
        "langNameEN": "Tsonga",
        "name": "Wikipedia",
        "url": "https://ts.wikipedia.org"
    },
    "tswiktionary": {
        "langCode": "ts",
        "langName": "Xitsonga",
        "langNameEN": "Tsonga",
        "name": "Wiktionary",
        "url": "https://ts.wiktionary.org"
    },
    "ttwiki": {
        "langCode": "tt",
        "langName": "татарча/tatarça",
        "langNameEN": "Tatar",
        "name": "Wikipedia",
        "url": "https://tt.wikipedia.org"
    },
    "ttwiktionary": {
        "langCode": "tt",
        "langName": "татарча/tatarça",
        "langNameEN": "Tatar",
        "name": "Wiktionary",
        "url": "https://tt.wiktionary.org"
    },
    "ttwikibooks": {
        "langCode": "tt",
        "langName": "татарча/tatarça",
        "langNameEN": "Tatar",
        "name": "Wikibooks",
        "url": "https://tt.wikibooks.org"
    },
    "tumwiki": {
        "langCode": "tum",
        "langName": "chiTumbuka",
        "langNameEN": "Tumbuka",
        "name": "Wikipedia",
        "url": "https://tum.wikipedia.org"
    },
    "twwiki": {
        "langCode": "tw",
        "langName": "Twi",
        "langNameEN": "Twi",
        "name": "Wikipedia",
        "url": "https://tw.wikipedia.org"
    },
    "tywiki": {
        "langCode": "ty",
        "langName": "reo tahiti",
        "langNameEN": "Tahitian",
        "name": "Wikipedia",
        "url": "https://ty.wikipedia.org"
    },
    "tyvwiki": {
        "langCode": "tyv",
        "langName": "тыва дыл",
        "langNameEN": "Tuvinian",
        "name": "Википедия",
        "url": "https://tyv.wikipedia.org"
    },
    "udmwiki": {
        "langCode": "udm",
        "langName": "удмурт",
        "langNameEN": "Udmurt",
        "name": "Википедия",
        "url": "https://udm.wikipedia.org"
    },
    "ugwiki": {
        "langCode": "ug",
        "langName": "ئۇيغۇرچە / Uyghurche",
        "langNameEN": "Uyghur",
        "name": "Wikipedia",
        "url": "https://ug.wikipedia.org"
    },
    "ugwiktionary": {
        "langCode": "ug",
        "langName": "ئۇيغۇرچە / Uyghurche",
        "langNameEN": "Uyghur",
        "name": "Wiktionary",
        "url": "https://ug.wiktionary.org"
    },
    "ukwiki": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікіпедія",
        "url": "https://uk.wikipedia.org"
    },
    "ukwiktionary": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікісловник",
        "url": "https://uk.wiktionary.org"
    },
    "ukwikibooks": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікіпідручник",
        "url": "https://uk.wikibooks.org"
    },
    "ukwikinews": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікіновини",
        "url": "https://uk.wikinews.org"
    },
    "ukwikiquote": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікіцитати",
        "url": "https://uk.wikiquote.org"
    },
    "ukwikisource": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікіджерела",
        "url": "https://uk.wikisource.org"
    },
    "ukwikivoyage": {
        "langCode": "uk",
        "langName": "українська",
        "langNameEN": "Ukrainian",
        "name": "Вікімандри",
        "url": "https://uk.wikivoyage.org"
    },
    "urwiki": {
        "langCode": "ur",
        "langName": "اردو",
        "langNameEN": "Urdu",
        "name": "ویکیپیڈیا",
        "url": "https://ur.wikipedia.org"
    },
    "urwiktionary": {
        "langCode": "ur",
        "langName": "اردو",
        "langNameEN": "Urdu",
        "name": "ویکی لغت",
        "url": "https://ur.wiktionary.org"
    },
    "urwikibooks": {
        "langCode": "ur",
        "langName": "اردو",
        "langNameEN": "Urdu",
        "name": "ویکی کتب",
        "url": "https://ur.wikibooks.org"
    },
    "urwikiquote": {
        "langCode": "ur",
        "langName": "اردو",
        "langNameEN": "Urdu",
        "name": "ویکی اقتباس",
        "url": "https://ur.wikiquote.org"
    },
    "uzwiki": {
        "langCode": "uz",
        "langName": "oʻzbekcha/ўзбекча",
        "langNameEN": "Uzbek",
        "name": "Vikipediya",
        "url": "https://uz.wikipedia.org"
    },
    "uzwiktionary": {
        "langCode": "uz",
        "langName": "oʻzbekcha/ўзбекча",
        "langNameEN": "Uzbek",
        "name": "Vikilug‘at",
        "url": "https://uz.wiktionary.org"
    },
    "uzwikiquote": {
        "langCode": "uz",
        "langName": "oʻzbekcha/ўзбекча",
        "langNameEN": "Uzbek",
        "name": "Vikiiqtibos",
        "url": "https://uz.wikiquote.org"
    },
    "vewiki": {
        "langCode": "ve",
        "langName": "Tshivenda",
        "langNameEN": "Venda",
        "name": "Wikipedia",
        "url": "https://ve.wikipedia.org"
    },
    "vecwiki": {
        "langCode": "vec",
        "langName": "vèneto",
        "langNameEN": "Venetian",
        "name": "Wikipedia",
        "url": "https://vec.wikipedia.org"
    },
    "vecwiktionary": {
        "langCode": "vec",
        "langName": "vèneto",
        "langNameEN": "Venetian",
        "name": "Wikisionario",
        "url": "https://vec.wiktionary.org"
    },
    "vecwikisource": {
        "langCode": "vec",
        "langName": "vèneto",
        "langNameEN": "Venetian",
        "name": "Wikisource",
        "url": "https://vec.wikisource.org"
    },
    "vepwiki": {
        "langCode": "vep",
        "langName": "vepsän kel’",
        "langNameEN": "Veps",
        "name": "Vikipedii",
        "url": "https://vep.wikipedia.org"
    },
    "viwiki": {
        "langCode": "vi",
        "langName": "Tiếng Việt",
        "langNameEN": "Vietnamese",
        "name": "Wikipedia",
        "url": "https://vi.wikipedia.org"
    },
    "viwiktionary": {
        "langCode": "vi",
        "langName": "Tiếng Việt",
        "langNameEN": "Vietnamese",
        "name": "Wiktionary",
        "url": "https://vi.wiktionary.org"
    },
    "viwikibooks": {
        "langCode": "vi",
        "langName": "Tiếng Việt",
        "langNameEN": "Vietnamese",
        "name": "Wikibooks",
        "url": "https://vi.wikibooks.org"
    },
    "viwikiquote": {
        "langCode": "vi",
        "langName": "Tiếng Việt",
        "langNameEN": "Vietnamese",
        "name": "Wikiquote",
        "url": "https://vi.wikiquote.org"
    },
    "viwikisource": {
        "langCode": "vi",
        "langName": "Tiếng Việt",
        "langNameEN": "Vietnamese",
        "name": "Wikisource",
        "url": "https://vi.wikisource.org"
    },
    "viwikivoyage": {
        "langCode": "vi",
        "langName": "Tiếng Việt",
        "langNameEN": "Vietnamese",
        "name": "Wikivoyage",
        "url": "https://vi.wikivoyage.org"
    },
    "vlswiki": {
        "langCode": "vls",
        "langName": "West-Vlams",
        "langNameEN": "West Flemish",
        "name": "Wikipedia",
        "url": "https://vls.wikipedia.org"
    },
    "vowiki": {
        "langCode": "vo",
        "langName": "Volapük",
        "langNameEN": "Volapük",
        "name": "Vükiped",
        "url": "https://vo.wikipedia.org"
    },
    "vowiktionary": {
        "langCode": "vo",
        "langName": "Volapük",
        "langNameEN": "Volapük",
        "name": "Vükivödabuk",
        "url": "https://vo.wiktionary.org"
    },
    "wawiki": {
        "langCode": "wa",
        "langName": "walon",
        "langNameEN": "Walloon",
        "name": "Wikipedia",
        "url": "https://wa.wikipedia.org"
    },
    "wawiktionary": {
        "langCode": "wa",
        "langName": "walon",
        "langNameEN": "Walloon",
        "name": "Wiccionaire",
        "url": "https://wa.wiktionary.org"
    },
    "wawikisource": {
        "langCode": "wa",
        "langName": "walon",
        "langNameEN": "Walloon",
        "name": "Wikisource",
        "url": "https://wa.wikisource.org"
    },
    "warwiki": {
        "langCode": "war",
        "langName": "Winaray",
        "langNameEN": "Waray",
        "name": "Wikipedia",
        "url": "https://war.wikipedia.org"
    },
    "wowiki": {
        "langCode": "wo",
        "langName": "Wolof",
        "langNameEN": "Wolof",
        "name": "Wikipedia",
        "url": "https://wo.wikipedia.org"
    },
    "wowiktionary": {
        "langCode": "wo",
        "langName": "Wolof",
        "langNameEN": "Wolof",
        "name": "Wiktionary",
        "url": "https://wo.wiktionary.org"
    },
    "wowikiquote": {
        "langCode": "wo",
        "langName": "Wolof",
        "langNameEN": "Wolof",
        "name": "Wikiquote",
        "url": "https://wo.wikiquote.org"
    },
    "wuuwiki": {
        "langCode": "wuu",
        "langName": "吴语",
        "langNameEN": "Wu Chinese",
        "name": "维基百科",
        "url": "https://wuu.wikipedia.org"
    },
    "xalwiki": {
        "langCode": "xal",
        "langName": "хальмг",
        "langNameEN": "Kalmyk",
        "name": "Wikipedia",
        "url": "https://xal.wikipedia.org"
    },
    "xhwiki": {
        "langCode": "xh",
        "langName": "isiXhosa",
        "langNameEN": "Xhosa",
        "name": "Wikipedia",
        "url": "https://xh.wikipedia.org"
    },
    "xmfwiki": {
        "langCode": "xmf",
        "langName": "მარგალური",
        "langNameEN": "Mingrelian",
        "name": "ვიკიპედია",
        "url": "https://xmf.wikipedia.org"
    },
    "yiwiki": {
        "langCode": "yi",
        "langName": "ייִדיש",
        "langNameEN": "Yiddish",
        "name": "װיקיפּעדיע",
        "url": "https://yi.wikipedia.org"
    },
    "yiwiktionary": {
        "langCode": "yi",
        "langName": "ייִדיש",
        "langNameEN": "Yiddish",
        "name": "װיקיװערטערבוך",
        "url": "https://yi.wiktionary.org"
    },
    "yiwikisource": {
        "langCode": "yi",
        "langName": "ייִדיש",
        "langNameEN": "Yiddish",
        "name": "װיקיביבליאָטעק",
        "url": "https://yi.wikisource.org"
    },
    "yowiki": {
        "langCode": "yo",
        "langName": "Yorùbá",
        "langNameEN": "Yoruba",
        "name": "Wikipedia",
        "url": "https://yo.wikipedia.org"
    },
    "yuewiktionary": {
        "langCode": "yue",
        "langName": "粵語",
        "langNameEN": "Cantonese",
        "name": "維基辭典",
        "url": "https://yue.wiktionary.org"
    },
    "zawiki": {
        "langCode": "za",
        "langName": "Vahcuengh",
        "langNameEN": "Zhuang",
        "name": "Wikipedia",
        "url": "https://za.wikipedia.org"
    },
    "zeawiki": {
        "langCode": "zea",
        "langName": "Zeêuws",
        "langNameEN": "Zeelandic",
        "name": "Wikipedia",
        "url": "https://zea.wikipedia.org"
    },
    "zhwiki": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "Wikipedia",
        "url": "https://zh.wikipedia.org"
    },
    "zhwiktionary": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "Wiktionary",
        "url": "https://zh.wiktionary.org"
    },
    "zhwikibooks": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "Wikibooks",
        "url": "https://zh.wikibooks.org"
    },
    "zhwikinews": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "Wikinews",
        "url": "https://zh.wikinews.org"
    },
    "zhwikiquote": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "Wikiquote",
        "url": "https://zh.wikiquote.org"
    },
    "zhwikisource": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "Wikisource",
        "url": "https://zh.wikisource.org"
    },
    "zhwikiversity": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "維基學院",
        "url": "https://zh.wikiversity.org"
    },
    "zhwikivoyage": {
        "langCode": "zh",
        "langName": "中文",
        "langNameEN": "Chinese",
        "name": "维基导游",
        "url": "https://zh.wikivoyage.org"
    },
    "zh_classicalwiki": {
        "langCode": "zh-classical",
        "langName": "文言",
        "langNameEN": "Classical Chinese",
        "name": "維基大典",
        "url": "https://zh-classical.wikipedia.org"
    },
    "zh_min_nanwiki": {
        "langCode": "zh-min-nan",
        "langName": "Bân-lâm-gú",
        "langNameEN": "Chinese (Min Nan)",
        "name": "Wikipedia",
        "url": "https://zh-min-nan.wikipedia.org"
    },
    "zh_min_nanwiktionary": {
        "langCode": "zh-min-nan",
        "langName": "Bân-lâm-gú",
        "langNameEN": "Chinese (Min Nan)",
        "name": "Wiktionary",
        "url": "https://zh-min-nan.wiktionary.org"
    },
    "zh_min_nanwikisource": {
        "langCode": "zh-min-nan",
        "langName": "Bân-lâm-gú",
        "langNameEN": "Chinese (Min Nan)",
        "name": "Wiki Tô·-su-kóan",
        "url": "https://zh-min-nan.wikisource.org"
    },
    "zh_yuewiki": {
        "langCode": "zh-yue",
        "langName": "粵語",
        "langNameEN": "Cantonese",
        "name": "維基百科",
        "url": "https://zh-yue.wikipedia.org"
    },
    "zuwiki": {
        "langCode": "zu",
        "langName": "isiZulu",
        "langNameEN": "Zulu",
        "name": "Wikipedia",
        "url": "https://zu.wikipedia.org"
    },
    "zuwiktionary": {
        "langCode": "zu",
        "langName": "isiZulu",
        "langNameEN": "Zulu",
        "name": "Wiktionary",
        "url": "https://zu.wiktionary.org"
    },
    "apiportalwiki": {
        "langName": "Special",
        "name": "API Portal",
        "url": "https://api.wikimedia.org"
    },
    "arwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Argentina",
        "url": "https://ar.wikimedia.org"
    },
    "bdwikimedia": {
        "langName": "Special",
        "name": "উইকিমিডিয়া বাংলাদেশ",
        "url": "https://bd.wikimedia.org"
    },
    "bewikimedia": {
        "langName": "Special",
        "name": "Wikimedia Belgium",
        "url": "https://be.wikimedia.org"
    },
    "betawikiversity": {
        "langName": "Special",
        "name": "Wikiversity",
        "url": "https://beta.wikiversity.org"
    },
    "brwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Brasil",
        "url": "https://br.wikimedia.org"
    },
    "cawikimedia": {
        "langName": "Special",
        "name": "Wikimedia Canada",
        "url": "https://ca.wikimedia.org"
    },
    "cowikimedia": {
        "langName": "Special",
        "name": "Wikimedia Colombia",
        "url": "https://co.wikimedia.org"
    },
    "commonswiki": {
        "langName": "Special",
        "name": "Wikimedia Commons",
        "url": "https://commons.wikimedia.org"
    },
    "dkwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Danmark",
        "url": "https://dk.wikimedia.org"
    },
    "etwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Eesti",
        "url": "https://ee.wikimedia.org"
    },
    "fiwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Suomi",
        "url": "https://fi.wikimedia.org"
    },
    "foundationwiki": {
        "langName": "Special",
        "name": "Wikimedia Foundation Governance Wiki",
        "url": "https://foundation.wikimedia.org"
    },
    "incubatorwiki": {
        "langName": "Special",
        "name": "Wikimedia Incubator",
        "url": "https://incubator.wikimedia.org"
    },
    "loginwiki": {
        "langName": "Special",
        "name": "Wikimedia Login Wiki",
        "url": "https://login.wikimedia.org"
    },
    "mediawikiwiki": {
        "langName": "Special",
        "name": "MediaWiki Wiki",
        "url": "https://www.mediawiki.org"
    },
    "metawiki": {
        "langName": "Special",
        "name": "Meta",
        "url": "https://meta.wikimedia.org"
    },
    "mkwikimedia": {
        "langName": "Special",
        "name": "Викимедија Македонија",
        "url": "https://mk.wikimedia.org"
    },
    "mxwikimedia": {
        "langName": "Special",
        "name": "Wikimedia México",
        "url": "https://mx.wikimedia.org"
    },
    "nlwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Nederland",
        "url": "https://nl.wikimedia.org"
    },
    "nowikimedia": {
        "langName": "Special",
        "name": "Wikimedia Norge",
        "url": "https://no.wikimedia.org"
    },
    "nycwikimedia": {
        "langName": "Special",
        "name": "Wikimedia New York City",
        "url": "https://nyc.wikimedia.org"
    },
    "outreachwiki": {
        "langName": "Special",
        "name": "Outreach Wiki",
        "url": "https://outreach.wikimedia.org"
    },
    "plwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Polska",
        "url": "https://pl.wikimedia.org"
    },
    "ptwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Portugal",
        "url": "https://pt.wikimedia.org"
    },
    "ruwikimedia": {
        "langName": "Special",
        "name": "Викимедиа",
        "url": "https://ru.wikimedia.org"
    },
    "sewikimedia": {
        "langName": "Special",
        "name": "Wikimedia Sverige",
        "url": "https://se.wikimedia.org"
    },
    "sourceswiki": {
        "langName": "Special",
        "name": "Wikisource",
        "url": "https://wikisource.org"
    },
    "specieswiki": {
        "langName": "Special",
        "name": "Wikispecies",
        "url": "https://species.wikimedia.org"
    },
    "testwiki": {
        "langName": "Special",
        "name": "Test Wikipedia",
        "url": "https://test.wikipedia.org"
    },
    "test2wiki": {
        "langName": "Special",
        "name": "Test Wikipedia 2",
        "url": "https://test2.wikipedia.org"
    },
    "testcommonswiki": {
        "langName": "Special",
        "name": "Test Wikimedia Commons",
        "url": "https://test-commons.wikimedia.org"
    },
    "trwikimedia": {
        "langName": "Special",
        "name": "Wikimedia Türkiye",
        "url": "https://tr.wikimedia.org"
    },
    "uawikimedia": {
        "langName": "Special",
        "name": "Вікімедіа Україна",
        "url": "https://ua.wikimedia.org"
    },
    "wikidatawiki": {
        "langName": "Special",
        "name": "Wikidata",
        "url": "https://www.wikidata.org"
    },
    "wikimaniawiki": {
        "langName": "Special",
        "name": "Wikimania",
        "url": "https://wikimania.wikimedia.org"
    }
};