/* eslint-disable camelcase */
// The WMF OAuth2 driver
import { TY_BACKEND_OAUTH1A_CLIENT_ID, TY_BACKEND_OAUTH1A_CLIENT_SECRET } from "../../../common/src/backend/BackendSecretConstants";
import axios, { AxiosInstance } from "axios";
import addOAuthInterceptor from "axios-oauth-1.0a";
import qs from "qs";
import { TY_CURRENT_BASE_URL } from "../../../common/src/backend/BackendConstants";

export interface AuthURLResponse {
    authURL: string;
    oauthToken: string;
    oauthTokenSecret: string;
}

export interface OAuthInitReponse {
    error?: string;
    oauth_token?: string;
    oauth_token_secret?: string;
    oauth_callback_confirmed?: string;
}

export interface OAuthTokens {
    error?: boolean;
    token?: string;
    secret?: string;
}

export default class WMFOAuthDriver {

    private WMOAuthAxios: AxiosInstance;


    constructor() {
        this.WMOAuthAxios = axios.create();
        addOAuthInterceptor(this.WMOAuthAxios, {
            algorithm: "HMAC-SHA1",
            key: TY_BACKEND_OAUTH1A_CLIENT_ID,
            secret: TY_BACKEND_OAUTH1A_CLIENT_SECRET,
        });
    }



    public async getAuthURL(): Promise<AuthURLResponse> {
        // Get the URL using the provided client ID and secret
        // Return URL needs to be defined in OAuth config on metawiki

        

        const result = await this.WMOAuthAxios.get(`https://meta.wikimedia.org/wiki/Special:OAuth/initiate?${qs.stringify({
            oauth_callback: "oob",
        })}`);

        const tokens:OAuthInitReponse = qs.parse(result.data);
        if (tokens.error) throw new Error(tokens.error);
        
        // Return the URL and 
        return {
            "authURL": `https://meta.wikimedia.org/wiki/Special:OAuth/authorize?${qs.stringify({
                oauth_token: tokens.oauth_token,
                oauth_consumer_key: TY_BACKEND_OAUTH1A_CLIENT_ID,
            })}`,
            "oauthToken": tokens.oauth_token,
            "oauthTokenSecret": tokens.oauth_token_secret,
        };
    }

    public async getToken(oauth_verifier: string, oauth_token: string, oauthTokenSecret: string): Promise<OAuthTokens> {
        try {
            // This request needs some extra info
            const tokenAxios = axios.create();
            addOAuthInterceptor(tokenAxios, {
                algorithm: "HMAC-SHA1",
                key: TY_BACKEND_OAUTH1A_CLIENT_ID,
                secret: TY_BACKEND_OAUTH1A_CLIENT_SECRET,
                token: oauth_token,
                tokenSecret: oauthTokenSecret,
            });

            const result = await tokenAxios.get(`https://meta.wikimedia.org/wiki/Special:OAuth/token?${qs.stringify({
                oauth_verifier,
            })}`);
            
            const tokenResult = qs.parse(result.data);

            if (tokenResult.error) return { error: true };

            return {
                token: tokenResult.oauth_token.toString(),
                secret: tokenResult.oauth_token_secret.toString(),
            };
        } catch (error) {
            return {error: true};
        }
    }

    public getAuthedAxios(token: OAuthTokens): AxiosInstance {
        const authedAxios = axios.create();
        addOAuthInterceptor(authedAxios, {
            algorithm: "HMAC-SHA1",
            key: TY_BACKEND_OAUTH1A_CLIENT_ID,
            secret: TY_BACKEND_OAUTH1A_CLIENT_SECRET,
            token: token.token,
            tokenSecret: token.secret,
        });
        return authedAxios;
    }
}