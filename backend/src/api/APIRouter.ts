import { Router } from "express";
import { TY_CURRENT_BASE_URL } from "../../../common/src/backend/BackendConstants";
import WMFOAuthDriver, { OAuthTokens } from "./WMFOAuthDriver";
import TeyoraDB from "../db/TeyoraDB";
import UserAuth from "../helpers/UserAuth";
import JWTVerify from "../helpers/JWTVerify";
import { TY_BACKEND_IDENTIFIER } from "../../../common/src/Constants";
import { WikiSupport } from "../info/wikis/WikiSupport";
import { ProfileEntry, UserProfile } from "../db/dbcollections/profile";

// This is the main API router, it handles all API points and what they do.
export default class APIRouter {
    private readonly router: Router;
    private readonly wmfOAuth: WMFOAuthDriver;
    private db: TeyoraDB;
    
    constructor(db: TeyoraDB) {
        this.router = Router();
        this.wmfOAuth = new WMFOAuthDriver();
        this.db = db;
        this.routes();
    }
    
    public getRouter(): Router {
        return this.router;
    }
    
    private routes() {
        // This is all our API routes :)
        this.router.get("/api", (req, res) => {
            res.json({
                message: "Welcome to the Teyora API!",
                version: TY_BACKEND_IDENTIFIER
            });
        });

        // Logout API
        this.router.get("/api/logout", (req, res) => {
            // TODO: Establish auth first
            res.clearCookie("DONOTSHARE_teyoraJWT");
            res.redirect(`${TY_CURRENT_BASE_URL}`);
        });

        // WMF OAuth redirect
        this.router.get("/api/oauth/wmf/redirect", async (req, res) => {
            try {
                const authURL = await this.wmfOAuth.getAuthURL();
                // Store the client secret as a cookie
                res.cookie("TEMPOAuthSecret", authURL.oauthTokenSecret);
                res.cookie("TEMPOAuthToken", authURL.oauthToken);
                res.redirect(authURL.authURL);
            } catch (error) {
                res.status(500).json({
                    message: "Something went wrong",
                    error: error.message
                });
            }
        });

        // WMF OAuth callback, this includes making a new account etc.
        this.router.get("/api/oauth/wmf/callback", async (req, res) => {
            if (!req.query.oauth_verifier || !req.query.oauth_token || !req.cookies.TEMPOAuthSecret) {
                res.status(400).json({
                    message: "No oauth_verifier or oauth_token provided, or cookie missing",
                });
                return;
            }

            const token: OAuthTokens = await this.wmfOAuth.getToken(
                req.query.oauth_verifier.toString(),
                req.cookies.TEMPOAuthToken,
                req.cookies.TEMPOAuthSecret
            );

            // Clear the cookie
            res.clearCookie("TEMPOAuthSecret");
            res.clearCookie("TEMPOAuthToken");

            if (token.error) {
                res.status(500).json({
                    message: "Something went wrong with your login request. Please try again.",
                });
                return;
            }

            // Get metawiki profile
            const authxios = this.wmfOAuth.getAuthedAxios(token);
            let profileRequest;
            try {
                profileRequest = (await authxios.get("https://meta.wikimedia.org/w/api.php?action=query&meta=userinfo&format=json")).data;
            } catch (error) {
                console.log(error);
                res.status(500).json({
                    message: "Failed to get user info from WMF OAuth",
                });
                return;
            } 
            

            const wmfUsername = profileRequest.query.userinfo.name;

            // Get the user info
            let userInfo = await UserAuth.getUserNPVByUsername(this.db,`WMF:${wmfUsername}`);

            // If the user doesn't exist, create it
            if (!userInfo) {
                userInfo = await UserAuth.createUser(this.db, `WMF:${wmfUsername}`, "Teyora WMF Callback API", "WMF");
            }
                
            // Make the JWT
            const jwtToken = await JWTVerify.newJWT(this.db, userInfo, token);

            // Set a new HttpOnly cookie
            res.cookie("DONOTSHARE_teyoraJWT", jwtToken, {
                httpOnly: true, // We don't set a domain as by default it's the current domain
            });
                
            // Head to the frontend
            res.redirect(`${TY_CURRENT_BASE_URL}`);
                
            return;
        });

        // Get the user info for the current user
        this.router.get("/api/user/info", async (req, res) => {
            try {
                const canRun = await UserAuth.getAndVerifyRequestInfo(this.db, req, res, {
                    SuperOverlordOnly: false,
                    OverlordOnly: false,
                    AdminOnly: false,
                    VerifiedOnly: false,
                    ApprovedUsersOnly: false,
                    SuspendedCanRun: true,
                    LockedUsersCanRun: true,
                });
                if (typeof canRun == "boolean") throw new Error(); // Unauthorized

                res.status(200).json(canRun.user);
            } catch (error) {
                if (res.destroyed) return; // Response already sent
                res.status(403).json({
                    error: "Forbidden",
                    message: "You don't have sufficent permissions for this endpoint. You may need to log in again."
                });
            }
        });

        // Get supported wikis
        this.router.get("/api/wikisupport", async (req, res) => {
            try {
                const canRun = await UserAuth.getAndVerifyRequestInfo(this.db, req, res, {
                    SuperOverlordOnly: false,
                    OverlordOnly: false,
                    AdminOnly: false,
                    VerifiedOnly: false,
                    ApprovedUsersOnly: false,
                    SuspendedCanRun: false,
                    LockedUsersCanRun: true, // No eligibility for this, used in First Time Setup
                });
                if (typeof canRun == "boolean") throw new Error(); // Unauthorized

                res.status(200).json(WikiSupport);
            } catch (error) {
                if (res.destroyed) return; // Response already sent
                res.status(403).json({
                    error: "Forbidden",
                    message: "You don't have sufficent permissions for this endpoint. You may need to log in again."
                });
            }
        });

        // Unlock a user
        this.router.get("/api/user/unlock", async (req, res) => {
            try {
                const canRun = await UserAuth.getAndVerifyRequestInfo(this.db, req, res, {
                    SuperOverlordOnly: false,
                    OverlordOnly: false,
                    AdminOnly: false,
                    VerifiedOnly: false,
                    ApprovedUsersOnly: false,
                    SuspendedCanRun: false,
                    LockedUsersCanRun: true, // No eligibility for this, used in First Time Setup
                });
                if (typeof canRun == "boolean") throw new Error(); // Unauthorized

                // If no WikiID
                if (!req.query.wikiID) {
                    res.status(400).json({
                        error: "Bad Request",
                        message: "No wikiID provided"
                    });
                    return;
                }

                try {
                    const user = await UserAuth.unlockUser(this.db, this.wmfOAuth, canRun, req.query.wikiID.toString());
                    res.status(200).json(user);
                } catch (error) {
                    // An issue occured with the wiki
                    res.status(500).json({
                        message: "Failed to unlock user on wiki",
                        error: error.message
                    });
                }
                
            } catch (error) {
                if (res.destroyed) return; // Response already sent
                res.status(403).json({
                    error: "Forbidden",
                    message: "You don't have sufficent permissions for this endpoint, or there was an issue getting the required information. You may need to log in again."
                });
            }
        });

        // Get a profile for this user
        this.router.get("/api/user/profile", async (req, res) => {
            try {
                const canRun = await UserAuth.getAndVerifyRequestInfo(this.db, req, res, {
                    SuperOverlordOnly: false,
                    OverlordOnly: false,
                    AdminOnly: false,
                    VerifiedOnly: false,
                    ApprovedUsersOnly: false,
                    SuspendedCanRun: false,
                    LockedUsersCanRun: false, // Users must be unlocked before they can move forward
                });
                if (typeof canRun == "boolean") throw new Error(); // Unauthorized

                try {
                    const profile = await UserAuth.getUserProfileNPV(this.db, canRun.user);
                    res.status(200).json(profile);
                } catch (error) {
                    // An issue occured with the wiki
                    res.status(500).json({
                        message: "An internal error occured",
                        error: error.message
                    });
                }
                
            } catch (error) {
                if (res.destroyed) return; // Response already sent
                res.status(403).json({
                    error: "Forbidden",
                    message: "You don't have sufficent permissions for this endpoint. You may need to log in again."
                });
            }
        });

        // Update the profile for this user
        this.router.put("/api/user/profile", async (req, res) => {
            try {
                // Assert that this request can be run
                const canRun = await UserAuth.getAndVerifyRequestInfo(this.db, req, res, {
                    SuperOverlordOnly: false,
                    OverlordOnly: false,
                    AdminOnly: false,
                    VerifiedOnly: false,
                    ApprovedUsersOnly: false,
                    SuspendedCanRun: false,
                    LockedUsersCanRun: false, // Users must be unlocked before they can move forward
                });
                if (typeof canRun == "boolean") throw new Error(); // Unauthorized

                // Get new profile
                const newProfile:UserProfile = req.body;

                // Verify the new profile
                // Bio checks: Length must be less than 241 characters, user must be approved
                newProfile.bio = (newProfile.bio && (newProfile.bio.length < 241 && canRun.user.isApproved) ? newProfile.bio : "").trim();

                // Nick name checks: Length must be less than 41 characters, user must be approved, else
                // nickname will be set to the username (this is done in ui)
                newProfile.nickname = (newProfile.nickname && (newProfile.nickname.length < 41 && canRun.user.isApproved)
                    ? newProfile.nickname : "").trim();

                // Avatar checks: Length must be less than 35 characters, else empty
                newProfile.profilePicture = (newProfile.profilePicture && newProfile.profilePicture.length < 35 ? newProfile.profilePicture : "").trim();

                // Update the user
                try {
                    const profile = await UserAuth.setUserProfileNPV(this.db, canRun.user, {
                        username: canRun.user.username, // We need to generate a profile entry
                        userID: canRun.user.userID,
                        profile: newProfile
                    } as ProfileEntry);

                    res.status(200).json(profile);
                } catch (error) {
                    // An issue occured setting the profile
                    res.status(500).json({
                        message: "An internal error occured",
                        error: error.message
                    });
                }
                
            } catch (error) {
                if (res.destroyed) return; // Response already sent
                res.status(403).json({
                    error: "Forbidden",
                    message: "You don't have sufficent permissions for this endpoint. You may need to log in again."
                });
            }
        });
    }
}