/*
Teyora project constants - change behaviour for your instance here.
*/

import {TY_GIT_HASH} from "./DynamicConstants";

export const TY_DEBUG = process.env.NODE_ENV === "production" ? "production" : "development";

// Version management
export const TY_FRONTEND_VERSION = "0.1.0";
export const TY_BACKEND_VERSION = "0.1.0";

// Version identifiers
export const TY_FRONTEND_IDENTIFIER = `${TY_FRONTEND_VERSION}${
    TY_DEBUG ? "d" : "p"
}-${TY_GIT_HASH.substr(0, 6)}`;
export const TY_BACKEND_IDENTIFIER = `${TY_BACKEND_VERSION}${
    TY_DEBUG ? "d" : "p"
}-${TY_GIT_HASH.substr(0, 6)}`;

/** Backend constants **/

/** Frontend constants **/

// How long to forcefully delay the rendering of the Teyora app for - this is utilised to add the "Teyora" splash screen when loading is fast.
// This serves no useful purpose outside of being aesthetically pleasing. If loading takes longer, lower this time.
export const TY_FRONTEND_SPLASH_DELAY = 0;

// How long to wait between each dialog in a queue
export const TY_FRONTEND_DIALOG_DELAY = 300;

// How long the verification loader should take
export const TY_FRONTEND_VERIFICATION_LOADER_DELAY = 2000;

// If you have a Sentry or GlitchTip instance, put the DSN here
// If you are setting up your own instance, PLEASE remove the default DSN.
export const TY_SENTRY_DSN = "https://941665b8251c414aba83773966b5b49f@tyanalytics.wmcloud.org/1";

// The wiki we are currently on. DO NOT KEEP! This must be changed later on!
export const TY_FRONTEND_WIKI = "enwiki";

/** Wikipedia-related constants **/

// ORES supported Wikis - Taken from "https://ores.wikimedia.org/v3/scores/"
// This should be replaced soon.
export const TY_ORES_SUPPORTED_WIKIS = ["arwiki", "bnwiki", "bswiki", "cawiki", "cswiki", "dewiki", "elwiki", "enwiki", "enwiktionary", "eswiki", "eswikibooks", "eswikiquote", "etwiki", "euwiki", "fakewiki", "fawiki", "fiwiki", "frwiki", "frwikisource", "glwiki", "hewiki", "hrwiki", "huwiki", "idwiki", "iswiki", "itwiki", "jawiki", "kowiki", "lvwiki", "nlwiki", "nowiki", "plwiki", "ptwiki", "rowiki", "ruwiki", "simplewiki", "sqwiki", "srwiki", "svwiki", "tawiki", "testwiki", "trwiki", "ukwiki", "viwiki", "wikidatawiki", "zhwiki"];