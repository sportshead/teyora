/**
 * This file contains two things:
 * 1. Your JWT secret, which is used to sign your JWTs (what users use to authenticate)
 * 2. Any OAuth2 secrets, which are used to authenticate external APIs and make requests
 * 
 * WARNING: You must keep this file private. Failing to do so will expose your instance
 * and users to the possibility of unauthorized access.
 * 
 * REMEMBER: Rename this file to BackendSecretConstants.ts and add your secret keys, or
 * the build will fail.
 */

export const TY_BACKEND_JWT_SECRET = "YOUR_JWT_SECRET";
export const TY_BACKEND_OAUTH1A_CLIENT_ID = "YOUR_OAUTH1A_CLIENT_ID";
export const TY_BACKEND_OAUTH1A_CLIENT_SECRET = "YOUR_OAUTH1A_CLIENT_SECRET";

// Your CouchDB admin password (use port 6767, see CONTRIBUTING.md)
export const TY_BACKEND_COUCHDB_PASSWORD = "YOUR_MONGO_PASSWORD";