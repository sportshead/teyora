import path from "path";
import fs from "fs";
import { TY_DEBUG } from "../Constants";

/*
 * These constants are kept separate from Constants.ts, since
 * they have server-side imports that webpack might bundle,
 * something that we don't want to happen.
 */

export const TY_ROOT_PATH = (() => {
    const root = path.resolve("/");
    let currentPath = path.resolve(__dirname);

    while (!fs.existsSync(path.join(currentPath, ".Teyora-anchor"))) {
        if (currentPath === root)
            return null;
        else
            currentPath = path.resolve(currentPath, "..");
    }

    return currentPath;
})();

// The current domain, used for security and other things
export const TY_CURRENT_DOMAIN = TY_DEBUG ? `localhost:${process.env.PORT ?? 45990}` : "teyora.wmflabs.org";

// The current domain security policy (should ALWAYS be HTTPS for production)
export const TY_CURRENT_DOMAIN_POLICY = TY_DEBUG ? "http" : "https";

// Base URL, a combination of the current domain and the current domain policy
export const TY_CURRENT_BASE_URL = `${TY_CURRENT_DOMAIN_POLICY}://${TY_CURRENT_DOMAIN}`;

// Sentry DSN - PLEASE change this to your own DSN rather than the one in the repo
export const TY_BACKEND_SENTRY_DSN = "https://d6bb0df6b9ae486cadd2d5d7ad29f53d@tyanalytics.wmcloud.org/2";