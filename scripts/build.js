"use strict";

/* eslint-disable @typescript-eslint/no-var-requires */
const { exec } = require("child_process");
const path = require("path");
const fs = require("fs");

(async () => {
    const begin = Date.now();

    const rootDir = (() => {
        const root = path.resolve("/");
        let currentPath = path.resolve(__dirname);

        while (!fs.existsSync(path.join(currentPath, ".Teyora-anchor"))) {
            if (currentPath === root)
                return null;
            else
                currentPath = path.resolve(currentPath, "..");
        }

        return currentPath;
    })();
    const commonsDir = path.join(rootDir, "common");

    const runProcess = async (command, options, runOptions = {
        silent: false
    }) => {
        if (!runOptions.silent) console.log("-".repeat(60));
        const proc = exec(command, {
            cwd: rootDir,
            windowsHide: true,
            ...options
        });

        let stdout = Buffer.alloc(0);
        let stderr = Buffer.alloc(0);

        // noinspection JSUnresolvedFunction
        proc.stdout.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stdout.write(d);

            stdout = Buffer.concat([stdout, data]);
        });

        // noinspection JSUnresolvedFunction
        proc.stderr.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stderr.write(d);

            stderr = Buffer.concat([stderr, data]);
        });

        const code = await new Promise((res, rej) => {
            proc.on("exit", function (code) {
                if (code === 0) res(code);
                else rej(code);
            });
        }).catch(r => r);

        if (!runOptions.silent) console.log("-".repeat(60));
        return {
            code: code,
            stdout: stdout,
            stderr: stderr
        };
    };

    // Introduction
    console.log();
    console.log(" ---------------------------- Teyora Build Script  ---------------------------- ");
    console.log();
    console.log("(c) 2020 The Teyora contributors.");
    console.log("Licensed under the Apache License 2.0 - read more at https://gitlab.com/Teyora");
    console.log();

    // Determine Teyora version
    const rwConstants = fs.readFileSync(path.resolve(commonsDir, "src", "Constants.ts")).toString("utf-8");
    const rwbVersion = (/TY_BACKEND_VERSION\s?=\s?"(.*)";/g
        .exec(rwConstants))[1];
    const rwfVersion = (/TY_FRONTEND_VERSION\s?=\s?"(.*)";/g
        .exec(rwConstants))[1];

    // Determine git commit
    const gitHash = (await runProcess("git rev-parse HEAD", {}, {
        silent: true
    })).stdout.toString("utf-8").trim();

    const rwbIdentifier = `${rwbVersion}${
        process.env.NODE_ENV === "production" ? "p" : "d"
    }-${gitHash.substr(0, 6)}`;
    const rwfIdentifier = `${rwfVersion}${
        process.env.NODE_ENV === "production" ? "p" : "d"
    }-${gitHash.substr(0, 6)}`;

    console.log(`Building for commit: ${gitHash.trim()}...`);
    console.log(`Teyora (Frontend) v${rwfIdentifier}`);
    console.log(`Teyora (Backend) v${rwbIdentifier}`);
    console.log();

    console.log("Updating constants...");
    console.log();

    fs.writeFileSync(
        path.resolve(commonsDir, "src", "DynamicConstants.ts"),
        `export const TY_GIT_HASH = "${gitHash}";`
    );

    console.log("Executing frontend build script...");
    console.log("=".repeat(60));
    await require("./../frontend/scripts/build.js");
    console.log("=".repeat(60));

    console.log("Executing backend build script...");
    console.log("=".repeat(60));
    await require("./../backend/scripts/build.js");
    console.log("=".repeat(60));

    for (let i = 0; i < 10; i++) console.log();

    console.log(`Build successfully completed after ${(Date.now() - begin) / 1000}s.`);
    console.log("Happy patrolling! ~~~~");
})();