# Contributing to Teyora

We've made the general setup process easy, given you're familiar with the tools and technologies we use.

## Preparing the Environment
1. Ensure that you have the latest version and `npm` installed.
2. **(recommended)** Make sure that you have a GPG key set up alongside your Git client so you can sign your commits.
3. Have a mongoDB installed - you can read how to at https://docs.mongodb.com/

## Setting Up
Teyora's build scripts are written in JavaScript for maximum cross-platform compatibility. This means all scripts are executed using Node. No need to worry about packages, since the build scripts rely only on Node modules. 

1. Clone the Teyora repository.
2. To setup Teyora for development, make sure that your `NODE_ENV` environment variable is unset or set to "development". Otherwise, set it to "production".
3. Download all required dependencies with the `setup` script.
   ```shell script
   # From "Teyora/"
   node scripts/setup.js
   ```
You can now begin developing Teyora.

## Setting up database
1. Download and install the latest version of Apache CouchDB from https://couchdb.apache.org/, following the appropriate documentation at https://docs.couchdb.org/en/stable/install/index.html
   Remember to set a secure password for your admin user when prompted.
2. Open Fauxton and verify the installation.
3. Head to the setup page then "Configure a Single Node"
4. Set the username to 'teyorabackend', then generate a very secure password for your admin user and save it for later.
5. Set the port to 6767, then save the configuration.

Everything else will be set up automatically by the backend scripts when you run your server.

## Fill in constants
Ensure you fill in all constants in teyora/common, this will make sure that the build scripts work correctly
for your environment.
1. src/backend/BackendConstants.ts
2. Constants.ts
3. Follow the instructions in src/backend/DEMOBackendSecretConstants.ts to add relevant keys

## Backend Logging
In case you want to read the logs of the Teyora Backend (which uses `bunyan` for logging), you'll also have to install `bunyan` globally and pipe the output there.
1. Install `bunyan` globally with the following:
   ```shell script
   npm install -g bunyan
   ```
2. Pipe the output of the running backend server to `bunyan`.
   ```shell script
   # This works on Windows too.
   npm run dev-start | bunyan
   ```

## Building Teyora
1. If you're done making changes, the next step is to build Teyora. There are three paths to take here.
   * If you're making changes to Teyora for eventual production use, you need to use the `build` script at the root.
   * If you're making changes to only one end for eventual production use, you need to use the `build` script in whatever end you're using.
   * If you want to skip dependency checking, you can just directly run `npm run webpack` for the frontend, or `tsc` for the backend. **[end]**
2. Choose the appropriate build script. This is either the `build.js` script at the root folder's scripts, or the `build.js` at each end's scripts.
3. Execute that build script.
   ```shell script
   node scripts/build.js
   ```
4. If you built the backend, the output files should be in `backend/build/`. If you built the frontend, the output script should be in `frontend/build/Teyora.js`, and an updated script should be available in the `static/scripts` directory. Building the frontend will also automatically update the `index.html` of the frontend.
5. If you have the backend server running, you can now visit Teyora using the port configured. By default, this is port `45990`.

## Developing Teyora
Since having to Dockerize everything is slow, you can just run Teyora's backend server bare on your machine. To do that, follow the following steps:
1. Build the backend at least once with the instructions above on "Building Teyora", or by running the following from the `backend/` directory.
   ```shell script
   npm run build
   ```
2. Run the Teyora backend server with the following from the `backend/` directory.
   ```shell script
   npm start
   ```
   This should start the Teyora backend server. Do note that since Teyora uses [bunyan](https://www.npmjs.com/package/bunyan) for logging, the log output is in JSON. You can pass the output to `bunyan-cli` in case you want to read it.
4. The web server should now be available at your system's port `45990`. Opening it should bring you to the Teyora frontend.
5. For every change in `frontend/` that requires rebuilding, you have to pack everything again. However, Teyora's `index.html` changes depending on the commit, so you'll have to use the provided npm script instead of manually building with `webpack`.
   ```shell script
   npm run repack
   ```
6. Just repack again whenever you change `frontend` components.

## Additional Notes

* [Frontend] Every time you make a change, you'll have to rebuild the bundle or just run `webpack` again. When recompiling TypeScript or repacking the webpack bundle, you should just let the IDE do the work for you.
* Though you can theoretically run `index.html` as is, if you don't have the backend, it won't work. Watch out for that.
* The `static/` folder has a few static files, and while you might have the urge to get rid of them since we can instead just make a purely-React file, we do have to consider that it works as a polyfill if:
    * The user does not have a fast internet connection and cannot quickly load the Teyora React script.
    * The user has JavaScript disabled.
    * The user's connection drops while loading the Teyora React script.
* The scripts were made without Docker in mind, which means users who don't want to use Docker can just run everything as is (but that's not recommended, of course).
* A security warning: Teyora uses JWTs (JSON Web Tokens) to authenticate the client and ensure Oauth tokens aren't stored on server to reduce the risk of external account takeover if your instance is compromised. That being said, DO NOT share your private JWT tokens EVER. Doing so will put your users at risk of token forgery.
    
## Directory Tree
An explainer of files and directories.

- **Backend** (`backend`) - The Teyora backend, which handles everything Wikipedia, authentication, etc.
- **Frontend** (`frontend`) - The Teyora frontend, which handles the Teyora React script. It is made with React and a ton of TS JSX.
- **Commons** (`common`) - Common type definitions used by both frontend and backend. Both ends are configured to automatically compile with the commons. Because of this, the commons does not have a `tsconfig.json` file. However, it still has a `package.json` file due to ESLint. 
- **Static** (`static`) - Teyora static files, which are usually served by the web handler normally.

## Translations
Teyora is flexible and can be translated to many languages and locales, but the default locale is British English (en-GB). Therefore, if you add any new keys to Teyora, you MUST ALWAYS add them to the default locale or your pull requests will be rejected.

## VS Code Extensions
If you are using Visual Studio Code, these extensions were used for development and we recommend you use them if they are available.

- Color Highlight by Sergii Naumov
- ESLint by Microsoft
- GitHub Copilot by Microsoft
- GitLens by GitKraken
- GitLab Workflow by GitLab
- HTTP Status Codes by Beatzoid
- i18n Ally by Lokalise
- MUI Snippets by vscodeshift