# Teyora
Teyora's core app - made with Typescript and React.


This repository contains both frontend and backend. You can find them in their respective folders (`frontend`, and `backend`). Both ends rely on the `common` folder for common definitions.

## Contributors
Teyora is primarily maintained and developed by [@ed_E](https://gitlab.com/ed_e)** ([\[\[User:Ed6767\]\]](https://en.wikipedia.org/wiki/User:Ed6767))

However, it wouldn't have been possible without:
* **[@pr0mpted](https://gitlab.com/pr0mpted)** - ([\[\[User:Prompt0259\]\]](https://en.wikipedia.org/wiki/User:Prompt0259)) - additional development and design
* **[@ChlodAlejandro](https://gitlab.com/ChlodAlejandro)** - ([\[\[User:Chlod\]\]](https://en.wikipedia.org/wiki/User:Chlod)) - additional development and design
* leijurv

Development has been assisted by GitHub Copilot.

## Development
External contributions aren't being taken at this time as Teyora is still being planned. Check back soon.